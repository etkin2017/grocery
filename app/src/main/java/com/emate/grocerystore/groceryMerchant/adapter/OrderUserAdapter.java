package com.emate.grocerystore.groceryMerchant.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.OrdersByDateAdapter;
import com.emate.grocerystore.develop.activities.models.Orders;
import com.emate.grocerystore.develop.activities.models.Users;
import com.emate.grocerystore.groceryMerchant.MyOrderListActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Etkin on 3/27/2018.
 */

public class OrderUserAdapter extends RecyclerView.Adapter<OrderUserAdapter.OrdersDataHolder> {

    List<Users> usersList = new ArrayList<>();
    Context ctx;

    List<Orders> ordersList = new ArrayList<>();

    public OrderUserAdapter(List<Users> usersList, Context ctx, List<Orders> ordersList) {
        this.usersList = usersList;
        this.ctx = ctx;
        this.ordersList = ordersList;
    }

    @Override
    public OrdersDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.users_row, parent, false);
        return new OrdersDataHolder(view);
    }

    @Override
    public void onBindViewHolder(OrdersDataHolder holder, final int position) {
        Users users = usersList.get(position);
        holder.userNm.setText(users.getUser_name());
        holder.mobNo.setText("Mob. No. : " + users.getMb_no());
        holder.addrees.setText("Address : " + users.getShipping_add());
        holder.user_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<String> dates = getDates(ordersList, usersList.get(position).getUnique_id());

                Intent intent = new Intent(ctx, MyOrderListActivity.class);
                intent.putExtra(Config.FROM, (ArrayList) ordersList);
                intent.putExtra("AllDates", (ArrayList) dates);
                ctx.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public class OrdersDataHolder extends RecyclerView.ViewHolder {

        TextView userNm, mobNo, addrees;
        LinearLayout user_container;


        public OrdersDataHolder(View itemView) {
            super(itemView);

            user_container = (LinearLayout) itemView.findViewById(R.id.user_container);
            userNm = (TextView) itemView.findViewById(R.id.userNm);
            mobNo = (TextView) itemView.findViewById(R.id.mobNo);
            addrees = (TextView) itemView.findViewById(R.id.addrees);

        }
    }

    public List<String> getDates(List<Orders> productModelList, String userId) {
        List<String> dates = new ArrayList<>();
        for (int i = 0; i < productModelList.size(); i++) {
            if (!dates.contains(productModelList.get(i).getOrder_dt_tim())) {
                if ((productModelList.get(i).getUsers().getUnique_id().equalsIgnoreCase(userId))) {
                    dates.add(productModelList.get(i).getOrder_dt_tim());
                }
            }
        }
        return dates;
    }
}
