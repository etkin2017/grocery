package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EnterOtpActivity extends AppCompatActivity {
    EditText edtotp;
    Button btnsubmit, btncancle, ok, no;
    String js;
    User user;

    public AwesomeValidation awesomeValidation1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);
        edtotp = (EditText) findViewById(R.id.edtotp);
        btnsubmit = (Button) findViewById(R.id.submitotp);
        btncancle = (Button) findViewById(R.id.cancleotp);
        awesomeValidation1 = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation1.addValidation(this, R.id.edtotp, "^[A-Za-z0-9\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.otperror);


        SharedPreferences sharedPreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        js = sharedPreferences.getString("k", "");
        try {
            JSONObject j3 = new JSONObject(js);
            JSONArray jj = j3.getJSONArray("success");
            // JSONArray j3=jj.getJSONObject(0).getJSONArray("user");
            JSONObject j2 = jj.getJSONObject(0);

            int id = j2.getInt("id");
            String name = j2.getString("name");
            String mob = j2.getString("mob");
            String email = j2.getString("email");
            String pass = j2.getString("pass");
            String shop = j2.getString("shop");
            String address = j2.getString("address1");
            String area = j2.getString("address2");
            String landmark = j2.getString("landmark");
            String city = j2.getString("city");
            String state= j2.getString("state");
            int pin = j2.getInt("pincode");
            String gst = j2.getString("gst");
            String sale = j2.getString("sale");
            String pdfUrl1 = j2.getString("idproofurl");
            String pdfUrl2 = j2.getString("shopproofurl");
            String pdfUrl3 = j2.getString("gstproofurl");

            user = new User();
            user.setId(id);
            user.setName(name);
            user.setMobno(mob);
            user.setEmail(email);
            user.setPassword(pass);
            user.setShop(shop);
            user.setAddress(address);
            user.setArea(area);
            user.setLandmark(landmark);
            user.setCity(city);
            user.setState(state);
            user.setPin(pin);
            user.setGst(gst);
            user.setSale(sale);
            user.setUrl1(pdfUrl1);
            user.setUrl2(pdfUrl2);
            user.setUrl3(pdfUrl3);


        } catch (Exception e1) {
            Toast.makeText(getApplicationContext(), "error in data ", Toast.LENGTH_LONG).show();
        }


        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (awesomeValidation1.validate()) {
                    StringRequest postreq1 = new StringRequest(Request.Method.POST, Config.url4, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Boolean result = jsonObject.getBoolean("getsuccess");
                                if (result == false) {
                                    Toast.makeText(getApplicationContext(), "OTP not match", Toast.LENGTH_LONG).show();
                                } else {
                                    if (result == true) {
                                        //Toast.makeText(getApplicationContext(),"Email Allready exist, Please check your email",Toast.LENGTH_LONG).show();

                                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EnterOtpActivity.this);
                                        dialogBuilder.setTitle("Are you sure");
                                        LayoutInflater inflater = EnterOtpActivity.this.getLayoutInflater();
                                        final View dialogView = inflater.inflate(R.layout.alertdialog, null);
                                        dialogBuilder.setView(dialogView);

                                        final EditText password1 = (EditText) dialogView.findViewById(R.id.abc);
                                        final EditText cnfpassword = (EditText) dialogView.findViewById(R.id.cnfpassword);
                                        ok = (Button) dialogView.findViewById(R.id.okotp);
                                        no = (Button) dialogView.findViewById(R.id.cancle);
                                        ok.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                //try {
                                                    String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}";
                                                    //|| password1.getText().toString().matches(regexPassword)
                                                    if (!password1.getText().toString().matches(regexPassword)) {
                                                        password1.setError("Enter Valid Password");
                                                        //Toast.makeText(getApplicationContext(),"not valid pass",Toast.LENGTH_LONG).show();
                                                    }
                                                    else
                                                    {
                                                        if (password1.getText().toString().equals(cnfpassword.getText().toString())) {
                                                            user.setPassword(password1.getText().toString());
                                                            StringRequest postreq = new StringRequest(Request.Method.POST, Config.url5, new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    try {
                                                                        JSONObject jsonObject = new JSONObject(response);
                                                                        //String s1 = jsonObject.getString("success");
                                                                        Boolean result = jsonObject.getBoolean("success");

                                                                        if (result == true) {
                                                                            Toast.makeText(getApplicationContext(), "Password Change Successfully Now you can Login", Toast.LENGTH_LONG).show();
                                                                            Intent in = new Intent(getApplicationContext(), MerchantLoginActivity.class);
                                                                            startActivity(in);

                                                                        } else {
                                                                            Toast.makeText(getApplicationContext(), "Password changing  Failed", Toast.LENGTH_LONG).show();
                                                                        }

                                                                    } catch (Exception e1) {
                                                                        Log.e("k1", e1.toString());
                                                                        Toast.makeText(getApplicationContext(), "error changing Password", Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            }, new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {
                                                                    Toast.makeText(getApplicationContext(), "error connecting to server", Toast.LENGTH_LONG).show();
                                                                }
                                                            }) {
                                                                @Override
                                                                protected Map<String, String> getParams() throws AuthFailureError {
                                                                    Map<String, String> map = new HashMap<String, String>();

                                                                    map.put("id", user.getId() + "");
                                                                    map.put("pass", password1.getText().toString());
                                                                    // map.put("confirmpass", Changepass);
                                                                    return map;

                                                                }
                                                            };
                                                            AppController.getInstance().addToRequestQueue(postreq);

                                                        } else {
                                                            Toast.makeText(getApplicationContext(), "Password not matched", Toast.LENGTH_LONG).show();
                                                        }

                                                        // Toast.makeText(getApplicationContext(),"Enter Valid Password",Toast.LENGTH_LONG).show();
                                                    }
                                               // } catch (Exception e) {
                                               //     e.printStackTrace();
                                               // }

                                            }
                                        });


                                        no.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                password1.setText("");
                                                cnfpassword.setText("");
                                            }
                                        });
                                        AlertDialog alertDialog = dialogBuilder.create();
                                        alertDialog.show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "error status updation failed", Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (Exception e1) {
                                Log.e("k1", e1.toString());
                                Toast.makeText(getApplicationContext(), "error sending otp", Toast.LENGTH_LONG).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "error connecting to server", Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> map = new HashMap<String, String>();
                            map.put("id", user.getId() + "");
                            map.put("otp", edtotp.getText().toString());

                            return map;

                        }
                    };
                    AppController.getInstance().addToRequestQueue(postreq1);
                }

            }
        });


        btncancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtotp.setText("");
            }
        });
    }
}
