package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotpwdActivity extends AppCompatActivity {
    EditText edtemail;
    Button btnotp;
    private AwesomeValidation awesomeValidation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpwd);
        edtemail=(EditText)findViewById(R.id.edtemail);
        btnotp=(Button)findViewById(R.id.otp);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.edtemail, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        btnotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (awesomeValidation.validate()) {
                    sendOtp();

                }
            }
        });


    }
    public void sendOtp()
    {
        StringRequest postreq=new StringRequest(Request.Method.POST, Config.url3, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject j1 = new JSONObject(response);
                    String result = j1.getString("success");
                    //Boolean result=j1.getBoolean("success");
                    if (result.equals("[]")) {
                        Toast.makeText(getApplicationContext(), "Email does not exist", Toast.LENGTH_LONG).show();
                    }
                    else {
                        SharedPreferences sharedpreferences=getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("k",j1.toString());
                        editor.commit();
                        if (result.length()>0)
                        {

                            Toast.makeText(getApplicationContext(), "Check your mail for otp", Toast.LENGTH_LONG).show();
                            Intent in = new Intent(getApplicationContext(), EnterOtpActivity.class);
                            startActivity(in);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), " OTP generation Failed", Toast.LENGTH_LONG).show();
                        }

                    }
                }
                catch (Exception e1) {
                    Log.e("k1", e1.toString());
                    Toast.makeText(getApplicationContext(), "error in saving data", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"error connecting to server", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            protected Map<String,String> getParams()throws AuthFailureError {
                Map<String,String> map =new HashMap<String,String>();

                map.put("email",edtemail.getText().toString());

                return map;

            }
        };
        AppController.getInstance().addToRequestQueue(postreq);


    }
}
