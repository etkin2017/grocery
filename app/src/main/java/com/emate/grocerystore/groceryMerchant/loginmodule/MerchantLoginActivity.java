package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.develop.activities.models.PrefManager;
import com.emate.grocerystore.groceryMerchant.DashBoard;
import com.emate.grocerystore.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MerchantLoginActivity extends AppCompatActivity {
    EditText edtemail, edtpwd;
    TextView register, forgotpwd;
    Button merchantlogin;
    Intent in;
    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_login);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        edtemail = (EditText) findViewById(R.id.email);
        edtpwd = (EditText) findViewById(R.id.password);
        merchantlogin = (Button) findViewById(R.id.mlogin);

        awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}";
        awesomeValidation.addValidation(this, R.id.password, regexPassword, R.string.passerror);
        merchantlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (awesomeValidation.validate()) {
                    login();
                }
            }
        });

        forgotpwd = (TextView) findViewById(R.id.forgotpwd);
        forgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), ForgotpwdActivity.class);
                startActivity(in);
            }
        });

        register = (TextView) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                in = new Intent(getApplicationContext(), MerchantSignupActivity.class);
                startActivity(in);
            }
        });
    }

    public void login() {
        final String email = edtemail.getText().toString().trim();
        final String pwd = edtpwd.getText().toString().trim();
        StringRequest postrequest = new StringRequest(Request.Method.POST, Config.url2, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject j1 = new JSONObject(response);
                    //JSONArray jsrr= new JSONArray(response);
                    //j1.getJSONArray()
                    JSONArray result = j1.getJSONArray("success");
                    if (result.length() > 0) {
                        // Intent accountsIntent = new Intent(getApplicationContext(), HomeActivity.class);
                        // accountsIntent.putExtra("email", edtemail.getText().toString().trim());
                        // startActivity(accountsIntent);
                        // finish();
                        Toast.makeText(getApplicationContext(), "Login successfull", Toast.LENGTH_LONG).show();
                        SharedPreferences sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("key", result.getJSONObject(0).toString());
                        editor.commit();

                        JSONObject jobject = result.getJSONObject(0);
                        PrefManager prefManager = new PrefManager(MerchantLoginActivity.this);
                        prefManager.setWhoLogin(com.emate.grocerystore.Config.MERCHANT);
                        prefManager.setIsUserLoggedIn(true);
                        prefManager.setMobileNumber(jobject.getString("mob"));
                        prefManager.setUserEmail(jobject.getString("email"));
                        prefManager.setUserName(jobject.getString("name"));
                        prefManager.setUserID(jobject.getString("id"));
                        startActivity(new Intent(MerchantLoginActivity.this, DashBoard.class));
                        finish();


                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid email and pass", Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e1) {
                    Toast.makeText(getApplicationContext(), "error in login", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error connecting to server", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("email", email);
                map.put("pass", pwd);
                return map;
            }
        };
        AppController.getInstance().addToRequestQueue(postrequest);
    }

    @Override
    public void onBackPressed() {
       /* Intent startMain = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(startMain);
        finish();*/
    }


}

