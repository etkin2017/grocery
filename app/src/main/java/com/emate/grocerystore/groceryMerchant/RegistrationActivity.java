package com.emate.grocerystore.groceryMerchant;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;

import org.json.JSONObject;

public class RegistrationActivity extends AppCompatActivity {


    EditText etName, etEmail, etPassword, etConfPassword, etMobile, etAddress;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_merchant);
        init();
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(etName.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Name",RegistrationActivity.this);
                } else if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Email",RegistrationActivity.this);
                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Password",RegistrationActivity.this);
                } else if (TextUtils.isEmpty(etConfPassword.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Confirmed Password",RegistrationActivity.this);
                } else if (TextUtils.isEmpty(etMobile.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Mobile",RegistrationActivity.this);
                } else if (TextUtils.isEmpty(etAddress.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Address",RegistrationActivity.this);
                } else {
                    registerMerchant();
                }
            }
        });
    }

    public void init() {
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfPassword = (EditText) findViewById(R.id.etConfPassword);
        etMobile = (EditText) findViewById(R.id.etMobile);
        etAddress = (EditText) findViewById(R.id.etAddress);
        btnRegister = (Button) findViewById(R.id.btnRegister);
    }

    public void registerMerchant() {


        final ProgressDialog pDialog = new ProgressDialog(RegistrationActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = AppController.Api_Url + "registerMerchant";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            //name,email,password,mobile,pan,address,pin,cname,status,regidate

            jsonObject.put("name", etName.getText().toString());
            jsonObject.put("email", etEmail.getText().toString());
            jsonObject.put("password", etPassword.getText().toString());
            jsonObject.put("mobile", etMobile.getText().toString());
            jsonObject.put("pan", "0313");
            jsonObject.put("address", etAddress.getText().toString());

            jsonObject.put("pin", "87356");
            jsonObject.put("cname", "CSK Mart");
            jsonObject.put("regidate", System.currentTimeMillis() + "");


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    //JSONArray jsonArray = response.getJSONArray("success");

                    boolean b = response.getBoolean("success");
                    if (b == true) {

                        AppController.successDialog("Successfully Registered", RegistrationActivity.this);


                    } else {
                        AppController.retShowAlertDialod("Failed To Register , User Already Registered With Email-Id Or Mobile Number", RegistrationActivity.this);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(RegistrationActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "register_user");

    }
}
