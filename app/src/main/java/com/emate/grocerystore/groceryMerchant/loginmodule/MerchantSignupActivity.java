package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.emate.grocerystore.AppController;

import com.emate.grocerystore.R;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MerchantSignupActivity extends AppCompatActivity implements OnItemSelectedListener {
    Button btnsubmit;
    EditText edtname, edtmob, edtemail, edtpwd, edtshop, edtadd, edtgst, edtadd2, edtland, edtcity, edtstate, edtpin;

    ProgressDialog progressDialog;
    Button SelectButton1, SelectButton2, SelectButton3;
    Uri uri1, uri2, uri3;

    public static final String PDF_UPLOAD_HTTP_URL = Config.url1;

    public int PDF_REQ_CODE1 = 1;
    public int PDF_REQ_CODE2 = 2;
    public int PDF_REQ_CODE3 = 3;

    String PdfPathHolder, PdfPathHolder2, PdfPathHolder3;

    private AwesomeValidation awesomeValidation;
    private Spinner spin;
    // private static final String TAG = "Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_signup);


        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        edtname = (EditText) findViewById(R.id.name);
        edtmob = (EditText) findViewById(R.id.mob);
        edtemail = (EditText) findViewById(R.id.email);
        edtpwd = (EditText) findViewById(R.id.password);
        edtshop = (EditText) findViewById(R.id.shop);
        edtadd = (EditText) findViewById(R.id.address);
        edtadd2 = (EditText) findViewById(R.id.address2);
        edtland = (EditText) findViewById(R.id.landmark);
        edtcity = (EditText) findViewById(R.id.city);
        edtstate = (EditText) findViewById(R.id.state);
        edtpin = (EditText) findViewById(R.id.pin);
        edtgst = (EditText) findViewById(R.id.gst);
        spin = (Spinner) findViewById(R.id.spin);


        AllowRunTimePermission();

        SelectButton1 = (Button) findViewById(R.id.btnbrowse);


        SelectButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent1 = new Intent();

                //intent1.setType("image/*");
                String[] mimeTypes = {"application/pdf", "image/jpeg"};
                intent1.setType("*/*");
                intent1.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

                intent1.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent1, "Select file"), PDF_REQ_CODE1);


            }
        });
        SelectButton2 = (Button) findViewById(R.id.btnbrowse1);


        SelectButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent2 = new Intent();

                String[] mimeTypes = {"application/pdf", "image/jpeg"};
                intent2.setType("*/*");
                intent2.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                intent2.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent2, "Select file"), PDF_REQ_CODE2);
            }
        });
        SelectButton3 = (Button) findViewById(R.id.btnbrowse2);


        SelectButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // PDF selection code start from here .

                Intent intent3 = new Intent();

                String[] mimeTypes = {"application/pdf", "image/jpeg"};
                intent3.setType("*/*");
                intent3.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);

                intent3.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent3, "Select file"), PDF_REQ_CODE3);

            }
        });

        btnsubmit = (Button) findViewById(R.id.btnsubmit);


        awesomeValidation.addValidation(this, R.id.name, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.nameerror);
        awesomeValidation.addValidation(this, R.id.mob, "[0-9]{10}", R.string.mobileerror);
        awesomeValidation.addValidation(this, R.id.email, Patterns.EMAIL_ADDRESS, R.string.emailerror);
        String regexPassword = "(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{8,}";
        awesomeValidation.addValidation(this, R.id.password, regexPassword, R.string.passerror);
        awesomeValidation.addValidation(this, R.id.shop, "^[A-Za-z0-9\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.shoperror);
        awesomeValidation.addValidation(this, R.id.address, "^[A-Za-z0-9\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.addresserror);
        awesomeValidation.addValidation(this, R.id.address2, "^[A-Za-z0-9\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.address2error);
        awesomeValidation.addValidation(this, R.id.city, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.cityerror);
        awesomeValidation.addValidation(this, R.id.state, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.stateerror);
        awesomeValidation.addValidation(this, R.id.pin, "[0-9]{6}", R.string.pinerror);
        awesomeValidation.addValidation(this, R.id.gst, "^[A-Z0-9\\s]{15}$", R.string.gsterror);
        //awesomeValidation.addValidation(this, R.id.spin, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.saleerror);


        //Spinner
        spin.setOnItemSelectedListener(this);
        String[] arr = getResources().getStringArray(R.array.sale);
        customadapter customAdapter = new customadapter(getApplicationContext(), arr);
        spin.setAdapter(customAdapter);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*try {
                    String abv = ((TextView) spin.getSelectedView().findViewById(R.id.text1)).getText().toString();
                    Log.e("ADV", abv);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                if (awesomeValidation.validate()) {

                    if (spin.getSelectedItemPosition() == 0) {
                        ((TextView) spin.getSelectedView().findViewById(R.id.text1)).setError("Choose Valid Item");
                        // Toast.makeText(getApplicationContext(), )
                    } else {

                        view.setBackgroundColor(Color.parseColor("#1A237E"));
                        btnsubmit.setTextColor(Color.parseColor("#ffffff"));
                        //view.setEnabled(false);

                        progressDialog = new ProgressDialog(MerchantSignupActivity.this);
                        progressDialog.setMessage("Loading..."); // Setting Message
                        progressDialog.setTitle("Submitting Your Data"); // Setting Title
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
                        progressDialog.show(); // Display Progress Dialog
                        progressDialog.setCancelable(false);
                        progressDialog.setMax(300);

                        StringRequest postreq = new StringRequest(Request.Method.POST, Config.url, new Response.Listener<String>() {

                            @Override
                            public void onResponse(String response) {
                                try {
                                    progressDialog.dismiss();
                                    JSONObject jsonObject = new JSONObject(response);
                                    String result = jsonObject.getString("success");
                                    if (result.equals("[]")) {
                                        PdfUploadFunction();

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Email or Mobile Number Allready exist, Please check your email", Toast.LENGTH_LONG).show();
                                    }

                                } catch (Exception e1) {
                                    Log.e("k1", e1.toString());
                                    Toast.makeText(getApplicationContext(), "error ", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getApplicationContext(), "error connecting to server", Toast.LENGTH_LONG).show();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> map = new HashMap<String, String>();
                                map.put("mob", edtmob.getText().toString());
                                map.put("email", edtemail.getText().toString());

                                return map;
                            }
                        };

                        AppController.getInstance().addToRequestQueue(postreq);


                    }


                }
                //insert();
            }

        });


    }//oncreate







   /* public void insert() {


        final StringRequest postreq = new StringRequest(Request.Method.POST, Config.url1, new Response.Listener<String>(){
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String s1 = jsonObject.getString("success");

                    if (s1.equals("1")) {
                        Toast.makeText(getApplicationContext(), "Email Allready exist, Please check your email", Toast.LENGTH_LONG).show();
                    } else if (s1.equals("2")) {

                        PdfPathHolder = FilePath.getPath(getApplicationContext(), uri);

                        if (PdfPathHolder == null) {

                            Toast.makeText(getApplicationContext(), "Please move your PDF file to internal storage & try again.", Toast.LENGTH_LONG).show();

                        } else {

                            try {

                                new MultipartUploadRequest(getApplicationContext(),Config.url1)
                                        .addFileToUpload(PdfPathHolder, "pdf")
                                        .setNotificationConfig(new UploadNotificationConfig())
                                        .setMaxRetries(5)
                                        .startUpload();

                            } catch (Exception exception) {

                                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        Toast.makeText(getApplicationContext(), "Data Inserted Successfully", Toast.LENGTH_LONG).show();
                        Intent in = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(in);


                    } else {
                        Toast.makeText(getApplicationContext(), "Data Insertion Failed", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e1) {
                    Log.e("k1", e1.toString());
                    Toast.makeText(getApplicationContext(), "error inserting data", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "error connecting to server", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                map.put("name", edtname.getText().toString());
                 map.put("mob", edtmob.getText().toString());
                map.put("email", edtemail.getText().toString());
                 map.put("pass", edtpwd.getText().toString());
                map.put("shop", edtshop.getText().toString());
                map.put("address", edtadd.getText().toString());
                map.put("gst", edtgst.getText().toString());
                 map.put("sale", spin.getText().toString());
                return map;
               }
            };

         MyApplication.getInstance().addToReqQueue(postreq);

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PDF_REQ_CODE1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri1 = data.getData();

            SelectButton1.setText("Selected");
        }
        if (requestCode == PDF_REQ_CODE2 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri2 = data.getData();

            SelectButton2.setText("Selected");
        }
        if (requestCode == PDF_REQ_CODE3 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri3 = data.getData();

            SelectButton3.setText("Selected");
        }

    }

    public void PdfUploadFunction() {


        try {
            PdfPathHolder = FilePath.getPath(this, uri1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PdfPathHolder2 = FilePath.getPath(this, uri2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            PdfPathHolder3 = FilePath.getPath(this, uri3);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (PdfPathHolder == null || PdfPathHolder2 == null || PdfPathHolder3 == null) {

            Toast.makeText(getApplicationContext(), "You have to choose all files", Toast.LENGTH_LONG).show();

        } else {

            try {

                SharedPreferences sh = getSharedPreferences(com.emate.grocerystore.Config.REFRESHTOKEN, Context.MODE_PRIVATE);
                String refershToken = sh.getString(com.emate.grocerystore.Config.TOKEN, "");


                MultipartUploadRequest mup = new MultipartUploadRequest(this, PDF_UPLOAD_HTTP_URL);

                mup.addFileToUpload(PdfPathHolder, "pdf");
                mup.addFileToUpload(PdfPathHolder2, "pdf2");
                mup.addFileToUpload(PdfPathHolder3, "pdf3");

                mup.addParameter("name", edtname.getText().toString());
                mup.addParameter("mob", edtmob.getText().toString());
                mup.addParameter("email", edtemail.getText().toString());
                mup.addParameter("pass", edtpwd.getText().toString());
                mup.addParameter("shop", edtshop.getText().toString());
                mup.addParameter("address", edtadd.getText().toString());
                mup.addParameter("address2", edtadd2.getText().toString());
                mup.addParameter("landmark", edtland.getText().toString());
                mup.addParameter("city", edtcity.getText().toString());
                mup.addParameter("state", edtstate.getText().toString());
                mup.addParameter("pin", edtpin.getText().toString());
                mup.addParameter("gst", edtgst.getText().toString());
                mup.addParameter("sale", ((TextView) spin.getSelectedView().findViewById(R.id.text1)).getText().toString());
                mup.addParameter("regId", refershToken);
                mup.setNotificationConfig(new UploadNotificationConfig());
                mup.setMaxRetries(5);
                mup.startUpload();


                Toast.makeText(getApplicationContext(), "Data Inserted Successfully", Toast.LENGTH_LONG).show();
                Intent in = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(in);


            } catch (Exception exception) {

                Toast.makeText(this, exception.getMessage(), Toast.LENGTH_SHORT).show();
            }


        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(MerchantSignupActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MerchantSignupActivity.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.CAMERA, android.Manifest.permission.READ_CONTACTS},
                    110);

        }
    }


    public void AllowRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(MerchantSignupActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(MerchantSignupActivity.this, "READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(MerchantSignupActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Canceled", Toast.LENGTH_LONG).show();

                }
            case 2:
                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Canceled", Toast.LENGTH_LONG).show();

                }
            case 3:
                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(MerchantSignupActivity.this, "Permission Canceled", Toast.LENGTH_LONG).show();

                }

                break;
        }

    }

    @Override
    public void onBackPressed() {
        Intent startMain = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(startMain);
        finish();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}
