package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioButton;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.SetLocation;

public class LoginActivity extends AppCompatActivity {
    Intent in;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_merchant);


    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radioButton1:
                if (checked)
                    in= new Intent(getApplicationContext(),SetLocation.class);
                startActivity(in);
                    break;
            case R.id.radioButton2:
                if (checked)
                    // Ninjas rule
                    in= new Intent(getApplicationContext(),SetLocation.class);
                startActivity(in);
                    break;
            case R.id.radioButton3:
                if (checked)
                    in= new Intent(getApplicationContext(),MerchantLoginActivity.class);
                startActivity(in);
                finish();
                break;
        }
    }
}
