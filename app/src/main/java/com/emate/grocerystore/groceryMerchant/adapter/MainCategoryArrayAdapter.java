package com.emate.grocerystore.groceryMerchant.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.groceryMerchant.entities.MainCategoryModel;

import java.util.List;

public class MainCategoryArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<MainCategoryModel> items;
    private final int mResource;

    public MainCategoryArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                                    @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txt_nm = (TextView) view.findViewById(R.id.txt_nm);
        TextView txt_id = (TextView) view.findViewById(R.id.txt_id);


        MainCategoryModel mainCategoryModel = items.get(position);

        txt_nm.setText(mainCategoryModel.getSub_cat_name());
        txt_id.setText(mainCategoryModel.getSub_cat_id());
        return view;
    }
}