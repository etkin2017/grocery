package com.emate.grocerystore.groceryMerchant;

/**
 * Created by Wasim on 13/01/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "GroceryMerchant";


    private static final String IS_MERCHANT_LOGGED_IN = "IsUserLoggedIn";
    private static final String MERCHANT_NAME = "UserName";
    private static final String MERCHANT_ID = "UserID";
    private static final String MERCHANT_MOBILENO = "UserMobileNo";
    private static final String MERCHANT_EMAIL = "UserEmail";
    private static final String STORE_MERCHANT = "MerchantJson";




    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void setIsMerchantLoggedIn(boolean isUserLoggedIn) {
        editor.putBoolean(IS_MERCHANT_LOGGED_IN, isUserLoggedIn);
        editor.commit();
    }

    public void setMerchantName(String username) {
        editor.putString(MERCHANT_NAME, username);
        editor.commit();
    }

    public void setMerchantId(String id) {
        editor.putString(MERCHANT_ID, id);
        editor.commit();
    }

    public void setMerchantMobileno(String mobileNumber) {
        editor.putString(MERCHANT_MOBILENO, mobileNumber);
        editor.commit();
    }

    public void setMerchantEmail(String email) {
        editor.putString(MERCHANT_EMAIL, email);
        editor.commit();
    }

    public void setStoreMerchant(String json){
        editor.putString(STORE_MERCHANT, json);
        editor.commit();
    }

    public String getStoreMerchant(){
        return pref.getString(STORE_MERCHANT, "");
    }



    public String getMobileNumber() {
        return pref.getString(MERCHANT_MOBILENO, "");

    }

    public String getUserEmail() {
        return pref.getString(MERCHANT_EMAIL, "");

    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_MERCHANT_LOGGED_IN, false);
    }

    public String getUsername() {
        return pref.getString(MERCHANT_NAME, "");
    }

    public String getID() {
        return pref.getString(MERCHANT_ID, "");
    }
}

