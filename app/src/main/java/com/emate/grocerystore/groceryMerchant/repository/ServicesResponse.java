package com.emate.grocerystore.groceryMerchant.repository;

import com.emate.grocerystore.groceryMerchant.entities.MainCategoryModel;
import com.emate.grocerystore.groceryMerchant.entities.ProductModel;
import com.emate.grocerystore.groceryMerchant.entities.SubCategoryModel;
import com.emate.grocerystore.groceryMerchant.entities.SuperCategoryModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;


/**
 * Created by Ashvini on 2/21/2018.
 */

public class ServicesResponse {


    @SerializedName("productList")
    private List<ProductModel> productList;


    @SerializedName("success")
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<ProductModel> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductModel> productList) {
        this.productList = productList;
    }


    @SerializedName("sub_category_details")
    List<SubCategoryModel> subCategoryModelList;

    @SerializedName("category")
    List<SuperCategoryModel> superCategoryModelList;

    @SerializedName("sub_category")
    List<MainCategoryModel> mainCategoryModelList;


    public List<SubCategoryModel> getSubCategoryModelList() {
        return subCategoryModelList;
    }

    public void setSubCategoryModelList(List<SubCategoryModel> subCategoryModelList) {
        this.subCategoryModelList = subCategoryModelList;
    }

    public List<SuperCategoryModel> getSuperCategoryModelList() {
        return superCategoryModelList;
    }

    public void setSuperCategoryModelList(List<SuperCategoryModel> superCategoryModelList) {
        this.superCategoryModelList = superCategoryModelList;
    }

    public List<MainCategoryModel> getMainCategoryModelList() {
        return mainCategoryModelList;
    }

    public void setMainCategoryModelList(List<MainCategoryModel> mainCategoryModelList) {
        this.mainCategoryModelList = mainCategoryModelList;
    }
}
