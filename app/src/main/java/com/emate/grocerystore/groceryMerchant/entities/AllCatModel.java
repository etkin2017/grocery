package com.emate.grocerystore.groceryMerchant.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Etkin King on 2/23/2018.
 */

public class AllCatModel {

    @SerializedName("sub_category_details")
    List<SubCategoryModel> subCategoryModelList;

    @SerializedName("category")
    List<SuperCategoryModel> superCategoryModelList;

    @SerializedName("sub_category")
    List<MainCategoryModel> mainCategoryModelList;


    public List<SubCategoryModel> getSubCategoryModelList() {
        return subCategoryModelList;
    }

    public void setSubCategoryModelList(List<SubCategoryModel> subCategoryModelList) {
        this.subCategoryModelList = subCategoryModelList;
    }

    public List<SuperCategoryModel> getSuperCategoryModelList() {
        return superCategoryModelList;
    }

    public void setSuperCategoryModelList(List<SuperCategoryModel> superCategoryModelList) {
        this.superCategoryModelList = superCategoryModelList;
    }

    public List<MainCategoryModel> getMainCategoryModelList() {
        return mainCategoryModelList;
    }

    public void setMainCategoryModelList(List<MainCategoryModel> mainCategoryModelList) {
        this.mainCategoryModelList = mainCategoryModelList;
    }
}
