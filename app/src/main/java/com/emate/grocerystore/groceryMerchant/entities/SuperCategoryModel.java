package com.emate.grocerystore.groceryMerchant.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Etkin King on 2/23/2018.
 */

public class SuperCategoryModel {

    @SerializedName("id")
    String id;

    @SerializedName("category_type")
    String category_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_type() {
        return category_type;
    }

    public void setCategory_type(String category_type) {
        this.category_type = category_type;
    }
}
