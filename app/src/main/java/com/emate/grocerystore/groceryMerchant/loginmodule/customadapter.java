package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.emate.grocerystore.R;

/**
 * Created by user on 20-03-2018.
 */

public class customadapter extends BaseAdapter {


    Context context;

    String[] array;
    LayoutInflater inflter;

    public customadapter(Context applicationContext, String[] array) {
        this.context = applicationContext;
      this.array=array;
       // this.countryNames = countryNames;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return array.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_item, null);

        TextView names = (TextView) view.findViewById(R.id.text1);
        names.setText(array[i]);

        if(i==0)
        {
            names.setTextColor(Color.GRAY);
        }
        return view;

    }
}
