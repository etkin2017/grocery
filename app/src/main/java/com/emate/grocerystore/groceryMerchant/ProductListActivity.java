package com.emate.grocerystore.groceryMerchant;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;


import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.groceryMerchant.adapter.MainCategoryArrayAdapter;
import com.emate.grocerystore.groceryMerchant.adapter.ProductAdapter;
import com.emate.grocerystore.groceryMerchant.adapter.SubCategoryArrayAdapter;
import com.emate.grocerystore.groceryMerchant.adapter.SuperCategoryArrayAdapter;
import com.emate.grocerystore.groceryMerchant.entities.MainCategoryModel;
import com.emate.grocerystore.groceryMerchant.entities.ProductModel;
import com.emate.grocerystore.groceryMerchant.entities.SubCategoryModel;
import com.emate.grocerystore.groceryMerchant.entities.SuperCategoryModel;
import com.emate.grocerystore.groceryMerchant.repository.ApiServices;
import com.emate.grocerystore.groceryMerchant.repository.ServiceFactory;
import com.emate.grocerystore.groceryMerchant.repository.ServicesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductListActivity extends AppCompatActivity {
    String url = AppController.Api_Url;
    RecyclerView prod_list;
    ProgressDialog pd;
    List<MainCategoryModel> mainCategoryModelList1 = new ArrayList<>();
    List<SubCategoryModel> subCategoryModelList1 = new ArrayList<>();
    Spinner category_spinner, maincategory_spinner, subcategory_spinner;
    List<SuperCategoryModel> superCategoryModelList11 = new ArrayList<>();
    List<MainCategoryModel> mainCategoryModelList11 = new ArrayList<>();
    List<SubCategoryModel> subCategoryModelList11 = new ArrayList<>();
    Button search;
    ImageButton done;
    ProductModel pm = new ProductModel();
    List<ProductModel> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list_merchant);
        prod_list = (RecyclerView) findViewById(R.id.prod_list);
        pd = new ProgressDialog(ProductListActivity.this);
        subcategory_spinner = (Spinner) findViewById(R.id.subcategory_spinner);
        category_spinner = (Spinner) findViewById(R.id.category_spinner);
        maincategory_spinner = (Spinner) findViewById(R.id.maincategory_spinner);
        search = (Button) findViewById(R.id.search);
        done = (ImageButton) findViewById(R.id.done);
        getCategories();

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = ((TextView) view.findViewById(R.id.txt_id)).getText().toString();
                pm.setCat_id(item);
                getMainCategoryList_Spinner(item);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        maincategory_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = ((TextView) view.findViewById(R.id.txt_id)).getText().toString();
                pm.setSubcat_id(item);
                getSubCategoryList_Spinner(item);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subcategory_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String item = ((TextView) view.findViewById(R.id.txt_id)).getText().toString();
                pm.setDet_cat_id(item);
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (maincategory_spinner.getAdapter().getCount() == 0) {
                    pm.setSubcat_id("null");
                }

                if (subcategory_spinner.getAdapter().getCount() == 0) {
                    pm.setDet_cat_id("null");
                }
                getProList();
            }
        });

       /* prod_list.addOnItemTouchListener(
                new RecyclerItemClickListener(ctx, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        // TODO Handle item click

                      //  CheckBox prod_chk = (CheckBox) view.findViewById(R.id.prod_chk);


                    }
                })
        );*/


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }

    public void getProList() {

        ApiServices myApiServices = ServiceFactory.makeService(url);
        try {

            //     pm.setCat_id("12");
            pd.show();
            Call<ServicesResponse> call = myApiServices.getProductList(pm);
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    pd.dismiss();
                    try {
                        productList = response.body().getProductList();
                        ProductAdapter adapter = new ProductAdapter(ProductListActivity.this, productList, done,1);
                        prod_list.setLayoutManager(new LinearLayoutManager(ProductListActivity.this));
                        prod_list.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    pd.dismiss();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void getCategories() {

        ApiServices myApiServices = ServiceFactory.makeService(url + "/getMerchantProducts/");
        try {

            pd.show();
            Call<ServicesResponse> call = myApiServices.getCategory();
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    pd.dismiss();
                    try {
                        superCategoryModelList11 = response.body().getSuperCategoryModelList();
                        mainCategoryModelList11 = response.body().getMainCategoryModelList();
                        subCategoryModelList11 = response.body().getSubCategoryModelList();


                        SuperCategoryArrayAdapter adapter = new SuperCategoryArrayAdapter(ProductListActivity.this,
                                R.layout.spinner_layout, superCategoryModelList11);
                        category_spinner.setAdapter(adapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    pd.dismiss();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getMainCategoryList_Spinner(String sup_cat_id) {

        List<MainCategoryModel> mcList = new ArrayList<>();

        for (int i = 0; i < mainCategoryModelList11.size(); i++) {
            if (mainCategoryModelList11.get(i).getId().equalsIgnoreCase(sup_cat_id)) {
                mcList.add(mainCategoryModelList11.get(i));
            }
        }
        // return mcList;


        MainCategoryArrayAdapter adapter = new MainCategoryArrayAdapter(this,
                R.layout.spinner_layout, mcList);
        maincategory_spinner.setAdapter(adapter);
    }

    public void getSubCategoryList_Spinner(String main_cat_id) {

        List<SubCategoryModel> scList = new ArrayList<>();

        for (int i = 0; i < subCategoryModelList11.size(); i++) {
            if (subCategoryModelList11.get(i).getSub_cat_id().equalsIgnoreCase(main_cat_id)) {
                scList.add(subCategoryModelList11.get(i));
            }
        }
        // return mcList;


        SubCategoryArrayAdapter adapter = new SubCategoryArrayAdapter(this,
                R.layout.spinner_layout, scList);
        subcategory_spinner.setAdapter(adapter);
    }
}
