package com.emate.grocerystore.groceryMerchant.repository;


import com.emate.grocerystore.groceryMerchant.entities.ProductModel;

import org.json.JSONArray;
import org.json.JSONStringer;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ashvini on 2/21/2018.
 */
public interface ApiServices {


       /*--------------------- FOR CATEGORY -----------------------*/

    @GET("http://192.168.0.109/grossary/mobile_app/webservice/categories/")
    Call<ServicesResponse> getCategory();

    @POST("http://192.168.0.109/grossary/mobile_app/webservice/getMerchantProducts")
    Call<ServicesResponse> getProductList(@Body ProductModel cat);

    //removeMerchantProducts
    @POST("http://192.168.0.109/grossary/mobile_app/webservice/removeMerchantProducts")
    Call<ServicesResponse> removeMerchantProducts(@Body ProductModel cat);

    @POST("http://192.168.0.109/grossary/mobile_app/webservice/addMerProd")
    Call<ServicesResponse> addMerchantProduct(@Body JSONArray jsonObject);

    @POST("http://192.168.0.109/grossary/mobile_app/webservice/addMerProd")
    Call<ServicesResponse> CHECKIN(@Body JSONStringer data);

}