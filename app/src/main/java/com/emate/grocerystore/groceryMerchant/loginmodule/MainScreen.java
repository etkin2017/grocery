package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.SetLocation;
import com.emate.grocerystore.develop.activities.activities.SetLocaton;
import com.emate.grocerystore.develop.activities.models.PrefManager;

public class MainScreen extends AppCompatActivity {
    TextView merchant_access;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        merchant_access = (TextView) findViewById(R.id.merchant_access);
        merchant_access.setText(Html.fromHtml("<u>Merchant Sign up/Login</u>"));
    }

    public void superMarket(View view) {
        startActivity(new Intent(MainScreen.this, SetLocation.class));
    }

    public void merchnat_login(View view) {
        startActivity(new Intent(MainScreen.this, MerchantLoginActivity.class));
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(MainScreen.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainScreen.this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.CAMERA, android.Manifest.permission.READ_CONTACTS},
                    110);

        }
    }

}
