package com.emate.grocerystore.groceryMerchant.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.groceryMerchant.entities.ProductModel;
import com.emate.grocerystore.groceryMerchant.entities.RecyclerItemClickListener;
import com.emate.grocerystore.groceryMerchant.entities.UnitModel;
import com.emate.grocerystore.groceryMerchant.expandableview.ExpandableRelativeLayout;
import com.emate.grocerystore.groceryMerchant.repository.ApiServices;
import com.emate.grocerystore.groceryMerchant.repository.ServiceFactory;
import com.emate.grocerystore.groceryMerchant.repository.ServicesResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Ashvini on 2/21/2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {

    Context ctx;
    List<ProductModel> productModelList;
    ImageButton done;
    int chk;
    String merchant_id = "";

    public ProductAdapter(Context ctx, final List<ProductModel> productModelList, ImageButton done, final int chk) {
        this.ctx = ctx;
        this.productModelList = productModelList;
        this.done = done;
        this.chk = chk;
        SharedPreferences sharedpreferences = ctx.getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
        String merchant_data = sharedpreferences.getString("key", "");

        try {
            JSONObject jobj1 = new JSONObject(merchant_data);
            merchant_id = jobj1.getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONArray jsonArray = new JSONArray();
                //`merchant_id``prod_id``unit_ids`
                for (int y = 0; y < productModelList.size(); y++) {
                    if (chk == 1) {
                        if (productModelList.get(y).isChk_prod()) {
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("merchant_id", merchant_id);
                                jsonObject.put("prod_id", productModelList.get(y).getProduct_id());
                                jsonObject.put("unit_ids", getUnitIds(productModelList.get(y)));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            jsonArray.put(jsonObject);

                        }
                    } else {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("merchant_id", merchant_id);
                            jsonObject.put("prod_id", productModelList.get(y).getProduct_id());
                            jsonObject.put("unit_ids", getUnitIds(productModelList.get(y)));
                            jsonObject.put("chk", productModelList.get(y).isChk_prod() + "");
                            //   jsonObject.put("id",)
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jsonArray.put(jsonObject);

                    }
                }

                try {
                    // JSONObject j2 = new JSONObject(jsonArray.toString());

                    JSONObject jp = new JSONObject();
                    jp.put("data", jsonArray);

                    add(jp);


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

    }

    public String getUnitIds(ProductModel pm2) {
        String unitIds = "";
        List<UnitModel> unitModelList = pm2.getUnitModelList();
        for (int a = 0; a < unitModelList.size(); a++) {
            if (unitModelList.get(a).isCheck()) {
                unitIds = unitIds + unitModelList.get(a).getUnit_id() + ",";
            }
        }
        return unitIds;//.substring(unitIds.length() - 1);
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_row_merchant, parent, false);
        return new ProductHolder(view);
    }


    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
      /*  if (chk == 0) {
            holder.prod_chk.setChecked(true);
        } else {*/
        holder.prod_chk.setChecked(productModelList.get(position).isChk_prod());
        //  }

        holder.prod_nm.setText(productModelList.get(position).getTitle());
        final UnitAdapter unitAdapter = new UnitAdapter(ctx, productModelList.get(position).getUnitModelList(), productModelList.get(position).getUnit_ids(), chk);
        holder.unit_list.setLayoutManager(new LinearLayoutManager(ctx));
        holder.unit_list.setAdapter(unitAdapter);
        holder.expand_item.collapse();
        holder.show_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.expand_item.isExpanded()) {
                    holder.expand_item.collapse();
                    holder.show_view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_less, 0);
                } else {
                    holder.expand_item.expand();
                    holder.show_view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expand_more, 0);

                }
            }
        });
        final int pos = position;
        holder.unit_list.addOnItemTouchListener(
                new RecyclerItemClickListener(ctx, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        // TODO Handle item click
                        CheckBox cb = (CheckBox) view.findViewById(R.id.chk_prod);


                        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    //   holder.prod_chk.setChecked(true);
                                    productModelList.get(pos).getUnitModelList().get(position).setCheck(true);
                                } else {
                                    productModelList.get(pos).getUnitModelList().get(position).setCheck(false);
                                }
                                List<UnitModel> umList = productModelList.get(pos).getUnitModelList();
                                if (checkUlist(umList)) {
                                    holder.prod_chk.setChecked(true);
                                } else {
                                    holder.prod_chk.setChecked(false);
                                }

                            }
                        });

                    }
                })
        );
        holder.prod_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                productModelList.get(pos).setChk_prod(b);
                List<UnitModel> umList = productModelList.get(pos).getUnitModelList();
                if (b == true && checkUlist(umList) == false) {
                    unitAdapter.setUnitModelList(setUnit(productModelList.get(pos).getUnitModelList(), b));
                } else if (b == false) {
                    unitAdapter.setUnitModelList(setUnit(productModelList.get(pos).getUnitModelList(), false));
                }
            }
        });


    }

    public boolean checkUlist(List<UnitModel> umList) {
        boolean a = false;
        for (int u = 0; u < umList.size(); ) {
            if (umList.get(u).isCheck()) {
                a = true;
                u = umList.size();
            } else {
                u++;
            }
        }
        return a;
    }

    public List<UnitModel> setUnit(List<UnitModel> umList, boolean a) {

        for (int u = 0; u < umList.size(); u++) {
            umList.get(u).setCheck(a);
        }
        return umList;
    }

    @Override
    public int getItemCount() {
        return productModelList.size();
    }

    public class ProductHolder extends ViewHolder {
        TextView prod_nm, show_view;
        RecyclerView unit_list;
        CheckBox prod_chk;
        ExpandableRelativeLayout expand_item;


        public ProductHolder(View itemView) {
            super(itemView);
            show_view = (TextView) itemView.findViewById(R.id.show_view);
            prod_nm = (TextView) itemView.findViewById(R.id.prod_nm);
            unit_list = (RecyclerView) itemView.findViewById(R.id.unit_list);
            expand_item = (ExpandableRelativeLayout) itemView.findViewById(R.id.expand_item);
            prod_chk = (CheckBox) itemView.findViewById(R.id.prod_chk);

        }
    }


    public void add(JSONObject js) {


        final ProgressDialog pDialog = new ProgressDialog(ctx);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = "";//AppController.Api_Url + "addMerProd";// "getProductBySuperCategory"; //getProd

        if (chk == 0) {
            url = Config.Api_Url + "removeProduct";
        } else {
            url = Config.Api_Url + "addMerProd";
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, js, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    boolean t1 = response.getBoolean("success");
                    Toast.makeText(ctx, t1 + "", Toast.LENGTH_LONG).show();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ctx, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "logim");

    }

    public void addMerchantProd(JSONArray jobj) {
        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("Loding");

        ApiServices myApiServices = ServiceFactory.makeService(Config.Api_Url + "/addMerProd/");
        try {

            //     pm.setCat_id("12");
            pd.show();
            Call<ServicesResponse> call = myApiServices.addMerchantProduct(jobj);
            call.enqueue(new Callback<ServicesResponse>() {
                @Override
                public void onResponse(Call<ServicesResponse> call, Response<ServicesResponse> response) {
                    pd.dismiss();
                    try {
                        boolean succ = response.body().isSuccess();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ServicesResponse> call, Throwable t) {
                    t.printStackTrace();
                    pd.dismiss();
                }


            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
