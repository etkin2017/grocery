package com.emate.grocerystore.groceryMerchant.loginmodule;

/**
 * Created by user on 09-03-2018.
 */

public class User {

    private int id;
    private String name;
    private String mobno;
    private String email;
    private String password;
    private String shop;
    private String address;
    private String area;
    private String landmark;
    private String city;
    private String state;
    private int pin;
    private String gst;
    private String sale;
    private String url1;
    private String url2;
    private String url3;
    private String otp;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {

        this.mobno = mobno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }


    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark =landmark;

    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city =city;

    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state =state;

    }
    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }


    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getSale() {
        return sale;
    }

    public void setSale(String sale) {
        this.sale = sale;
    }

    public String getUrl1() {

        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }
    public String getUrl2() {

        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }
    public String getUrl3() {

        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }


    public String getOtp() {

        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }



}
