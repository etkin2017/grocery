package com.emate.grocerystore.groceryMerchant.entities;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Ashvini on 2/21/2018.
 */

/*@Entity(tableName = "products")*/
public class ProductModel {

    @SerializedName("product_id")

    String product_id;

    @SerializedName("cat_id")
    //@ColumnInfo(name = "cat_id")
    private String cat_id;


    @SerializedName("subcat_id")
    //@ColumnInfo(name = "subcat_id")
    private String subcat_id;
    @SerializedName("det_cat_id")
    //@ColumnInfo(name = "det_cat_id")
    private String det_cat_id;

    @SerializedName("brand_id")
    //@ColumnInfo(name = "brand_id")
    private String brand_id;

    @SerializedName("title")
    //@ColumnInfo(name = "title")
    private String title;

    @SerializedName("available")
    //@ColumnInfo(name = "available")
    private String available;

    @SerializedName("seller_name")
    //@ColumnInfo(name = "seller_name")
    private String seller_name;

    @SerializedName("status")
    //@ColumnInfo(name = "status")
    private String status;

    @SerializedName("hstatus")
    //@ColumnInfo(name = "hstatus")
    private String hstatus;


    @SerializedName("image1")
    //@ColumnInfo(name = "image1")
    private String image1;


    @SerializedName("image2")
    //@ColumnInfo(name = "image2")
    private String image2;


    @SerializedName("image3")
    //@ColumnInfo(name = "image3")
    private String image3;

    @SerializedName("image4")
    //@ColumnInfo(name = "image4")
    private String image4;


    @SerializedName("image5")
    //@ColumnInfo(name = "image5")
    private String image5;

    @SerializedName("seller")
    //@ColumnInfo(name = "seller")
    private String seller;


    @SerializedName("shipping")
    //@ColumnInfo(name = "shipping")
    private String shipping;

    @SerializedName("cgst")
    //@ColumnInfo(name = "cgst")
    private String cgst;

    @SerializedName("sgst")
    //@ColumnInfo(name = "sgst")
    private String sgst;

    @SerializedName("seller_id")
    //@ColumnInfo(name = "seller_id")
    private String seller_id;

    @SerializedName("units")
    private List<UnitModel> unitModelList;


    @SerializedName("content")
    //@ColumnInfo(name = "content")
    private String content;


    @SerializedName("unit_id")
    //@ColumnInfo(name = "unit_id")
    private long unit_id;


    @SerializedName("pro_id")
    //@ColumnInfo(name = "pro_id")
    private String pro_id;


    @SerializedName("quantity")
    //@ColumnInfo(name = "quantity")
    private String quantity;


    @SerializedName("unit")
    //@ColumnInfo(name = "unit")
    private String unit;

    @SerializedName("price")
    //@ColumnInfo(name = "price")
    private String price;

    @SerializedName("dis")
    //@ColumnInfo(name = "dis")
    private String dis;

    @SerializedName("mrp")
    //@ColumnInfo(name = "mrp")
    private String mrp;
    // for unit


    @SerializedName("unit_ids")
    private String unit_ids;

    @SerializedName("merchant_id")
    private String merchant_id;


    public String getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getUnit_ids() {
        return unit_ids;
    }

    public void setUnit_ids(String unit_ids) {
        this.unit_ids = unit_ids;
    }

    boolean chk_prod = false;


    public boolean isChk_prod() {
        return chk_prod;
    }

    public void setChk_prod(boolean chk_prod) {
        this.chk_prod = chk_prod;
    }

    public List<UnitModel> getUnitModelList() {
        return unitModelList;
    }

    public void setUnitModelList(List<UnitModel> unitModelList) {
        this.unitModelList = unitModelList;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getSubcat_id() {
        return subcat_id;
    }

    public void setSubcat_id(String subcat_id) {
        this.subcat_id = subcat_id;
    }

    public String getDet_cat_id() {
        return det_cat_id;
    }

    public void setDet_cat_id(String det_cat_id) {
        this.det_cat_id = det_cat_id;
    }

    public String getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String brand_id) {
        this.brand_id = brand_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHstatus() {
        return hstatus;
    }

    public void setHstatus(String hstatus) {
        this.hstatus = hstatus;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getImage5() {
        return image5;
    }

    public void setImage5(String image5) {
        this.image5 = image5;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(long unit_id) {
        this.unit_id = unit_id;
    }

    public String getPro_id() {
        return pro_id;
    }

    public void setPro_id(String pro_id) {
        this.pro_id = pro_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDis() {
        return dis;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }
}
