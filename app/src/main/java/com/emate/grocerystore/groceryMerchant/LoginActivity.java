package com.emate.grocerystore.groceryMerchant;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {


    EditText etEmail, etPassword;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Login");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*btnLogin = (Button) findViewById(R.id.btnLogin);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(etEmail.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Email", LoginActivity.this);
                } else if (TextUtils.isEmpty(etPassword.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Password", LoginActivity.this);
                } else {
                    loginUser();
                }

            }
        });*/

    }

    public void loginUser() {


        final ProgressDialog pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "loginMerchant";// "getProductBySuperCategory"; //getProd
        final JSONObject jsonObject = new JSONObject();
        try {
            //user_name,email,mb_no,shipping_add,total_order,user_status

            jsonObject.put("email", etEmail.getText().toString());
            jsonObject.put("password", etPassword.getText().toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = response.getJSONArray("success");
                    if (jsonArray.length() > 0) {
                        JSONObject jobject = jsonArray.getJSONObject(0);
                        // AppController.retShowAlertDialod("Login Done", LoginActivity.this);
                        PrefManager prefManager = new PrefManager(LoginActivity.this);
                        prefManager.setIsMerchantLoggedIn(true);
                        prefManager.setMerchantMobileno(jobject.getString("mobile"));
                        prefManager.setMerchantEmail(jobject.getString("email"));
                        prefManager.setMerchantName(jobject.getString("name"));
                        prefManager.setMerchantId(jobject.getString("id"));


                        AppController.successDialog("Login Successful", LoginActivity.this);


                    } else {
                        AppController.retShowAlertDialod("Login Failed", LoginActivity.this);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "logim");

    }
}
