package com.emate.grocerystore.groceryMerchant;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AppControllerOLD extends Application {

    public static final String TAG = AppControllerOLD.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static String FROM = "FROM";
    public static SweetAlertDialog sweetAlertDialog;

    private static AppControllerOLD mInstance;
    private static final RetryPolicy VOLLEY_REQUEST_RETRY_POLICY = new DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

    public static String Api_Url = "http://192.168.0.109/grossary/mobile_app/webservice/";//http://grocery.etkininfotech.com/grocery/mobile_app/webservice/";
    public static String api_bsase_url = "http://192.168.0.109/groceryShop_webServices/";
    public static String img_base_url = "http://192.168.0.109/";

    @Override
    public void onCreate() {
        super.onCreate();
       // MultiDex.install(this);
        mInstance = this;
        //   sweetAlertDialog = new SweetAlertDialog(this);

       // TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Rupee_Foradian.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

    }


    public static double roundStr(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    public static float retFlot(String str) {
        float v = 0;

        try {
            if (str.equalsIgnoreCase("null") || str.equalsIgnoreCase("")) {

            } else {
                v = Float.parseFloat(str);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return v;
    }

    public static String retStrr(String b) {

        String a = "-";
        if (b.equalsIgnoreCase("null") || b.equalsIgnoreCase("")) {
            a = "-";
        } else {
            a = b;
        }
        return a;

    }


    public static String getInitalsStr(String str) {
        String[] parts = str.split(" ");
        String initialsStr = "";
        for (int i = 0; i < parts.length; i++) {
            initialsStr = initialsStr + parts[i].charAt(0);
        }

        return initialsStr;
    }


    public static String getDay(long timstamp) {

        String dt = "";
        try {

            Date date = new Date(timstamp);
            String d1 = new SimpleDateFormat("MMM-dd/yyyy hh:mm").format(date);

            dt = d1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // return dt;

        String day = (new SimpleDateFormat("EEEE")).format(new Timestamp(timstamp).getTime()).toString();
        return day + ", " + dt;

    }

    public static int getRandomColorCode() {
        Random rand = new Random();
        int r = rand.nextInt(255);
        int g = rand.nextInt(255);
        int b = rand.nextInt(255);
        int randomColor = Color.rgb(r, g, b);

        return randomColor;
    }

    public static void retFromDiscount(Context ctx, String item) {
     /*   List<String> disList = Arrays.asList(ctx.getResources().getStringArray(R.array.discount_array));
          <item>Upto 5%</item>
        <item>5% - 10%   </item>
        <item>10% - 15%   </item>
        <item>15% - 20%   </item>
        <item>20% - 25%    </item>
        <item>More than 25%</item>

                for(int i)
        if(disList.contains(item)){

        }

    }

    public int getPos(List<String> disList , String item){*/
    }

    public static synchronized AppControllerOLD getInstance() {
        return mInstance;
    }

    public static void setData(TextView tv, String str) {
        try {
            if (str == null || str.equalsIgnoreCase("null")) {
                tv.setText("-");
            } else {
                tv.setText(" " + str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void successDialog(String alert_text, Context ctx) {
        sweetAlertDialog = new SweetAlertDialog((Activity) ctx);
        sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText("\n" + alert_text)
                .setContentText("\n")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
        sweetAlertDialog.show();


    }


    public static void retShowAlertDialod(String alert_text, Context ctx) {
        sweetAlertDialog = new SweetAlertDialog((Activity) ctx);
        sweetAlertDialog.changeAlertType(SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitleText("\n" + alert_text)
                .setContentText("\n")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
        sweetAlertDialog.show();


    }

  /*  public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }



    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
*/

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new OkHttpStack());

        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);

    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        getRequestQueue().add(req);
    }




    public static String retDate(String tstamp) {
        String dt = "";
        try {
            long time_st = Long.parseLong(tstamp);
            Date date = new Date(time_st);
            String d1 = new SimpleDateFormat("dd-MM-yyyy").format(date);

            dt = d1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dt;
    }




}

