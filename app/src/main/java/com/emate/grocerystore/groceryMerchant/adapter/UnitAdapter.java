package com.emate.grocerystore.groceryMerchant.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.groceryMerchant.entities.UnitModel;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ashvini on 2/21/2018.
 */

public class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.UnitHolder> {


    Context ctx;
    List<UnitModel> unitModelList = new ArrayList<>();
    String unitIds;
    int chk;

    public UnitAdapter() {
    }

    public UnitAdapter(Context ctx, List<UnitModel> unitModelList, String unitIds, int chk) {
        this.ctx = ctx;
        this.unitModelList = unitModelList;
        this.unitIds = unitIds;
        this.chk = chk;
    }

    public List<UnitModel> getUnitModelList() {
        return unitModelList;
    }

    public void setUnitModelList(List<UnitModel> unitModelList) {
        // this.unitModelList.clear();
        this.unitModelList = unitModelList;
        try {
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public UnitHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.unit_row, parent, false);
        return new UnitHolder(view);
    }

    @Override
    public void onBindViewHolder(UnitHolder holder, int position) {
        UnitModel um = unitModelList.get(position);
        holder.unit_quant.setText(um.getPrice() + "/" + um.getQuantity() + um.getUnit());


       /* if (chk == 0) {
            try {
                if (unitIds.contains(um.getUnit_id() + "")) {
                    holder.chk_prod.setChecked(true);
                } else {
                    holder.chk_prod.setChecked(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {*/
            holder.chk_prod.setChecked(um.isCheck());
      //  }
        holder.setIsRecyclable(true);

    }

    @Override
    public int getItemCount() {
        return unitModelList.size();
    }

    public class UnitHolder extends RecyclerView.ViewHolder {

        TextView unit_quant;
        CheckBox chk_prod;

        public UnitHolder(View itemView) {
            super(itemView);
            unit_quant = (TextView) itemView.findViewById(R.id.unit_quant);
            chk_prod = (CheckBox) itemView.findViewById(R.id.chk_prod);


        }

       /* TextView prod_nm, show_view;
        RecyclerView unit_list;



        public UnitHolder(View itemView) {
            super(itemView);
            show_view = (TextView) itemView.findViewById(R.id.show_view);
            prod_nm = (TextView) itemView.findViewById(R.id.prod_nm);
            unit_list = (RecyclerView) itemView.findViewById(R.id.unit_list);

        }*/
    }

}
