package com.emate.grocerystore.groceryMerchant.loginmodule;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.PrefManager;

/*import com.etkininfotech.grocerystore.groceryMerchant.DashBoard;
import com.etkininfotech.grocerystore.R;*/

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;
    SharedPreferences sharedpreferences = null;
    String merchant_data = null;

    PrefManager prefManager;// = new PrefManager(getActivity());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_merchant);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            sharedpreferences = getSharedPreferences("MyPREFERENCES", Context.MODE_PRIVATE);
            merchant_data = sharedpreferences.getString("key", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity


                // if (merchant_data == null) {

                Intent i = new Intent(getApplicationContext(), MainScreen.class);
                startActivity(i);
                finish();
                /*} else {
                    Intent i = new Intent(getApplicationContext(), DashBoard.class);
                    startActivity(i);
                    finish();
                }*/
                // close this activity
            }
        }, SPLASH_TIME_OUT);


    }


}
