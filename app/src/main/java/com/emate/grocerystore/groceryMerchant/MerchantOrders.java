package com.emate.grocerystore.groceryMerchant;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.*;
import com.emate.grocerystore.groceryMerchant.adapter.OrderUserAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MerchantOrders extends AppCompatActivity {
    RecyclerView merchant_order;
    TextView no_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_orders);
        merchant_order = (RecyclerView) findViewById(R.id.merchant_order);
        no_result = (TextView) findViewById(R.id.no_result);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Merchant Orders");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        getProd();
    }


    /*public List<Users> getUserList(List<Orders> productModelList) {
        List<Users> usersList = new ArrayList<>();
        for (int i = 0; i < productModelList.size(); i++) {
            if (!usersList.contains(productModelList.get(i).getUsers().getUnique_id())) {
                usersList.add(productModelList.get(i).getUsers());
            }
        }
        return usersList;
    }*/

    public List<Users> getUserList(List<Orders> productModelList) {
        List<String> userIds = new ArrayList<>();
        List<Users> usersList = new ArrayList<>();
        for (int i = 0; i < productModelList.size(); i++) {
            if (!userIds.contains(productModelList.get(i).getUsers().getUnique_id())) {
                userIds.add(productModelList.get(i).getUsers().getUnique_id());
                usersList.add(productModelList.get(i).getUsers());
            }
        }
        return usersList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getProd() {

        SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
        String merchId = sh.getString(Config.MERCHANT_ID, "");
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getMerchantOrder";
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("merchant_id", merchId);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<Orders> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");

                    if (j_products.length() == 0) {
                        no_result.setVisibility(View.VISIBLE);
                        merchant_order.setVisibility(View.GONE);
                    } else {
                        no_result.setVisibility(View.GONE);
                        merchant_order.setVisibility(View.VISIBLE);
                    }

                    try {

                        for (int i = 0; i < j_products.length(); i++) {
                            Orders orders = new Orders();
//order_id, user_id, order_dt_tim, total_amt, payment_mode, delivery_status, delivery_dt_tim, product_id, unit_id, prod_qty, txnid
                            String order_id = j_products.getJSONObject(i).getString("id");
                            String order_dt_tim = j_products.getJSONObject(i).getString("order_dt_tim");
                            String total_amt = j_products.getJSONObject(i).getString("total_amt");
                            String payment_mode = j_products.getJSONObject(i).getString("payment_mode");
                            String delivery_status = j_products.getJSONObject(i).getString("delivery_status");
                            String delivery_dt_tim = j_products.getJSONObject(i).getString("delivery_dt_tim");
                            String txnid = j_products.getJSONObject(i).getString("txnid");
                            String prod_qty = j_products.getJSONObject(i).getString("prod_qty");

                            orders.setOrder_id(order_id);
                            orders.setOrder_dt_tim(order_dt_tim);
                            orders.setDelivery_dt_tim(delivery_dt_tim);
                            orders.setDelivery_status(delivery_status);
                            orders.setPayment_mode(payment_mode);
                            orders.setProd_qty(prod_qty);
                            orders.setTxnid(txnid);
                            orders.setTotal_amt(total_amt);

                            ProductModel pm = new ProductModel();
                            String id = j_products.getJSONObject(i).getString("prodId");
                            String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                            String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                            String brand_id = j_products.getJSONObject(i).getString("brand_id");
                            String title = j_products.getJSONObject(i).getString("title");
                            String available = j_products.getJSONObject(i).getString("available");
                            String seller_name = j_products.getJSONObject(i).getString("seller_name");

                            String status = j_products.getJSONObject(i).getString("status");
                            String hstatus = j_products.getJSONObject(i).getString("hstatus");
                            String image1 = j_products.getJSONObject(i).getString("image1");
                            String image2 = j_products.getJSONObject(i).getString("image2");
                            String image3 = j_products.getJSONObject(i).getString("image3");
                            String image4 = j_products.getJSONObject(i).getString("image4");
                            String image5 = j_products.getJSONObject(i).getString("image5");
                            String seller = j_products.getJSONObject(i).getString("seller");
                            String shipping = j_products.getJSONObject(i).getString("shipping");
                            String cgst = j_products.getJSONObject(i).getString("cgst");
                            String sgst = j_products.getJSONObject(i).getString("sgst");
                            String seller_id = j_products.getJSONObject(i).getString("seller_id");

                            String content = j_products.getJSONObject(i).getString("content");


                            pm.setId(id);
                            pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                            pm.setSubcat_id(subcat_id);
                            pm.setDet_cat_id(det_cat_id);
                            pm.setBrand_id(brand_id);
                            pm.setTitle(title);
                            pm.setAvailable(available);
                            pm.setSeller_name(seller_name);
                            pm.setStatus(status);
                            pm.setHstatus(hstatus);
                            pm.setImage1(image1);
                            pm.setImage2(image2);
                            pm.setImage3(image3);
                            pm.setImage4(image4);
                            pm.setImage5(image5);
                            pm.setSeller(seller);
                            pm.setShipping(shipping);
                            pm.setCgst(cgst);
                            pm.setSgst(sgst);
                            pm.setSeller_id(seller_id);
                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("unit_id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);

                            pm.setUnitModel(um);
                            pm.setContent(content);


                            Users users = new Users();
                            users.setUnique_id(j_products.getJSONObject(i).getString("userId"));
                            users.setEmail(j_products.getJSONObject(i).getString("user_email"));
                            users.setUser_name(j_products.getJSONObject(i).getString("user_name"));
                            users.setMb_no(j_products.getJSONObject(i).getString("user_mobno"));
                            users.setShipping_add(j_products.getJSONObject(i).getString("shipping_add"));

                            orders.setUsers(users);
                            orders.setPm(pm);
                            productModelList.add(orders);

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    // List<String> alldts = getDates(productModelList);
                    OrderUserAdapter adapter = new OrderUserAdapter(getUserList(productModelList), MerchantOrders.this, productModelList);
                    merchant_order.setLayoutManager(new LinearLayoutManager(MerchantOrders.this));
                    merchant_order.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(MerchantOrders.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");

    }
}
