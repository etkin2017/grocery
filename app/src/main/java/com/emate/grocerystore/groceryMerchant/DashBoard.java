package com.emate.grocerystore.groceryMerchant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.emate.grocerystore.R;
import com.emate.grocerystore.groceryMerchant.adapter.OrderUserAdapter;
import com.emate.grocerystore.groceryMerchant.repository.RemoveProduct;


public class DashBoard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        try {
           // getSupportActionBar().setHomeButtonEnabled(true);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Merchant");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void addProduct(View view) {
        startActivity(new Intent(DashBoard.this, ProductListActivity.class));
    }

    public void removeProduct(View view) {
        startActivity(new Intent(DashBoard.this, RemoveProduct.class));

    }


    public void orders(View view) {
        startActivity(new Intent(DashBoard.this, MerchantOrders.class));

    }


    public void send_enquiry(View view) {
        startActivity(new Intent(DashBoard.this, ProductListActivity.class));

    }
}
