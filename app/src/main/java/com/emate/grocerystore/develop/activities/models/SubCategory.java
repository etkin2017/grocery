package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 11/7/2017.
 */

public class SubCategory implements Serializable{
    String sub_cat_id,sub_cat_name,id;

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
