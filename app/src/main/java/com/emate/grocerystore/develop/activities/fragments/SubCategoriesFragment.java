package com.emate.grocerystore.develop.activities.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.HomeActivity;
import com.emate.grocerystore.develop.activities.activities.ProductBySuperCategory;
import com.emate.grocerystore.develop.activities.models.SubCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategoriesFragment extends Fragment {


    public SubCategoriesFragment() {
        // Required empty public constructor
    }

    FragmentManager manager;
    FragmentTransaction transaction;
    RecyclerView lvCities;
    View rootview;
    String cat_id = "";
    ImageView back_btn;
    List<SubCategory> sortedList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_sub_categories, container, false);
        lvCities = (RecyclerView) rootview.findViewById(R.id.sub_categories_list);
        cat_id = getArguments().getString("cat_id");

        back_btn = (ImageView) rootview.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }

            }
        });

        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        lvCities.setLayoutAnimation(animation);
        SubCategoryAdapter adapter = new SubCategoryAdapter(sortByCatId());
        lvCities.setHasFixedSize(true);
        lvCities.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvCities.setAdapter(adapter);
        return rootview;
    }

    public List<SubCategory> sortByCatId() {

        for (int i = 0; i < HomeActivity.subCatList.size(); i++) {
            if (HomeActivity.subCatList.get(i).getId().equals(cat_id)) {
                sortedList.add(HomeActivity.subCatList.get(i));
            }
        }

        return sortedList;
    }

    public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.CatHolder> {

        List<SubCategory> categoriesList;

        public SubCategoryAdapter(List<SubCategory> categoriesList) {
            this.categoriesList = categoriesList;
        }


        @Override
        public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(getActivity()).inflate(R.layout.caterories_row, parent, false);
            CatHolder catHolder = new CatHolder(v);
            return catHolder;
        }

        @Override
        public void onBindViewHolder(final CatHolder holder, final int position) {

            holder.cat_nm.setText(categoriesList.get(position).getSub_cat_name());
            if (Config.cotainSubCat(HomeActivity.subCategoryDeatilsList, categoriesList.get(position).getSub_cat_id())) {
                holder.cat_arrow.setVisibility(View.VISIBLE);
            } else {
                holder.cat_arrow.setVisibility(View.GONE);

            }

            holder.category_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.cat_arrow.getVisibility() != View.GONE) {
                        SubCategoriesDetailsFragment states = new SubCategoriesDetailsFragment();
                        //  city.setData(position,getActivity());
                        manager = getActivity().getSupportFragmentManager();
                        transaction = manager.beginTransaction();
                        //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

                        Bundle b = new Bundle();
                        b.putString("cat_id", categoriesList.get(position).getId());
                        b.putString("sub_cat_id", categoriesList.get(position).getSub_cat_id());
                        states.setArguments(b);

                        transaction.replace(R.id.contain, states, "details");
                        transaction.addToBackStack("details");
                        transaction.commit();
                    } else {


                        Intent int1 = new Intent(getActivity(), ProductBySuperCategory.class);
                        int1.putExtra("sub_cat", (ArrayList) categoriesList);
                        int1.putExtra("brandList", (ArrayList) HomeActivity.brandList);
                        int1.putExtra("cat_id", categoriesList.get(position).getSub_cat_id());
                        int1.putExtra("cat_nm", categoriesList.get(position).getSub_cat_name());
                        int1.putExtra("sub_category", "1");
                        startActivity(int1);

                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return categoriesList.size();
        }

        public class CatHolder extends RecyclerView.ViewHolder {

            TextView cat_nm;
            ImageView cat_arrow;
            LinearLayout category_container;

            public CatHolder(View itemView) {
                super(itemView);
                cat_arrow = (ImageView) itemView.findViewById(R.id.cat_arrow);
                cat_nm = (TextView) itemView.findViewById(R.id.cat_nm);
                category_container = (LinearLayout) itemView.findViewById(R.id.category_container);

            }
        }

    }

}
