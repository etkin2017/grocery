package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.RewardsAdapter;
import com.emate.grocerystore.develop.activities.models.PrefManager;
import com.emate.grocerystore.develop.activities.models.Rewards;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RewardPoints extends AppCompatActivity {

    RecyclerView rewards_pts;
    TextView no_result_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward_points);
        rewards_pts = (RecyclerView) findViewById(R.id.rewards_pts);
        no_result_found = (TextView) findViewById(R.id.no_result_found);
        getRewards();
    }


    public void getRewards() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getRewards";
        JSONObject jsonObject = new JSONObject();
        try {
            PrefManager prefManager = new PrefManager(RewardPoints.this);
            jsonObject.put("user_id", prefManager.getID() + "");


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<Rewards> rewardPointses = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");
                    if (j_products.length() == 0) {
                        no_result_found.setVisibility(View.VISIBLE);
                        rewards_pts.setVisibility(View.GONE);
                    } else {
                        no_result_found.setVisibility(View.GONE);
                        rewards_pts.setVisibility(View.VISIBLE);


                        try {

                            for (int i = 0; i < j_products.length(); i++) {
                                JSONObject jsonObject1 = j_products.getJSONObject(i);
                                Rewards rewards = new Rewards();
                                rewards.setUser_name(jsonObject1.getString("user_name"));
                                rewards.setPts(jsonObject1.getString("pts"));
                                rewards.setTstamp(jsonObject1.getString("tstamp"));

                                rewardPointses.add(rewards);

                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        RewardsAdapter adapter = new RewardsAdapter(rewardPointses, RewardPoints.this);
                        rewards_pts.setLayoutManager(new LinearLayoutManager(RewardPoints.this));
                        rewards_pts.setAdapter(adapter);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(RewardPoints.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product11");

    }

}
