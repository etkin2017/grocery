package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 12/18/2017.
 */

public class ProductAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private static final int MAX_RESULTS = 10;
    private Context mContext;
    private List<ProductModel> resultList = new ArrayList<ProductModel>();
    ProgressBar pd;

    public ProductAutoCompleteAdapter(Context context, ProgressBar pd) {
        mContext = context;
        this.pd = pd;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public ProductModel getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.search_product_row, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(getItem(position).getTitle());
        ((TextView) convertView.findViewById(R.id.text2)).setText(getItem(position).getSeller_name());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {

                    List<ProductModel> pmodlList = new ArrayList<>();
                    try {


                        List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>(1);

                        nameValuePairList.add(new BasicNameValuePair("desc", constraint.toString()));

                        HttpClient httpClient = new DefaultHttpClient();
                        HttpPost post = new HttpPost(Config.Api_Url + "searchProd");
                        post.setEntity(new UrlEncodedFormEntity(nameValuePairList));
                        HttpResponse response = httpClient.execute(post);
                        HttpEntity entity = response.getEntity();
                        String data = EntityUtils.toString(entity);

                        JSONObject jsonObject1 = new JSONObject(data);

                        JSONArray j_products = jsonObject1.getJSONArray("success");
                        for (int i = 0; i < j_products.length(); i++) {
                            ProductModel pm = new ProductModel();
                            String id = j_products.getJSONObject(i).getString("id");
                            String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                            String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                            String brand_id = j_products.getJSONObject(i).getString("brand_id");
                            String title = j_products.getJSONObject(i).getString("title");
                            String available = j_products.getJSONObject(i).getString("available");
                            String seller_name = j_products.getJSONObject(i).getString("seller_name");

                            String status = j_products.getJSONObject(i).getString("status");
                            String hstatus = j_products.getJSONObject(i).getString("hstatus");
                            String image1 = j_products.getJSONObject(i).getString("image1");
                            String image2 = j_products.getJSONObject(i).getString("image2");
                            String image3 = j_products.getJSONObject(i).getString("image3");
                            String image4 = j_products.getJSONObject(i).getString("image4");
                            String image5 = j_products.getJSONObject(i).getString("image5");
                            String seller = j_products.getJSONObject(i).getString("seller");
                            String shipping = j_products.getJSONObject(i).getString("shipping");
                            String cgst = j_products.getJSONObject(i).getString("cgst");
                            String sgst = j_products.getJSONObject(i).getString("sgst");
                            String seller_id = j_products.getJSONObject(i).getString("seller_id");

                            String content = j_products.getJSONObject(i).getString("content");


                            pm.setId(id);
                            pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                            pm.setSubcat_id(subcat_id);
                            pm.setDet_cat_id(det_cat_id);
                            pm.setBrand_id(brand_id);
                            pm.setTitle(title);
                            pm.setAvailable(available);
                            pm.setSeller_name(seller_name);
                            pm.setStatus(status);
                            pm.setHstatus(hstatus);
                            pm.setImage1(image1);
                            pm.setImage2(image2);
                            pm.setImage3(image3);
                            pm.setImage4(image4);
                            pm.setImage5(image5);
                            pm.setSeller(seller);
                            pm.setShipping(shipping);
                            pm.setCgst(cgst);
                            pm.setSgst(sgst);
                            pm.setSeller_id(seller_id);
                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("unit_id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);

                            pm.setUnitModel(um);
                            pm.setContent(content);

                            pmodlList.add(pm);


                            //suggest.add(SuggestKey);
                        }

                        filterResults.values = pmodlList;
                        filterResults.count = pmodlList.size();

                    } catch (ClientProtocolException e) {
                        e.printStackTrace();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();


                    } catch (UnknownHostException e) {
                        e.printStackTrace();

                    } catch (IOException e) {
                        e.printStackTrace();

                    } catch (Exception e) {
                        e.printStackTrace();

                    }



                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, Filter.FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<ProductModel>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return filter;
    }

    /**
     * Returns a search result for the given book title.
     */



}
