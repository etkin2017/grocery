package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.HomeActivity;
import com.emate.grocerystore.develop.activities.activities.ProductBySuperCategory;
import com.emate.grocerystore.develop.activities.models.Category;
import com.emate.grocerystore.develop.activities.models.SubCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 1/18/2018.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryHolder> {

    List<Category> catList1;
    List<Category> catList2;
    Context ctx;
    List<SubCategory> subCatList;
    TextView view_all_btn;
    boolean chk = false;
    View view;
    RecyclerView rec;

    public CategoryListAdapter(List<Category> catList, Context ctx, List<SubCategory> subCatList, TextView view_all_btn, RecyclerView rec) {
        this.catList1 = catList.subList(0, 8);
        this.catList2 = catList;
        this.ctx = ctx;
        this.subCatList = subCatList;
        this.view_all_btn = view_all_btn;
        this.rec = rec;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(ctx).inflate(R.layout.grid_row, parent, false);
        CategoryHolder holder = new CategoryHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, final int position) {

        holder.cat_name.setText(catList1.get(position).getCategory_type());
        try {
            holder.cat_icon.setImageDrawable(ctx.getResources().getDrawable(retCat(position)));
            holder.cat_icon.setBackgroundColor(Color.WHITE);
            holder.cat_icon.setId(position);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        holder.cat_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<SubCategory> subCategoryArrayList = new ArrayList<SubCategory>();
                for (int i = 0; i < subCatList.size(); i++) {
                    if (subCatList.get(i).getId().equals(catList1.get(position).getId())) {
                        subCategoryArrayList.add(subCatList.get(i));
                    }

                }


                Intent int1 = new Intent(ctx, ProductBySuperCategory.class);
                int1.putExtra("brandList", (ArrayList) HomeActivity.brandList);
                int1.putExtra("sub_cat", subCategoryArrayList);
                int1.putExtra("cat_id", catList1.get(position).getId());
                int1.putExtra("cat_nm", catList1.get(position).getCategory_type());
                int1.putExtra("sub_category", "0");
                ctx.startActivity(int1);
            }
        });

        view_all_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (chk) {
                    chk = false;
                    view_all_btn.setText("View All");
                } else {
                    chk = true;

                    view_all_btn.setText("Hide");
                }
                refresh();
            }
        });


    }

    public void refresh() {
        if (!chk) {
            this.catList1 = catList2.subList(0, 8);
        } else {
            this.catList1 = catList2;
        }

        int d = catList1.size() * 180;
        ViewGroup.LayoutParams params = rec.getLayoutParams();
        params.height = d;
        rec.setLayoutParams(params);
        notifyDataSetChanged();
        //new CategoryListAdapter(catList1,ctx,)
    }

    public int retCat(int p) {
        int a = R.drawable.backet;
        switch (p) {
            case 0:
                a = R.drawable.ic_fruit_n_veg;
                break;
            case 1:
                a = R.drawable.ic_grain_oil_masala;
                break;

            case 2:
                a = R.drawable.ic_backery_cake;
                break;

            case 3:
                a = R.drawable.ic_beverage;
                break;
            case 4:
                a = R.drawable.ic_branded_food;
                break;

            case 5:
                a = R.drawable.ic_beauty_hygiene;
                break;

            case 6:
                a = R.drawable.ic_household;
                break;
            case 7:
                a = R.drawable.ic_gourmet_food;
                break;

            default:
                break;


        }
        return a;

    }

    @Override
    public int getItemCount() {
        return catList1.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {

        ImageView cat_icon;
        TextView cat_name;
        LinearLayout cat_container;

        public CategoryHolder(View itemView) {
            super(itemView);
            cat_icon = (ImageView) itemView.findViewById(R.id.cat_icon);
            cat_name = (TextView) itemView.findViewById(R.id.cat_name);
            cat_container = (LinearLayout) itemView.findViewById(R.id.cat_container);

        }
    }
}
