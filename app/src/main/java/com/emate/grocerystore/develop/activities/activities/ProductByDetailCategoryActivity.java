package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;

import com.emate.grocerystore.develop.activities.adapter.GridAdapter;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.SubCategoryDeatils;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductByDetailCategoryActivity extends AppCompatActivity {

    GridView products;
    SubCategoryDeatils scd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_by_detail_category);
        products = (GridView) findViewById(R.id.products);
        scd = ((SubCategoryDeatils) getIntent().getSerializableExtra("prod_cat"));
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(scd.getDet_cat_name());

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

        getProd(scd.getDet_cat_id(),"getProdByDcat");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void getProd(String id1, String url1) {

        SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
        String merchId = sh.getString(Config.MERCHANT_ID, "");
        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + url1;// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id1);
            jsonObject.put("first", 1);
            jsonObject.put("last", 10);
            jsonObject.put("merchant_id", merchId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<ProductModel> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");

                    try {

                        for (int i = 0; i < j_products.length(); i++) {

                            ProductModel pm = new ProductModel();
                            String id = j_products.getJSONObject(i).getString("id");
                            String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                            String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                            String brand_id = j_products.getJSONObject(i).getString("brand_id");
                            String title = j_products.getJSONObject(i).getString("title");
                            String available = j_products.getJSONObject(i).getString("available");
                            String seller_name = j_products.getJSONObject(i).getString("seller_name");

                            String status = j_products.getJSONObject(i).getString("status");
                            String hstatus = j_products.getJSONObject(i).getString("hstatus");
                            String image1 = j_products.getJSONObject(i).getString("image1");
                            String image2 = j_products.getJSONObject(i).getString("image2");
                            String image3 = j_products.getJSONObject(i).getString("image3");
                            String image4 = j_products.getJSONObject(i).getString("image4");
                            String image5 = j_products.getJSONObject(i).getString("image5");
                            String seller = j_products.getJSONObject(i).getString("seller");
                            String shipping = j_products.getJSONObject(i).getString("shipping");
                            String cgst = j_products.getJSONObject(i).getString("cgst");
                            String sgst = j_products.getJSONObject(i).getString("sgst");
                            String seller_id = j_products.getJSONObject(i).getString("seller_id");

                            String content = j_products.getJSONObject(i).getString("content");


                            pm.setId(id);
                            pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                            pm.setSubcat_id(subcat_id);
                            pm.setDet_cat_id(det_cat_id);
                            pm.setBrand_id(brand_id);
                            pm.setTitle(title);
                            pm.setAvailable(available);
                            pm.setSeller_name(seller_name);
                            pm.setStatus(status);
                            pm.setHstatus(hstatus);
                            pm.setImage1(image1);
                            pm.setImage2(image2);
                            pm.setImage3(image3);
                            pm.setImage4(image4);
                            pm.setImage5(image5);
                            pm.setSeller(seller);
                            pm.setShipping(shipping);
                            pm.setCgst(cgst);
                            pm.setSgst(sgst);
                            pm.setSeller_id(seller_id);

                            pm.setContent(content);


                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("unit_id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);

                            pm.setUnitModel(um);

                            productModelList.add(pm);

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    GridAdapter adapter = new GridAdapter(productModelList, ProductByDetailCategoryActivity.this);
                    products.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ProductByDetailCategoryActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");

    }
}
