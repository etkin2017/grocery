package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ProdDetails;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashvini on 11/16/2017.
 */

public class GridAdapter extends BaseAdapter {


    List<ProductModel> productModelArrayList;
    Context ctx;

    public GridAdapter(List<ProductModel> catList, Context ctx) {
        this.productModelArrayList = catList;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return productModelArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return productModelArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        CatHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.product_row, viewGroup, false);
        }
        try {
            holder = new CatHolder(convertView);
            holder.ttl.setText(productModelArrayList.get(position).getTitle());
            Picasso.with(ctx).load(Uri.parse(productModelArrayList.get(position).getImage1())).placeholder(R.drawable.grocery_icon).into(holder.img);
            holder.price.setText(ctx.getResources().getString(R.string.indian_rupee) + productModelArrayList.get(position).getUnitModel().getMrp());
            Config.strikeThroughText(holder.price);
           // float disPrice = Config.getPrice(Float.parseFloat(productModelArrayList.get(position).getDprice()), Float.parseFloat(productModelArrayList.get(position).getPrice()));
            holder.dis_price.setText(ctx.getResources().getString(R.string.indian_rupee) + productModelArrayList.get(position).getUnitModel().getPrice() + "");
            holder.dis_per.setText(productModelArrayList.get(position).getUnitModel().getDis() + "%");

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent product_Details = new Intent(ctx, ProdDetails.class);
                    product_Details.putExtra("product_details", productModelArrayList.get(position));
                    ctx.startActivity(product_Details);
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return convertView;


    }

    public class CatHolder {
        TextView ttl, dis_price, price, dis_per;
        ImageView img;


        public CatHolder(View itemView) {

            ttl = (TextView) itemView.findViewById(R.id.ttl);
            dis_price = (TextView) itemView.findViewById(R.id.dis_price);
            price = (TextView) itemView.findViewById(R.id.price);
            dis_per = (TextView) itemView.findViewById(R.id.dis_per);
            img = (ImageView) itemView.findViewById(R.id.img);

        }
    }


}


