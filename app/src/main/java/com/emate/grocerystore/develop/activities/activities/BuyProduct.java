package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.PrefManager;

import org.apache.http.util.EncodingUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class BuyProduct extends AppCompatActivity {

    private ArrayList<ByteArrayOutputStream> streams;
    String merchant_key = "yzXYQogL";//"4Ia9GJ";
    String salt = " JpcoROjdHm";// "ZxJl0K9v";
    String action1 = "";
    String base_url = "https://test.payu.in";
    int error = 0;
    String hashString = "";
    String txnid = "";
    String udf2 = "";
    String txn = "abcd";
    String hash = "";
    String hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
    String amount = "";
    String name = "";
    String email = "";
    String phone = "";
    String productinfo = "";
    String successurl = "";
    String failurl = "";
    String serviceprovider = "payu_paisa";
    WebView web;
    public static Context context;
    //  BuyPack buyPack;
    public ProgressDialog pd;
    String postData = "";
    // PaymentData data;
    String pack;

    String amount_two;
    Map<String, Object> mh;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_product);


        web = (WebView) findViewById(R.id.payWeb);

        String intent_data = getIntent().getStringExtra("groceryData");

        try {
            JSONObject array = new JSONObject(intent_data);
            mh = jsonToMap(array);
            amount_two = getIntent().getStringExtra("amount");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        web.clearHistory();
        web.clearCache(true);
        settings.setPluginState(WebSettings.PluginState.ON);
        init(amount_two);
        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                //  handler.proceed();

                final AlertDialog.Builder builder = new AlertDialog.Builder(BuyProduct.this);
                builder.setMessage("SSL certificate is invalid");
                builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.proceed();
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                final AlertDialog dialog = builder.create();
                dialog.show();

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                if (!pd.isShowing())
                    ShowPD();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                DismissPD();

                Log.e("url", url);
                if (url.contains(successurl)) {
                    try {
                        successurl = Config.Api_Url + "addOderHis";
                        Map<String, String> newMap = new HashMap<String, String>();
                        for (Map.Entry<String, Object> entry : mh.entrySet()) {
                            if (entry.getValue() instanceof String) {
                                newMap.put(entry.getKey(), (String) entry.getValue());
                            }
                        }


                        String formdata = "qty=" + newMap.get("prod_qty") + "&amt=" + newMap.get("amt") + "&user_id=" + user_id + "&order_dt_tim=" + newMap.get("order_dt_tim") + "&payment_mode=" + newMap.get("payment_mode") + "" + "&product_id=" + newMap.get("product_id") + "&uids=(" + newMap.get("unit_id") + ")" + "&delivery_status=Pending&txnid=" + txnid;
                        view.postUrl(successurl, EncodingUtils.getBytes(formdata, "BASE64"));
                        view.setWebViewClient(new WebViewClient() {
                            @Override
                            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                                super.onReceivedSslError(view, handler, error);
                                handler.proceed();
                            }

                        });
                        Toast.makeText(BuyProduct.this, "Order Placed", Toast.LENGTH_SHORT).show();
                        DatabaseHandler databaseHandler = new DatabaseHandler(BuyProduct.this);
                        databaseHandler.deleteCart();

                        finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (url.contains(failurl)) {
                    try {
                        Toast.makeText(BuyProduct.this, "Failed To Place Order", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    try {
                        Toast.makeText(BuyProduct.this, "Failed To get Url", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        web.postUrl(base_url.concat("/_payment"), EncodingUtils.getBytes(postData, "BASE64"));

    }

    public void init(String amount1) {
        amount = amount1;
        try {
            PrefManager prefManager = new PrefManager(BuyProduct.this);
            user_id = prefManager.getID();
            name = prefManager.getUsername();
            email = prefManager.getUserEmail();
            phone = prefManager.getMobileNumber();
            productinfo = "Grocery Product";
        } catch (Exception e) {
            e.printStackTrace();
        }


        setTransId();
      /*  successurl = context.getString(R.string.ip) + "/payment";
        failurl = context.getString(R.string.ip) + "/fail";*/

        successurl = "success";
        failurl = "fail";
        setHash();
        pd = new ProgressDialog(BuyProduct.this);
        postData = "key=" + merchant_key + "&" + "hash=" + hash + "&" + "txnid=" + txnid + "&" + "udf2=" + txnid + "&" + "service_provider=" + serviceprovider + "&" + "amount=" + amount + "&" + "firstname=" + name + "&" + "email=" + email + "&" + "phone=" + phone + "&" + "productinfo=" + productinfo + "&" + "surl=" + successurl + "&" + "furl=" + failurl;


    }

    public void ShowPD() {
        pd.setMessage("Processing...");
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setIndeterminate(true);
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);
        pd.show();
    }

    public void DismissPD() {
        pd.dismiss();
    }

    public boolean empty(String s) {
        if (s == null || s.trim().equals(""))
            return true;
        else
            return false;
    }


    public String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();


            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) hexString.append("0");
                hexString.append(hex);
            }

        } catch (NoSuchAlgorithmException nsae) {
        }

        return hexString.toString();


    }


    public void setTransId() {
        if (empty(txnid)) {
            Random rand = new Random();
            String rndm = Integer.toString(rand.nextInt()) + (System.currentTimeMillis() / 1000L);
            txnid = hashCal("SHA-256", rndm).substring(0, 20);
        }
        udf2 = txnid;
    }

    public void setHash() {
        if (empty(hash)) {
            if (empty(merchant_key)
                    || empty(txnid)
                    || empty(amount)
                    || empty(name)
                    || empty(email)
                    || empty(phone)
                    || empty(productinfo)
                    || empty(successurl)
                    || empty(failurl)
                    || empty(serviceprovider))
                error = 1;
            else {
                String[] hashVarSeq1 = new String[]{merchant_key, txnid, amount, productinfo, name, email, "", udf2, "", "", "", "", "", "", "", ""};
                for (String part : hashVarSeq1) {
                    hashString = (empty(part)) ? hashString.concat("") : hashString.concat(part);
                    hashString = hashString.concat("|");
                }
                hashString = hashString.concat(salt);


                hash = hashCal("SHA-512", hashString);
                action1 = base_url.concat("/_payment");
            }
        } else if (!empty(hash)) {
            hash = hash;
            action1 = base_url.concat("/_payment");
        }


    }


    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if (json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}
