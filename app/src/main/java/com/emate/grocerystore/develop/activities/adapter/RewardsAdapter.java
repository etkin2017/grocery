package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.Rewards;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 3/1/2018.
 */

public class RewardsAdapter extends RecyclerView.Adapter<RewardsAdapter.RewardsHolder> {

    List<Rewards> rewardsList = new ArrayList<>();
    Context ctx;

    public RewardsAdapter(List<Rewards> rewardsList, Context ctx) {
        this.rewardsList = rewardsList;
        this.ctx = ctx;
    }

    @Override
    public RewardsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.reward_rows, parent, false);
        RewardsHolder holder = new RewardsHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(RewardsHolder holder, int position) {
        holder.dt_txt.setText("Date : " + AppController.retDate(rewardsList.get(position).getTstamp()));
        holder.nm_txt.setText("Name : "+rewardsList.get(position).getUser_name());
        holder.pts_txt.setText("Points : "+rewardsList.get(position).getPts());

    }

    @Override
    public int getItemCount() {
        return rewardsList.size();
    }

    class RewardsHolder extends RecyclerView.ViewHolder {

        TextView pts_txt, nm_txt, dt_txt;

        public RewardsHolder(View itemView) {
            super(itemView);
            pts_txt = (TextView) itemView.findViewById(R.id.pts_txt);
            nm_txt = (TextView) itemView.findViewById(R.id.nm_txt);
            dt_txt = (TextView) itemView.findViewById(R.id.dt_txt);

        }
    }
}
