package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.RewardsAdapter;
import com.emate.grocerystore.develop.activities.models.PrefManager;
import com.emate.grocerystore.develop.activities.models.Rewards;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EarnedPoints extends AppCompatActivity {
    Spinner yearspin, monthspin;
    TextView no_result_found;
    Button btn_search;
    RecyclerView reward_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earned_points);

        yearspin = (Spinner) findViewById(R.id.yearspin);
        monthspin = (Spinner) findViewById(R.id.monthspin);
        btn_search = (Button) findViewById(R.id.btn_search);
        reward_list = (RecyclerView) findViewById(R.id.reward_list);
        no_result_found = (TextView) findViewById(R.id.no_result_found);

        ArrayList<String> years = new ArrayList<String>();
        years.add("Select");
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 2017; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);
        yearspin.setAdapter(adapter);


        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (monthspin.getSelectedItemPosition() == 0) {
                    AppController.retShowAlertDialod("Please Select Month", EarnedPoints.this);
                } else if (yearspin.getSelectedItemPosition() == 0) {
                    AppController.retShowAlertDialod("Please Select Year", EarnedPoints.this);

                } else {
                    getRewards();
                }
            }
        });
    }

    public void getRewards() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getEarnedRewards";
        JSONObject jsonObject = new JSONObject();
        try {
            PrefManager prefManager = new PrefManager(EarnedPoints.this);
            jsonObject.put("user_id", prefManager.getID() + "");
            jsonObject.put("month", monthspin.getSelectedItem().toString());
            jsonObject.put("year", yearspin.getSelectedItem().toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<Rewards> rewardPointses = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");
                    if (j_products.length() == 0) {
                        no_result_found.setVisibility(View.VISIBLE);
                        reward_list.setVisibility(View.GONE);
                    } else {
                        no_result_found.setVisibility(View.GONE);
                        reward_list.setVisibility(View.VISIBLE);


                        try {

                            for (int i = 0; i < j_products.length(); i++) {
                                JSONObject jsonObject1 = j_products.getJSONObject(i);
                                Rewards rewards = new Rewards();
                                rewards.setUser_name(jsonObject1.getString("user_name"));
                                rewards.setPts(jsonObject1.getString("pts"));
                                rewards.setTstamp(jsonObject1.getString("tstamp"));

                                rewardPointses.add(rewards);

                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        RewardsAdapter adapter = new RewardsAdapter(rewardPointses, EarnedPoints.this);
                        reward_list.setLayoutManager(new LinearLayoutManager(EarnedPoints.this));
                        reward_list.setAdapter(adapter);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(EarnedPoints.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product11");

    }
}
