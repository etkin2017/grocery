package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.GridAdapter;
import com.emate.grocerystore.develop.activities.fragments.FIlterDialogFragment;
import com.emate.grocerystore.develop.activities.interfaces.FilterListener;
import com.emate.grocerystore.develop.activities.models.Brand;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.SubCategory;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductBySuperCategory extends AppCompatActivity implements FilterListener {

    RecyclerView sub_categories;
    GridView product_by_cat;

    ImageButton filter;
    TextView product_cnt;

    String brndIds = "null";
    String prc = "null";
    String dicnt = "null";
    String sortID = "null";

    List<Brand> brandList = new ArrayList<>();

    public static List<String> brand_id = new ArrayList<>();
    public static List<String> prcList = new ArrayList<>();
    public static List<String> disList = new ArrayList<>();
    public static int pos = 1;
    TextView clear_filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_by_super_category);
        init();
        //(ArrayList<String>) getIntent().getSerializableExtra("mylist");
        ArrayList<SubCategory> categoryArrayList = (ArrayList<SubCategory>) getIntent().getSerializableExtra("sub_cat");
        brandList = (ArrayList<Brand>) getIntent().getSerializableExtra("brandList");
        RecyclerAdapter adapter = new RecyclerAdapter(categoryArrayList);
        sub_categories.setAdapter(adapter);

        try {
            String sub_category = getIntent().getStringExtra("sub_category");
            if (sub_category.equalsIgnoreCase("0")) {
                getProd(getIntent().getStringExtra("cat_id"), "getProductBySuperCategory", brndIds, prc, dicnt, sortID);

            } else {
                getProd(getIntent().getStringExtra("cat_id"), "getProd", brndIds, prc, dicnt, sortID);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        if ((brand_id.size() > 0) || (prcList.size() > 0) || (disList.size() > 0)) {
            filter.setColorFilter(Color.BLUE);
            clear_filter.setVisibility(View.VISIBLE);
            // filter.setBackgroundColor(Color.BLACK);
        } else {
            filter.setColorFilter(Color.BLACK);
            clear_filter.setVisibility(View.GONE);
            //  filter.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        }
    }

    public void init() {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra("cat_nm"));

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        sub_categories = (RecyclerView) findViewById(R.id.sub_categories);
        product_cnt = (TextView) findViewById(R.id.product_cnt);
        filter = (ImageButton) findViewById(R.id.filter);
        clear_filter = (TextView) findViewById(R.id.clear_filter);

        product_by_cat = (GridView) findViewById(R.id.product_by_cat);
        sub_categories.setHasFixedSize(true);
        //  sub_categories.smoothScrollToPosition(0);
        sub_categories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FIlterDialogFragment fIlterDialogFragment = new FIlterDialogFragment();
                fIlterDialogFragment.setBrandList(brandList);
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.prod_container, fIlterDialogFragment, "filter")
                        .addToBackStack("filter")
                        .commit();
            }
        });
        clear_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearFilter();
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                brand_id.clear();
                prcList.clear();
                disList.clear();
                pos = 1;
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void clearFilter() {
        brand_id.clear();
        prcList.clear();
        disList.clear();
        pos = 1;
        brndIds = "null";
        prc = "null";
        dicnt = "null";
        sortID = "null";
        onResume();
    }


    public void getProd(String id1, String url1, String brnd, String price, String discount, String sortBy) {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + url1;
        JSONObject jsonObject = new JSONObject();

        SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
        String merchId = sh.getString(Config.MERCHANT_ID, "");

        try {
            jsonObject.put("id", id1);
            jsonObject.put("first", 0);
            jsonObject.put("last", 10);
            jsonObject.put("brnd", brnd);
            jsonObject.put("price", price);
            jsonObject.put("discount", discount);
            jsonObject.put("sortBy", sortBy);
            jsonObject.put("merchant_id", merchId);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<ProductModel> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");

                    try {

                        for (int i = 0; i < j_products.length(); i++) {

                            ProductModel pm = new ProductModel();
                            String id = j_products.getJSONObject(i).getString("id");
                            String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                            String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                            String brand_id = j_products.getJSONObject(i).getString("brand_id");
                            String title = j_products.getJSONObject(i).getString("title");
                            String available = j_products.getJSONObject(i).getString("available");
                            String seller_name = j_products.getJSONObject(i).getString("seller_name");

                            String status = j_products.getJSONObject(i).getString("status");
                            String hstatus = j_products.getJSONObject(i).getString("hstatus");
                            String image1 = j_products.getJSONObject(i).getString("image1");
                            String image2 = j_products.getJSONObject(i).getString("image2");
                            String image3 = j_products.getJSONObject(i).getString("image3");
                            String image4 = j_products.getJSONObject(i).getString("image4");
                            String image5 = j_products.getJSONObject(i).getString("image5");
                            String seller = j_products.getJSONObject(i).getString("seller");
                            String shipping = j_products.getJSONObject(i).getString("shipping");
                            String cgst = j_products.getJSONObject(i).getString("cgst");
                            String sgst = j_products.getJSONObject(i).getString("sgst");
                            String seller_id = j_products.getJSONObject(i).getString("seller_id");

                            String content = j_products.getJSONObject(i).getString("content");


                            pm.setId(id);
                            pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                            pm.setSubcat_id(subcat_id);
                            pm.setDet_cat_id(det_cat_id);
                            pm.setBrand_id(brand_id);
                            pm.setTitle(title);
                            pm.setAvailable(available);
                            pm.setSeller_name(seller_name);
                            pm.setStatus(status);
                            pm.setHstatus(hstatus);
                            pm.setImage1(image1);
                            pm.setImage2(image2);
                            pm.setImage3(image3);
                            pm.setImage4(image4);
                            pm.setImage5(image5);
                            pm.setSeller(seller);
                            pm.setShipping(shipping);
                            pm.setCgst(cgst);
                            pm.setSgst(sgst);
                            pm.setSeller_id(seller_id);
                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("unit_id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);

                            pm.setUnitModel(um);
                            pm.setContent(content);

                            productModelList.add(pm);

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    GridAdapter adapter = new GridAdapter(productModelList, ProductBySuperCategory.this);
                    product_by_cat.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ProductBySuperCategory.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product11");

    }


    public void onDetachedFragment() {
        try {
            String sub_category = getIntent().getStringExtra("sub_category");
            if (sub_category.equalsIgnoreCase("0")) {
                getProd(getIntent().getStringExtra("cat_id"), "getProductBySuperCategory", brndIds, prc, dicnt, "null");
            } else {
                getProd(getIntent().getStringExtra("cat_id"), "getProd", brndIds, prc, dicnt, "null");
            }


            onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onDetachedSortByFragment() {
        try {
            String sub_category = getIntent().getStringExtra("sub_category");
            if (sub_category.equalsIgnoreCase("0")) {
                getProd(getIntent().getStringExtra("cat_id"), "getProductBySuperCategory", "null", "null", "null", sortID);
            } else {
                getProd(getIntent().getStringExtra("cat_id"), "getProd", "null", "null", "null", sortID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRefineByDataPass(String brandid, String price, String discount) {
        this.brndIds = brandid;
        this.prc = price;
        this.dicnt = discount;
    }

    @Override
    public void onSortByDataPass(String sortby) {
        this.sortID = sortby;
    }

    public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CatHolder> {
        ArrayList<SubCategory> catList;

        RecyclerAdapter(ArrayList<SubCategory> catList) {
            this.catList = catList;
        }


        @Override
        public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(ProductBySuperCategory.this).inflate(R.layout.cat_nm, parent, false);
            CatHolder holder = new CatHolder(view);


            return holder;
        }

        @Override
        public void onBindViewHolder(CatHolder holder, final int position) {
            holder.catnm.setText(catList.get(position).getSub_cat_name());
            holder.catnm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        getSupportActionBar().setTitle(catList.get(position).getSub_cat_name());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    getProd(catList.get(position).getSub_cat_id(), "getProd", brndIds, prc, dicnt, sortID);
                }
            });

        }

        @Override
        public int getItemCount() {
            return catList.size();
        }

        public class CatHolder extends RecyclerView.ViewHolder {
            TextView catnm;

            public CatHolder(View itemView) {
                super(itemView);
                catnm = (TextView) itemView.findViewById(R.id.catnm);
            }
        }
    }


/*    public class GridAdapter2 extends RecyclerView.Adapter<GridAdapter2.CatHolder> {
        List<ProductModel> productModelArrayList;

        GridAdapter2(List<ProductModel> catList) {
            this.productModelArrayList = catList;
        }


        @Override
        public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(ProductBySuperCategory.this).inflate(R.layout.product_row, parent, false);
            CatHolder holder = new CatHolder(view);


            return holder;
        }

        @Override
        public void onBindViewHolder(CatHolder holder, int position) {
            holder.ttl.setText(productModelArrayList.get(position).getTitle());
            Picasso.with(ProductBySuperCategory.this).load(Uri.parse("")).placeholder(R.drawable.grocery_icon).into(holder.img);
            holder.price.setText(productModelArrayList.get(position).getPrice());
            // holder.dis_price.setText(Config.getPrice(Float.parseFloat(productModelArrayList.get(position).getDprice()), Float.parseFloat(productModelArrayList.get(position).getPrice())));
            holder.dis_per.setText(productModelArrayList.get(position).getDprice() + "%");

        }

        @Override
        public int getItemCount() {
            return productModelArrayList.size();
        }

        public class CatHolder extends RecyclerView.ViewHolder {
            TextView ttl, dis_price, price, dis_per;
            ImageView img;


            public CatHolder(View itemView) {
                super(itemView);
                ttl = (TextView) itemView.findViewById(R.id.ttl);
                dis_price = (TextView) itemView.findViewById(R.id.dis_price);
                price = (TextView) itemView.findViewById(R.id.price);
                dis_per = (TextView) itemView.findViewById(R.id.dis_per);
                img = (ImageView) itemView.findViewById(R.id.img);

            }
        }
    }*/


}
