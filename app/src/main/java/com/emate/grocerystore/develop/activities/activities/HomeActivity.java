package com.emate.grocerystore.develop.activities.activities;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.annotation.StyleRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.BrandListAdapter;
import com.emate.grocerystore.develop.activities.adapter.CategoryListAdapter;
import com.emate.grocerystore.develop.activities.adapter.HorizontalProductAdapter;
import com.emate.grocerystore.develop.activities.adapter.ProductAdapter;
import com.emate.grocerystore.develop.activities.ash_bottomnavigation.AHBottomNavigation;
import com.emate.grocerystore.develop.activities.ash_bottomnavigation.AHBottomNavigationItem;
import com.emate.grocerystore.develop.activities.card_slider.CardSliderLayoutManager;
import com.emate.grocerystore.develop.activities.card_slider.CardSnapHelper;
import com.emate.grocerystore.develop.activities.card_slider.cards.CardSliderAdapter;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.fragments.CategoriesFragment;
import com.emate.grocerystore.develop.activities.models.BadgeView;
import com.emate.grocerystore.develop.activities.models.Brand;
import com.emate.grocerystore.develop.activities.models.Category;
import com.emate.grocerystore.develop.activities.models.Offers;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.SubCategory;
import com.emate.grocerystore.develop.activities.models.SubCategoryDeatils;
import com.emate.grocerystore.develop.activities.models.UnitModel;
import com.emate.grocerystore.slideAdapter;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class HomeActivity extends AppCompatActivity {


    public static List<Category> catList;
    public static List<SubCategory> subCatList;
    public static List<SubCategoryDeatils> subCategoryDeatilsList;
    public static List<Brand> brandList = new ArrayList<>();
    DrawerLayout drawer;
    private static int currentPage = 0;
    ImageView drwer_menu;
    EditText search_bar;

    AHBottomNavigation bottomNavigation;
    //For Grid Menu
    RecyclerView grid_view, brandlist_recy;
    ImageView img_shping;
    public List<ProductModel> pmodlList = new ArrayList<>();
    public List<ProductModel> discount_product = new ArrayList<>();
    public List<ProductModel> daily_need = new ArrayList<>();
    public List<Offers> offers_product = new ArrayList<>();
    String[] produ = new String[]{};
    String[] price1 = new String[]{};
    String[] desc = new String[]{};
    String[] pics = new String[]{};

    //for Card Slider

    CardSliderAdapter sliderAdapter;// = new SliderAdapter(pics, 20, new OnCardClickListener());
    private FrameLayout frme_lay_product;// replace with controies
    private CardSliderLayoutManager layoutManger;
    private RecyclerView recyclerView;
    //  private ImageSwitcher mapSwitcher;
    private TextSwitcher ts_product_priceSwitcher;// replace with temperature temperatureSwitcher;
    private TextSwitcher ts_product_descriptionsSwitcher;// replace with description descriptionsSwitcher;
//    private View greenDot;

    private TextView tv_product_1;// replace with tv_county_1 and _2 country1TextView;
    private TextView tv_product_2;
    private int productOffset1;
    private int productOffset2;
    private long productAnimDuration;
    private int currentPosition;
    BadgeView badge;
    TextView view_all_btn;


    ScrollView main_scrollview;
    RecyclerView product_slider, hori_product_slider;

    View target;

    String merchnatId = "";

    public void initDailyEssen(List<Offers> productModelList) {

        ImageView prod1, prod2, prod3, prod4, prod5, prod6;
        prod1 = (ImageView) findViewById(R.id.prod1);
        prod2 = (ImageView) findViewById(R.id.prod2);
        prod3 = (ImageView) findViewById(R.id.prod3);
        prod4 = (ImageView) findViewById(R.id.prod4);
        prod5 = (ImageView) findViewById(R.id.prod5);
        prod6 = (ImageView) findViewById(R.id.prod6);

        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(0).getImage())).into(prod1);
        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(1).getImage())).into(prod2);
        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(2).getImage())).into(prod3);
        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(3).getImage())).into(prod4);
        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(4).getImage())).into(prod5);
        Picasso.with(HomeActivity.this).load(Uri.parse(productModelList.get(5).getImage())).into(prod6);


    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        merchnatId = getIntent().getStringExtra(Config.FROM);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drwer_menu = (ImageView) findViewById(R.id.drwer_menu);
        grid_view = (RecyclerView) findViewById(R.id.cat_grid);
        ViewGroup.LayoutParams params=grid_view.getLayoutParams();
        params.height= (8*180);
        grid_view.setLayoutParams(params);

        view_all_btn = (TextView) findViewById(R.id.view_all_btn);
        brandlist_recy = (RecyclerView) findViewById(R.id.brandlist);
        grid_view.setHasFixedSize(true);
        brandlist_recy.setHasFixedSize(true);
        //  sub_categories.smoothScrollToPosition(0);
        //new GridLayoutManager(getActivity(), 1, GridLayoutManager.HORIZONTAL, false)


        //     grid_view.setLayoutManager(new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false));

        target = findViewById(R.id.img_shping);
        badge = new BadgeView(HomeActivity.this, target);
        img_shping = (ImageView) findViewById(R.id.img_shping);
        search_bar = (EditText) findViewById(R.id.search_bar);
        main_scrollview = (ScrollView) findViewById(R.id.main_scrollview);
        product_slider = (RecyclerView) findViewById(R.id.product_slider);
        hori_product_slider = (RecyclerView) findViewById(R.id.hori_product_slider);
        catList = new ArrayList<>();
        subCatList = new ArrayList<>();
        subCategoryDeatilsList = new ArrayList<>();
        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

        getCategories();


        drwer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });


        try {
            initBottomNavigationUI();
        } catch (Exception e) {
            e.printStackTrace();
        }


        main_scrollview.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                int childHeight = ((LinearLayout) findViewById(R.id.main_container)).getHeight();
                boolean isScrollable = main_scrollview.getHeight() < childHeight + main_scrollview.getPaddingTop() + main_scrollview.getPaddingBottom();

                if (isScrollable) {
                    showOrHideBottomNavigation(false);
                } else {
                    showOrHideBottomNavigation(true);
                }

            }
        });


    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setAdapter(sliderAdapter);
        recyclerView.setHasFixedSize(true);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    onActiveCardChange();
                }
            }
        });

        layoutManger = (CardSliderLayoutManager) recyclerView.getLayoutManager();

        new CardSnapHelper().attachToRecyclerView(recyclerView);
    }


    private void initSwitchers() {
        ts_product_priceSwitcher = (TextSwitcher) findViewById(R.id.ts_product_price);
        ts_product_priceSwitcher.setFactory(new TextViewFactory(R.style.TemperatureTextView, true));
        ts_product_priceSwitcher.setCurrentText(price1[0] + "Rs");

        ts_product_descriptionsSwitcher = (TextSwitcher) findViewById(R.id.ts_product_desc);
        ts_product_descriptionsSwitcher.setInAnimation(this, android.R.anim.fade_in);
        ts_product_descriptionsSwitcher.setOutAnimation(this, android.R.anim.fade_out);
        ts_product_descriptionsSwitcher.setFactory(new TextViewFactory(R.style.DescriptionTextView, false));
        ts_product_descriptionsSwitcher.setCurrentText(Html.fromHtml(desc[0]));


    }

    private void initCountryText() {
        productAnimDuration = getResources().getInteger(R.integer.labels_animation_duration);
        productOffset1 = getResources().getDimensionPixelSize(R.dimen.left_offset);
        productOffset2 = getResources().getDimensionPixelSize(R.dimen.card_width);
        tv_product_1 = (TextView) findViewById(R.id.tv_product_1);
        tv_product_2 = (TextView) findViewById(R.id.tv_product_2);

        tv_product_1.setX(productOffset1);
        tv_product_2.setX(productOffset2);
        tv_product_1.setText(produ[0]);
        tv_product_2.setAlpha(0f);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this,
                    new String[]{
                            android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION,

                    },
                    110);

        }
        getCartItem();
    }

    public void initBottomNavigationUI() {

        bottomNavigation.setDefaultBackgroundResource(R.drawable.bottom_navi_bg);
        bottomNavigation.setBackgroundDrawable(getResources().getDrawable(R.drawable.bottom_navi_bg));


        // Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Home", R.drawable.ic_home);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Login", R.drawable.ic_login);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem("Update", R.drawable.ic_update);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);


        //set transulant

        bottomNavigation.setTranslucentNavigationEnabled(true);


        bottomNavigation.setBehaviorTranslationEnabled(false);


        bottomNavigation.setForceTint(true);

        bottomNavigation.setTranslucentNavigationEnabled(true);

// Manage titles
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

// Set current item programmatically
        bottomNavigation.setCurrentItem(1);


// Set listeners
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                // Do something cool here...
                return true;
            }
        });
        bottomNavigation.setOnNavigationPositionListener(new AHBottomNavigation.OnNavigationPositionListener() {
            @Override
            public void onPositionChange(int y) {
                // Manage the new y position
            }
        });
        img_shping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showOrHideBottomNavigation(false);
                startActivity(new Intent(HomeActivity.this, CartSection.class));

            }
        });

        search_bar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // showOrHideBottomNavigation(true);

                startActivity(new Intent(HomeActivity.this, SearchProduct.class));
            }
        });


    }

    public void getCartItem() {

        DatabaseHandler databaseHandler = new DatabaseHandler(HomeActivity.this);
        int cnt = databaseHandler.getCartCount();
        try {
            if (cnt > 0) {
                badge.setText(cnt + "");
                badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
                badge.setTextColor(Color.WHITE);
               /* badge.setBackgroundResource(R.drawable.ovul2);
                badge.setBadgeMargin(-10, -10);
                badge.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);*/

                badge.show();
            } else {
                badge.hide();
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }


    public void showOrHideBottomNavigation(boolean show) {
        if (show) {
            bottomNavigation.restoreBottomNavigation(true);
        } else {
            bottomNavigation.hideBottomNavigation(true);
        }
    }

    public void getCategories() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "allCat";

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("onResponse", response.toString());

                        pDialog.dismiss();

                        try {

                            JSONArray j_cat = response.getJSONArray("category");
                            JSONArray j_sub_cat = response.getJSONArray("sub_category");
                            JSONArray j_sub_cat_details = response.getJSONArray("sub_category_details");


                            JSONArray j_slider = response.getJSONArray("slider");

                            JSONArray j_products = response.getJSONArray("special_product");
                            JSONArray j_discount_products = response.getJSONArray("discount_product");
                            JSONArray j_offers = response.getJSONArray("best_offers");
                            JSONArray j_daily_needs = response.getJSONArray("daily_needs");
                            JSONArray j_brands = response.getJSONArray("brands");

                            ArrayList<String> images = new ArrayList<>();
                            try {

                                for (int i = 0; i < j_slider.length(); i++) {
                                    String id = j_slider.getJSONObject(i).getString("id");
                                    String image = j_slider.getJSONObject(i).getString("image");
                                    images.add(image);

                                }
                                init(images);

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            for (int i = 0; i < j_cat.length(); i++) {
                                String id = j_cat.getJSONObject(i).getString("id");

                                String cat_type = j_cat.getJSONObject(i).getString("category_type");
                                Category c = new Category();
                                c.setId(id);
                                c.setCategory_type(cat_type);
                                catList.add(c);

                            }

                            try {
                                CategoriesFragment states = new CategoriesFragment();
                                //  city.setData(position,getActivity());
                                FragmentManager manager = getSupportFragmentManager();
                                FragmentTransaction transaction = manager.beginTransaction();
                                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

                                transaction.replace(R.id.contain, states, "categories");

                                transaction.commitAllowingStateLoss();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            for (int i = 0; i < j_sub_cat.length(); i++) {
                                String sub_cat_id = j_sub_cat.getJSONObject(i).getString("sub_cat_id");
                                String sub_cat_name = j_sub_cat.getJSONObject(i).getString("sub_cat_name");
                                String cat_id = j_sub_cat.getJSONObject(i).getString("id");
                                SubCategory subCategory = new SubCategory();
                                subCategory.setId(cat_id);
                                subCategory.setSub_cat_id(sub_cat_id);
                                subCategory.setSub_cat_name(sub_cat_name);
                                subCatList.add(subCategory);

                            }


                            for (int i = 0; i < j_sub_cat_details.length(); i++) {
                                String det_cat_id = j_sub_cat_details.getJSONObject(i).getString("det_cat_id");
                                String det_cat_name = j_sub_cat_details.getJSONObject(i).getString("det_cat_name");
                                String sub_cat_id = j_sub_cat_details.getJSONObject(i).getString("sub_cat_id");
                                String id = j_sub_cat_details.getJSONObject(i).getString("id");
                                SubCategoryDeatils subCategoryDeatils = new SubCategoryDeatils();
                                subCategoryDeatils.setId(id);
                                subCategoryDeatils.setSub_cat_id(sub_cat_id);
                                subCategoryDeatils.setDet_cat_id(det_cat_id);
                                subCategoryDeatils.setDet_cat_name(det_cat_name);
                                subCategoryDeatilsList.add(subCategoryDeatils);

                            }

                            CategoryListAdapter adapter = new CategoryListAdapter(catList, HomeActivity.this, subCatList, view_all_btn,grid_view);
                            /*int numberOfColumns = catList.size() / 2;*/
                            grid_view.setLayoutManager(new GridLayoutManager(HomeActivity.this, 2));
                            grid_view.setAdapter(adapter);

                            //  grid_view.setAdapter(new GridAdapter(catList));


                            try {
                                for (int i = 0; i < j_brands.length(); i++) {
                                    String brand_nm = j_brands.getJSONObject(i).getString("name");
                                    String brand_id = j_brands.getJSONObject(i).getString("id");

                                    Brand brand = new Brand();
                                    brand.setId(brand_id);
                                    brand.setName(brand_nm);

                                    brandList.add(brand);

                                }

                                BrandListAdapter brandListAdapter = new BrandListAdapter(brandList, HomeActivity.this);
                                int numBran = brandList.size();
                                brandlist_recy.setLayoutManager(new GridLayoutManager(HomeActivity.this, numBran));
                                brandlist_recy.setAdapter(brandListAdapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                pmodlList.clear();
                                for (int i = 0; i < j_products.length(); i++) {

                                    ProductModel pm = new ProductModel();
                                    String id = j_products.getJSONObject(i).getString("id");
                                    String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                                    String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                                    String brand_id = j_products.getJSONObject(i).getString("brand_id");
                                    String title = j_products.getJSONObject(i).getString("title");
                                    String available = j_products.getJSONObject(i).getString("available");
                                    String seller_name = j_products.getJSONObject(i).getString("seller_name");

                                    String status = j_products.getJSONObject(i).getString("status");
                                    String hstatus = j_products.getJSONObject(i).getString("hstatus");
                                    String image1 = j_products.getJSONObject(i).getString("image1");
                                    String image2 = j_products.getJSONObject(i).getString("image2");
                                    String image3 = j_products.getJSONObject(i).getString("image3");
                                    String image4 = j_products.getJSONObject(i).getString("image4");
                                    String image5 = j_products.getJSONObject(i).getString("image5");
                                    String seller = j_products.getJSONObject(i).getString("seller");
                                    String shipping = j_products.getJSONObject(i).getString("shipping");
                                    String cgst = j_products.getJSONObject(i).getString("cgst");
                                    String sgst = j_products.getJSONObject(i).getString("sgst");
                                    String seller_id = j_products.getJSONObject(i).getString("seller_id");

                                    String content = j_products.getJSONObject(i).getString("content");


                                    pm.setId(id);
                                    pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                                    pm.setSubcat_id(subcat_id);
                                    pm.setDet_cat_id(det_cat_id);
                                    pm.setBrand_id(brand_id);
                                    pm.setTitle(title);
                                    pm.setAvailable(available);
                                    pm.setSeller_name(seller_name);
                                    pm.setStatus(status);
                                    pm.setHstatus(hstatus);
                                    pm.setImage1(image1);
                                    pm.setImage2(image2);
                                    pm.setImage3(image3);
                                    pm.setImage4(image4);
                                    pm.setImage5(image5);
                                    pm.setSeller(seller);
                                    pm.setShipping(shipping);
                                    pm.setCgst(cgst);
                                    pm.setSgst(sgst);
                                    pm.setSeller_id(seller_id);

                                    pm.setContent(content);

                                    UnitModel um = new UnitModel();
                                    String unit_id = j_discount_products.getJSONObject(i).getString("unit_id");
                                    String quantity = j_discount_products.getJSONObject(i).getString("quantity");
                                    String dprice = j_discount_products.getJSONObject(i).getString("price");
                                    String mrp = j_discount_products.getJSONObject(i).getString("mrp");
                                    String unit = j_discount_products.getJSONObject(i).getString("unit");
                                    String discount = j_discount_products.getJSONObject(i).getString("dis");

                                    um.setId(unit_id);
                                    um.setQuantity(quantity);
                                    um.setDis(discount);
                                    um.setMrp(mrp);
                                    um.setPrice(dprice);
                                    um.setUnit(unit);

                                    pm.setUnitModel(um);
                                    pmodlList.add(pm);

                                }
                                sortProductList(pmodlList);
                                //initDailyEssen(pmodlList);


                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }


                            try {
                                discount_product.clear();

                                for (int i = 0; i < j_discount_products.length(); i++) {

                                    ProductModel pm = new ProductModel();

                                    String id = j_discount_products.getJSONObject(i).getString("id");
                                    String subcat_id = j_discount_products.getJSONObject(i).getString("subcat_id");
                                    String det_cat_id = j_discount_products.getJSONObject(i).getString("det_cat_id");
                                    String brand_id = j_discount_products.getJSONObject(i).getString("brand_id");
                                    String title = j_discount_products.getJSONObject(i).getString("title");
                                    String available = j_discount_products.getJSONObject(i).getString("available");
                                    String seller_name = j_discount_products.getJSONObject(i).getString("seller_name");

                                    String status = j_discount_products.getJSONObject(i).getString("status");
                                    String hstatus = j_discount_products.getJSONObject(i).getString("hstatus");
                                    String image1 = j_discount_products.getJSONObject(i).getString("image1");
                                    String image2 = j_discount_products.getJSONObject(i).getString("image2");
                                    String image3 = j_discount_products.getJSONObject(i).getString("image3");
                                    String image4 = j_discount_products.getJSONObject(i).getString("image4");
                                    String image5 = j_discount_products.getJSONObject(i).getString("image5");
                                    String seller = j_discount_products.getJSONObject(i).getString("seller");
                                    String shipping = j_discount_products.getJSONObject(i).getString("shipping");
                                    String cgst = j_discount_products.getJSONObject(i).getString("cgst");
                                    String sgst = j_discount_products.getJSONObject(i).getString("sgst");
                                    String seller_id = j_discount_products.getJSONObject(i).getString("seller_id");

                                    String content = j_discount_products.getJSONObject(i).getString("content");


                                    pm.setId(id);
                                    pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                                    pm.setSubcat_id(subcat_id);
                                    pm.setDet_cat_id(det_cat_id);
                                    pm.setBrand_id(brand_id);
                                    pm.setTitle(title);
                                    pm.setAvailable(available);
                                    pm.setSeller_name(seller_name);
                                    pm.setStatus(status);
                                    pm.setHstatus(hstatus);
                                    pm.setImage1(image1);
                                    pm.setImage2(image2);
                                    pm.setImage3(image3);
                                    pm.setImage4(image4);
                                    pm.setImage5(image5);
                                    pm.setSeller(seller);
                                    pm.setShipping(shipping);
                                    pm.setCgst(cgst);
                                    pm.setSgst(sgst);
                                    pm.setSeller_id(seller_id);
                                    pm.setContent(content);

                                    UnitModel um = new UnitModel();
                                    String unit_id = j_discount_products.getJSONObject(i).getString("unit_id");
                                    String quantity = j_discount_products.getJSONObject(i).getString("quantity");
                                    String dprice = j_discount_products.getJSONObject(i).getString("price");
                                    String mrp = j_discount_products.getJSONObject(i).getString("mrp");
                                    String unit = j_discount_products.getJSONObject(i).getString("unit");
                                    String discount = j_discount_products.getJSONObject(i).getString("dis");

                                    um.setId(unit_id);
                                    um.setQuantity(quantity);
                                    um.setDis(discount);
                                    um.setMrp(mrp);
                                    um.setPrice(dprice);
                                    um.setUnit(unit);

                                    pm.setUnitModel(um);
                                    discount_product.add(pm);


                                }

                                initProduct_slider(discount_product.subList(0, 6));


                                try {
                                    daily_need.clear();

                                    for (int i = 0; i < j_daily_needs.length(); i++) {

                                        ProductModel pm = new ProductModel();
                                        String id = j_daily_needs.getJSONObject(i).getString("id");
                                        String subcat_id = j_daily_needs.getJSONObject(i).getString("subcat_id");
                                        String det_cat_id = j_daily_needs.getJSONObject(i).getString("det_cat_id");
                                        String brand_id = j_daily_needs.getJSONObject(i).getString("brand_id");
                                        String title = j_daily_needs.getJSONObject(i).getString("title");
                                        String available = j_daily_needs.getJSONObject(i).getString("available");
                                        String seller_name = j_daily_needs.getJSONObject(i).getString("seller_name");

                                        String status = j_daily_needs.getJSONObject(i).getString("status");
                                        String hstatus = j_daily_needs.getJSONObject(i).getString("hstatus");
                                        String image1 = j_daily_needs.getJSONObject(i).getString("image1");
                                        String image2 = j_daily_needs.getJSONObject(i).getString("image2");
                                        String image3 = j_daily_needs.getJSONObject(i).getString("image3");
                                        String image4 = j_daily_needs.getJSONObject(i).getString("image4");
                                        String image5 = j_daily_needs.getJSONObject(i).getString("image5");
                                        String seller = j_daily_needs.getJSONObject(i).getString("seller");
                                        String shipping = j_daily_needs.getJSONObject(i).getString("shipping");
                                        String cgst = j_daily_needs.getJSONObject(i).getString("cgst");
                                        String sgst = j_daily_needs.getJSONObject(i).getString("sgst");
                                        String seller_id = j_daily_needs.getJSONObject(i).getString("seller_id");

                                        String content = j_daily_needs.getJSONObject(i).getString("content");


                                        pm.setId(id);
                                        pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                                        pm.setSubcat_id(subcat_id);
                                        pm.setDet_cat_id(det_cat_id);
                                        pm.setBrand_id(brand_id);
                                        pm.setTitle(title);
                                        pm.setAvailable(available);
                                        pm.setSeller_name(seller_name);
                                        pm.setStatus(status);
                                        pm.setHstatus(hstatus);
                                        pm.setImage1(image1);
                                        pm.setImage2(image2);
                                        pm.setImage3(image3);
                                        pm.setImage4(image4);
                                        pm.setImage5(image5);
                                        pm.setSeller(seller);
                                        pm.setShipping(shipping);
                                        pm.setCgst(cgst);
                                        pm.setSgst(sgst);
                                        pm.setSeller_id(seller_id);

                                        UnitModel um = new UnitModel();
                                        String unit_id = j_discount_products.getJSONObject(i).getString("unit_id");
                                        String quantity = j_discount_products.getJSONObject(i).getString("quantity");
                                        String dprice = j_discount_products.getJSONObject(i).getString("price");
                                        String mrp = j_discount_products.getJSONObject(i).getString("mrp");
                                        String unit = j_discount_products.getJSONObject(i).getString("unit");
                                        String discount = j_discount_products.getJSONObject(i).getString("dis");

                                        um.setId(unit_id);
                                        um.setQuantity(quantity);
                                        um.setDis(discount);
                                        um.setMrp(mrp);
                                        um.setPrice(dprice);
                                        um.setUnit(unit);

                                        pm.setUnitModel(um);


                                        pm.setContent(content);

                                        daily_need.add(pm);


                                    }


                                    initHori_product_slider(daily_need);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                                try {

                                    offers_product.clear();
                                    for (int k = 0; k < j_offers.length(); k++) {

                                        Offers off = new Offers();
                                        off.setId(j_offers.getJSONObject(k).getString("id"));
                                        off.setOffer(j_offers.getJSONObject(k).getString("offer"));
                                        off.setStart_date(j_offers.getJSONObject(k).getString("start_date"));
                                        off.setEnd_date(j_offers.getJSONObject(k).getString("end_date"));
                                        off.setImage(j_offers.getJSONObject(k).getString("image"));
                                        off.setProduct_id(j_offers.getJSONObject(k).getString("product_id"));
                                        off.setCat_id(j_offers.getJSONObject(k).getString("cat_id"));
                                        off.setSub_cat_id(j_offers.getJSONObject(k).getString("sub_cat_id"));
                                        off.setDet_cat_id(j_offers.getJSONObject(k).getString("det_cat_id"));

                                        offers_product.add(off);

                                    }
                                    initBestOffers(offers_product);
                                    initDailyEssen(offers_product.subList(4, 11));

                                } catch (Exception ex) {

                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        } catch (Exception e) {
                            pDialog.dismiss();
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                VolleyLog.d("byee", "Error: " + error.getMessage());
                pDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(req, "json_array_req");

    }

    private void initBestOffers(final List<Offers> offersList) {
        ImageView offer1, offer2, offer3, offer4;
        offer1 = (ImageView) findViewById(R.id.offer1);
        offer2 = (ImageView) findViewById(R.id.offer2);
        offer3 = (ImageView) findViewById(R.id.offer3);
        offer4 = (ImageView) findViewById(R.id.offer4);

        try {
            Picasso.with(HomeActivity.this).load(Uri.parse(offersList.get(0).getImage())).into(offer1);
            Picasso.with(HomeActivity.this).load(Uri.parse(offersList.get(1).getImage())).into(offer2);
            Picasso.with(HomeActivity.this).load(Uri.parse(offersList.get(2).getImage())).into(offer3);
            Picasso.with(HomeActivity.this).load(Uri.parse(offersList.get(3).getImage())).into(offer4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Intent offInt = new Intent(HomeActivity.this, TopOffersProduct.class);
       /* offer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offInt.putExtra("subcat_id", offersList.get(0).getSub_cat_id());
                startActivity(offInt);
            }
        });

        offer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offInt.putExtra("subcat_id", offersList.get(1).getSub_cat_id());
                startActivity(offInt);
            }
        });

        offer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offInt.putExtra("subcat_id", offersList.get(2).getSub_cat_id());
                startActivity(offInt);
            }
        });

        offer4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                offInt.putExtra("subcat_id", offersList.get(3).getSub_cat_id());
                startActivity(offInt);
            }
        });*/


    }

    private void init(final ArrayList<String> images) {
//        for(int i=0;i<XMEN.length;i++)
//            XMENArray.add(XMEN[i]);

        final ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new slideAdapter(HomeActivity.this, images));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public class GridAdapter extends BaseAdapter {


        List<Category> catList1;

        public GridAdapter(List<Category> catList) {
            this.catList1 = catList;
        }

        @Override
        public int getCount() {
            return catList1.size();
        }

        @Override
        public Object getItem(int i) {
            return catList1.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            View grid;
            ImageView cat_icon;
            TextView cat_name;
            LayoutInflater inflater = (LayoutInflater) (HomeActivity.this).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                grid = new View(HomeActivity.this);
                grid = inflater.inflate(R.layout.grid_row, null);

                cat_icon = (ImageView) grid.findViewById(R.id.cat_icon);
                cat_name = (TextView) grid.findViewById(R.id.cat_name);

                cat_name.setText(catList1.get(position).getCategory_type());
                try {
                    cat_icon.setImageDrawable(getResources().getDrawable(retCat(position)));
                    cat_icon.setBackgroundColor(Color.WHITE);
                    cat_icon.setId(position);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                grid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ArrayList<SubCategory> subCategoryArrayList = new ArrayList<SubCategory>();
                        for (int i = 0; i < subCatList.size(); i++) {
                            if (subCatList.get(i).getId().equals(catList1.get(position).getId())) {
                                subCategoryArrayList.add(subCatList.get(i));
                            }

                        }


                        Intent int1 = new Intent(HomeActivity.this, ProductBySuperCategory.class);
                        int1.putExtra("brandList", (ArrayList) HomeActivity.brandList);
                        int1.putExtra("sub_cat", subCategoryArrayList);
                        int1.putExtra("cat_id", catList1.get(position).getId());
                        int1.putExtra("cat_nm", catList1.get(position).getCategory_type());
                        int1.putExtra("sub_category", "0");
                        startActivity(int1);

                    }
                });

            } else {
                grid = (View) convertView;

            }


            return grid;


        }

    }


    public int retCat(int p) {
        int a = R.drawable.backet;
        switch (p) {
            case 0:
                a = R.drawable.ic_fruit_n_veg;
                break;
            case 1:
                a = R.drawable.ic_grain_oil_masala;
                break;

            case 2:
                a = R.drawable.ic_backery_cake;
                break;

            case 3:
                a = R.drawable.ic_beverage;
                break;
            case 4:
                a = R.drawable.ic_branded_food;
                break;

            case 5:
                a = R.drawable.ic_beauty_hygiene;
                break;

            case 6:
                a = R.drawable.ic_household;
                break;
            case 7:
                a = R.drawable.ic_gourmet_food;
                break;

            default:
                break;


        }
        return a;

    }


    private class OnCardClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            final CardSliderLayoutManager lm = (CardSliderLayoutManager) recyclerView.getLayoutManager();

            if (lm.isSmoothScrolling()) {
                return;
            }

            final int activeCardPosition = lm.getActiveCardPosition();
            if (activeCardPosition == RecyclerView.NO_POSITION) {
                return;
            }

            final int clickedPosition = recyclerView.getChildAdapterPosition(view);
            if (clickedPosition == activeCardPosition) {
                //TODO Ashvini (on Click on Card )

                Intent product_Details = new Intent(HomeActivity.this, ProdDetails.class);
                product_Details.putExtra("product_details", pmodlList.get(activeCardPosition));
                startActivity(product_Details);

            } else if (clickedPosition > activeCardPosition) {
                recyclerView.smoothScrollToPosition(clickedPosition);
                onActiveCardChange(clickedPosition);
            }
        }
    }

    private class TextViewFactory implements ViewSwitcher.ViewFactory {

        @StyleRes
        final int styleId;
        final boolean center;

        TextViewFactory(@StyleRes int styleId, boolean center) {
            this.styleId = styleId;
            this.center = center;
        }

        @SuppressWarnings("deprecation")
        @Override
        public View makeView() {
            final TextView textView = new TextView(HomeActivity.this);

            if (center) {
                textView.setGravity(Gravity.CENTER);
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                textView.setTextAppearance(HomeActivity.this, styleId);
            } else {
                textView.setTextAppearance(styleId);
            }

            return textView;
        }

    }


    private void onActiveCardChange() {
        final int pos = layoutManger.getActiveCardPosition();
        if (pos == RecyclerView.NO_POSITION || pos == currentPosition) {
            return;
        }

        onActiveCardChange(pos);
    }


    private void onActiveCardChange(int pos) {
        int animH[] = new int[]{R.anim.slide_in_right, R.anim.slide_out_left};
        int animV[] = new int[]{R.anim.slide_in_top, R.anim.slide_out_bottom};

        final boolean left2right = pos < currentPosition;
        if (left2right) {
            animH[0] = R.anim.slide_in_left;
            animH[1] = R.anim.slide_out_right;

            animV[0] = R.anim.slide_in_bottom;
            animV[1] = R.anim.slide_out_top;
        }

        setProductText(produ[pos % produ.length], left2right);

        ts_product_priceSwitcher.setInAnimation(HomeActivity.this, animH[0]);
        ts_product_priceSwitcher.setOutAnimation(HomeActivity.this, animH[1]);
        ts_product_priceSwitcher.setText(price1[pos % price1.length] + "Rs");
        ts_product_descriptionsSwitcher.setText(Html.fromHtml(desc[pos % desc.length]));

        currentPosition = pos;
    }

    public void initProduct_slider(List<ProductModel> productModelList) {
        ProductAdapter adapter = new ProductAdapter(productModelList, HomeActivity.this);
        product_slider.setLayoutManager(new GridLayoutManager(this, 2));
        product_slider.setAdapter(adapter);

    }


    public void initHori_product_slider(List<ProductModel> productModelList) {
        HorizontalProductAdapter adapter = new HorizontalProductAdapter(productModelList, HomeActivity.this);
        hori_product_slider.setLayoutManager(new LinearLayoutManager(this));
        hori_product_slider.setAdapter(adapter);
        //hori_product_slider
    }

    public void sortProductList(final List<ProductModel> plList) {


        int num = plList.size();
        try {
            produ = new String[num];
            price1 = new String[num];
            desc = new String[num];
            pics = new String[num];
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            for (int i = 0; i < plList.size(); i++) {
                produ[i] = plList.get(i).getTitle();
                desc[i] = plList.get(i).getContent();
                price1[i] = plList.get(i).getUnitModel().getPrice();
                pics[i] = plList.get(i).getImage1();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            initCountryText();
            initSwitchers();

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            sliderAdapter = new CardSliderAdapter(pics, HomeActivity.this, new OnCardClickListener());
            initRecyclerView();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setProductText(String text, boolean left2right) {
        final TextView invisibleText;
        final TextView visibleText;
        if (tv_product_1.getAlpha() > tv_product_2.getAlpha()) {
            visibleText = tv_product_1;
            invisibleText = tv_product_2;
        } else {
            visibleText = tv_product_2;
            invisibleText = tv_product_1;
        }

        final int vOffset;
        if (left2right) {
            invisibleText.setX(0);
            vOffset = productOffset2;
        } else {
            invisibleText.setX(productOffset2);
            vOffset = 0;
        }

        invisibleText.setText(text);

        final ObjectAnimator iAlpha = ObjectAnimator.ofFloat(invisibleText, "alpha", 2f);
        final ObjectAnimator vAlpha = ObjectAnimator.ofFloat(visibleText, "alpha", 0f);
        final ObjectAnimator iX = ObjectAnimator.ofFloat(invisibleText, "x", productOffset1);
        final ObjectAnimator vX = ObjectAnimator.ofFloat(visibleText, "x", vOffset);

        final AnimatorSet animSet = new AnimatorSet();
        animSet.playTogether(iAlpha, vAlpha, iX, vX);
        animSet.setDuration(productAnimDuration);
        animSet.start();
    }

}
