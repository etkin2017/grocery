package com.emate.grocerystore.develop.activities.models;

/**
 * Created by Wasim on 13/01/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "GroceryStore";

    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";
    private static final String WHO_LOGIN = "WHO_LOGIN";
    private static final String USER_NAME = "UserName";
    private static final String USER_ID = "UserID";
    private static final String USER_MOBILENO = "UserMobileNo";
    private static final String USER_EMAIL = "UserEmail";


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        editor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        editor.commit();
    }

    public String getWhoLogin() {
        return pref.getString(WHO_LOGIN, "");
    }


    public void setWhoLogin(String whoLogin) {
        editor.putString(WHO_LOGIN, whoLogin);
        editor.commit();

    }


    public void setUserName(String username) {
        editor.putString(USER_NAME, username);
        editor.commit();
    }

    public void setUserID(String id) {
        editor.putString(USER_ID, id);
        editor.commit();
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(USER_MOBILENO, mobileNumber);
        editor.commit();
    }

    public void setUserEmail(String email) {
        editor.putString(USER_EMAIL, email);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(USER_MOBILENO, "");

    }

    public String getUserEmail() {
        return pref.getString(USER_EMAIL, "");

    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGGED_IN, false);
    }

    public String getUsername() {
        return pref.getString(USER_NAME, "");
    }

    public String getID() {
        return pref.getString(USER_ID, "");
    }
}

