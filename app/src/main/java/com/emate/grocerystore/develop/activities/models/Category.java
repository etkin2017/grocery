package com.emate.grocerystore.develop.activities.models;

/**
 * Created by Ashvini on 11/7/2017.
 */

public class Category {
    String id,category_type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory_type() {
        return category_type;
    }

    public void setCategory_type(String category_type) {
        this.category_type = category_type;
    }
}
