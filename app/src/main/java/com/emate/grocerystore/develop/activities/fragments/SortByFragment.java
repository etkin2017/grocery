package com.emate.grocerystore.develop.activities.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ProductBySuperCategory;
import com.emate.grocerystore.develop.activities.interfaces.FilterListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class SortByFragment extends Fragment {

    FilterListener sortBy;
    RadioButton new_product, low_to_high_product, high_to_low_product;
    View view;
    Button btn_filter;

    String sortt = "1";

    public SortByFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            sortBy = (FilterListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sort_by, container, false);
        initView(view);



        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
                passData(sortt);
                ((ProductBySuperCategory) getActivity()).onDetachedSortByFragment();

            }
        });


        return view;
    }

    public void passData(String dt) {
        try {
            sortBy.onSortByDataPass(dt);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initView(View view) {
        high_to_low_product = (RadioButton) view.findViewById(R.id.high_to_low_product);
        new_product = (RadioButton) view.findViewById(R.id.new_product);
        low_to_high_product = (RadioButton) view.findViewById(R.id.low_to_high_product);

        btn_filter = (Button) view.findViewById(R.id.btn_filter);

        if(ProductBySuperCategory.pos==1){
            new_product.setChecked(true);
        }else if(ProductBySuperCategory.pos==2){
            low_to_high_product.setChecked(true);
        }else{
            high_to_low_product.setChecked(true);
        }

        high_to_low_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    //   passData("3");
                    sortt = "3";
                    ProductBySuperCategory.pos=3;
                }

            }
        });

        low_to_high_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    //   passData("2");
                    sortt = "2";
                    ProductBySuperCategory.pos=2;
                }

            }
        });

        new_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b) {
                    // passData("1");
                    sortt = "1";
                    ProductBySuperCategory.pos=1;
                }

            }
        });
    }

}
