package com.emate.grocerystore.develop.activities.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.CartAdapter;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.Cart;
import com.emate.grocerystore.develop.activities.models.PrefManager;
import com.emate.grocerystore.develop.activities.views.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CartSection extends AppCompatActivity {
    RecyclerView cartList;
    List<Cart> cartList1 = new ArrayList<>();
    TextView shipping_fee, total, deliver_to, change_address;
    LinearLayout proceed_to_pay;
    CardView ptotal_container;
    TextView no_result;
    CartAdapter adapter;
    List<Cart> carts = new ArrayList<>();
    DatabaseHandler databaseHandler;
    String unitIdsList = "";

    public String priceList() {
        String prcList = "";
        for (int i = 0; i < cartList1.size(); i++) {
            long singleProdPrc = Long.parseLong(cartList1.get(i).getPmData().getUnitModel().getPrice()) * cartList1.get(i).getQty();
            if (i == (cartList1.size() - 1)) {
                prcList = prcList + singleProdPrc + "";
            } else {
                prcList = prcList + singleProdPrc + " , ";
            }
        }
        return prcList;

    }

    @Override
    protected void onResume() {
        super.onResume();
        getProd();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_section);
        init();


        proceed_to_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PrefManager prefManager = new PrefManager(CartSection.this);
                boolean chkLng = prefManager.isUserLoggedIn();
                String amount = total.getText().toString();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("user_id", prefManager.getID() + "");
                    jsonObject.put("order_dt_tim", System.currentTimeMillis() + "");
                    jsonObject.put("amt", priceList());
                    jsonObject.put("payment_mode", "cash on delivery");

                    jsonObject.put("product_id", databaseHandler.prodIdList());
                    jsonObject.put("unit_id", databaseHandler.unitIdsArray());
                    jsonObject.put("prod_qty", databaseHandler.qtyIdList());




                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (chkLng) {
                    // //id,user_id,order_dt_tim,total_amt,payment_mode,delivery_status,delivery_dt_tim
                    //product_id,unit_id,prod_qty
                    Intent intent = new Intent(CartSection.this, BuyProduct.class);
                    intent.putExtra("groceryData", jsonObject.toString());
                    intent.putExtra("amount", amount);
                    startActivity(intent);


                } else {
                    //startActivity(new Intent(CartSection.this, LoginActivity.class));
                    Intent intent = new Intent(CartSection.this, LoginActivity.class);
                    intent.putExtra("groceryData", jsonObject.toString());
                    intent.putExtra("amount", amount);
                    startActivity(intent);

                }
            }
        });

        change_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog d = new Dialog(CartSection.this, R.style.Custom_Dialog);
                d.setContentView(R.layout.change_address);
                final EditText edt_address = (EditText) d.findViewById(R.id.edt_address);
                Button btn_change_address = (Button) d.findViewById(R.id.btn_change_address);

              /*  btn_change_address.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(edt_address.getText().toString())) {
                            AppController.retShowAlertDialod("Enter Address", CartSection.this);
                        } else {
                            removeCartItem(cartList1.get(0).getCartid(), edt_address.getText().toString(), "chng_cartAddress");
                            d.dismiss();
                        }
                    }
                });*/

                d.show();


            }
        });

        cartList.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {

                        TextView remove = (TextView) view.findViewById(R.id.remove_cart);
                        remove.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                DatabaseHandler databaseHandler = new DatabaseHandler(CartSection.this);
                                databaseHandler.deleteCart(cartList1.get(position));
                                getProd();
                                // removeCartItem(cartList1.get(position).getCartid(), "", "delCartItem");
                            }
                        });
                    }
                }));


    }

    public void removeCartItem(String cartid, String address, String url1) {


        final ProgressDialog pDialog = new ProgressDialog(CartSection.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + url1;// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cartid", cartid);
            jsonObject.put("chng_addrs", address);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    boolean b = response.getBoolean("success");
                    if (b == true) {
                        getProd();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(CartSection.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "remove_cartItem");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getProd() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getCartProductDetails";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            unitIdsList = databaseHandler.unitIdList();
            jsonObject.put("unit_id", unitIdsList);
            //  jsonObject.put("",);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");


                    try {
                        if (j_products.length() > 0) {
                            cartList1 = AppController.cartListfromJson(j_products);
                            ptotal_container.setVisibility(View.VISIBLE);
                            no_result.setVisibility(View.GONE);
                            cartList.setVisibility(View.VISIBLE);

                            if (cartList1.get(0).getChng_addrs().equalsIgnoreCase("null")) {
                                AppController.setData(deliver_to, cartList1.get(0).getUsersData().getShipping_add());
                            } else {
                                AppController.setData(deliver_to, cartList1.get(0).getChng_addrs());
                            }
                        } else {
                            cartList.setVisibility(View.GONE);
                            ptotal_container.setVisibility(View.GONE);
                            no_result.setVisibility(View.VISIBLE);
                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    CartAdapter adapter = new CartAdapter(CartSection.this, cartList1, total, shipping_fee, deliver_to);
                    cartList.setLayoutManager(new LinearLayoutManager(CartSection.this));
                    cartList.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                //Toast.makeText(CartSection.this, "Network Error", Toast.LENGTH_LONG).show();
                cartList.setVisibility(View.GONE);
                ptotal_container.setVisibility(View.GONE);
                no_result.setVisibility(View.VISIBLE);

            }
        });
        AppController.getInstance().addToRequestQueue(req, "getCart");

    }


    public void init() {

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Cart");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        cartList = (RecyclerView) findViewById(R.id.cartList);
        shipping_fee = (TextView) findViewById(R.id.shipping_fee);
        total = (TextView) findViewById(R.id.total);
        deliver_to = (TextView) findViewById(R.id.deliver_to);
        change_address = (TextView) findViewById(R.id.change_address);
        proceed_to_pay = (LinearLayout) findViewById(R.id.proceed_to_pay);
        ptotal_container = (CardView) findViewById(R.id.ptotal_container);
        no_result = (TextView) findViewById(R.id.no_result);
        databaseHandler = new DatabaseHandler(CartSection.this);
        carts = databaseHandler.getAllCarts();


        adapter = new CartAdapter(CartSection.this, cartList1, total, shipping_fee, deliver_to);
    }
}
