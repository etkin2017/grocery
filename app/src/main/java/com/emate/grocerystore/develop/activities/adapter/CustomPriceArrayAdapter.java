package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import java.util.List;

/**
 * Created by Ashvini on 1/24/2018.
 */

public class CustomPriceArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<UnitModel> items;
    private final int mResource;

    public CustomPriceArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                                   @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public
    @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView p_quantity = (TextView) view.findViewById(R.id.p_quantity);
        TextView p_unit = (TextView) view.findViewById(R.id.p_unit);
        TextView p_mrp = (TextView) view.findViewById(R.id.p_mrp);
        TextView p_price = (TextView) view.findViewById(R.id.p_price);

        UnitModel unitModel = items.get(position);

        p_quantity.setText(unitModel.getQuantity());
        p_unit.setText(unitModel.getUnit() +" - ");
        p_mrp.setText(mContext.getResources().getString(R.string.indian_rupee)+unitModel.getMrp());
        p_price.setText(mContext.getResources().getString(R.string.indian_rupee)+" "+unitModel.getPrice());
        Config.strikeThroughText(p_mrp);
        return view;
    }
}