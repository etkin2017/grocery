package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 1/27/2018.
 */

public class Orders implements Serializable{
    String order_id, user_id, order_dt_tim, total_amt, payment_mode, delivery_status, delivery_dt_tim, product_id, unit_id, prod_qty, txnid;


    ProductModel pm ;
    Users users;

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public ProductModel getPm() {
        return pm;
    }

    public void setPm(ProductModel pm) {
        this.pm = pm;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_dt_tim() {
        return order_dt_tim;
    }

    public void setOrder_dt_tim(String order_dt_tim) {
        this.order_dt_tim = order_dt_tim;
    }

    public String getTotal_amt() {
        return total_amt;
    }

    public void setTotal_amt(String total_amt) {
        this.total_amt = total_amt;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }

    public String getDelivery_status() {
        return delivery_status;
    }

    public void setDelivery_status(String delivery_status) {
        this.delivery_status = delivery_status;
    }

    public String getDelivery_dt_tim() {
        return delivery_dt_tim;
    }

    public void setDelivery_dt_tim(String delivery_dt_tim) {
        this.delivery_dt_tim = delivery_dt_tim;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(String unit_id) {
        this.unit_id = unit_id;
    }

    public String getProd_qty() {
        return prod_qty;
    }

    public void setProd_qty(String prod_qty) {
        this.prod_qty = prod_qty;
    }

    public String getTxnid() {
        return txnid;
    }

    public void setTxnid(String txnid) {
        this.txnid = txnid;
    }
}
