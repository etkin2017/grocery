package com.emate.grocerystore.develop.activities.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPassword extends AppCompatActivity {

    Button forget_password;
    EditText email_id;
    Dialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        // Config.setActBar("Forgot Password", ForgotPassword.this, true);

        email_id = (EditText) findViewById(R.id.email_id);
        forget_password = (Button) findViewById(R.id.forget_password);


        alertDialog = new Dialog(ForgotPassword.this);
        alertDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.alert_dialog);
        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean chk_emailid = android.util.Patterns.EMAIL_ADDRESS.matcher(email_id.getText().toString()).matches();

                if (TextUtils.isEmpty(email_id.getText().toString())) {
                    AppController.retShowAlertDialod("Enter Email Id", ForgotPassword.this);
                } else if (chk_emailid == false) {
                    AppController.retShowAlertDialod("Enter valid email id", ForgotPassword.this);
                } else {
                    HashMap<String, String> hashMap = new HashMap<String, String>();

                    forgotPassword();


                }

            }
        });

    }


    public void forgotPassword() {


        final ProgressDialog pDialog = new ProgressDialog(ForgotPassword.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getUserByEmailId";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            //user_name,email,mb_no,shipping_add,total_order,user_status

            jsonObject.put("email", email_id.getText().toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    //boolean b = response.getBoolean("success");
                    if (response.getJSONArray("success").length() > 0) {
                        // Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();


                        JSONArray jsonArray = response.getJSONArray("success");
                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        String emailId = jsonObject.getString("email");
                        String name = jsonObject.getString("user_name");
                        String id = jsonObject.getString("id");

                        send_mail(emailId, id, name);
                        AppController.retShowAlertDialod("Successfully Send Email Please Check Register  Email ", ForgotPassword.this);
                        // send_mail("Forget Password \n Your email id is" + email_id.getText().toString() + " \n ", "", "");

                    } else {
                        //  Toast.makeText(getApplicationContext(), res.getString("message"), Toast.LENGTH_LONG).show();

                        AppController.retShowAlertDialod("Invalid Email Id", ForgotPassword.this);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ForgotPassword.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "remove_cartItem");

    }


    public void send_mail(final String emailid, final String id, final String name) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait while sending mail");
        dialog.show();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", name);
            jsonObject.put("email", emailid);
            jsonObject.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Config.EMail_Url + "Fpassword/index", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(7000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, "remove_cartItem");

    }


}
