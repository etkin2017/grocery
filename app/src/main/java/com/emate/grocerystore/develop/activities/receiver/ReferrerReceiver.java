package com.emate.grocerystore.develop.activities.receiver;

/**
 * Created by Ashvini on 02-08-2016.
 */

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;

/**
 * To test a referrer broadcast from ADB:
 *
 * adb shell
 *
 * am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.android.custom.referrer.receiver/.ReferrerReceiver --es "referrer" "utm_source=YourAppName&utm_medium=YourMedium&utm_campaign=YourCampaign&utm_content=YourSampleContent"
 */
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;

import org.json.JSONObject;


/**
 * To test a referrer broadcast from ADB:
 * <p/>
 * adb shell
 * <p/>
 * am broadcast -a com.android.vending.INSTALL_REFERRER -n com.example.android.custom.referrer.receiver/.ReferrerReceiver --es "referrer" "utm_source=YourAppName&utm_medium=YourMedium&utm_campaign=YourCampaign&utm_content=YourSampleContent"
 */
public class ReferrerReceiver extends BroadcastReceiver {

    String unique_id="";
    Context ctx;

    @Override
    public void onReceive(Context context, Intent intent) {
        ctx = context;
        final String action = intent.getAction();
        if (action != null && TextUtils.equals(action, "com.android.vending.INSTALL_REFERRER")) {
            try {
                final String referrer = intent.getStringExtra("referrer");
// Parse parameters
                String[] params = referrer.split("&");
                for (String p : params) {
                    if (p.startsWith("utm_content=")) {
                        final String content = p.substring("utm_content=".length());


                        unique_id = Settings.Secure.getString(ctx.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                        //Log.e("UNIQUE ID", unique_id);

/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_content", content);

                        addReferral(content,unique_id);


                    }

                    if (p.startsWith("utm_campaign=")) {
                        final String content = p.substring("utm_campaign=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_campaign", content);

                    }

                    if (p.startsWith("utm_source=")) {
                        final String content = p.substring("utm_source=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_source", content);

                    }


                    if (p.startsWith("utm_medium=")) {
                        final String content = p.substring("utm_medium=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_medium", content);

                    }
                    if (p.startsWith("utm_term=")) {
                        final String content = p.substring("utm_term=".length());
/**
 * USE HERE YOUR CONTENT (i.e. configure the app based on the link the user clicked)
 */
                        //Log.i("Receiver_utm_term", content);

                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
/**
 * OPTIONAL: Forward the intent to Google Analytics V2 receiver
 */
// new com.google.analytics.tracking.android.AnalyticsReceiver().onReceive(context, intent);
        }
    }

    public void addReferral(final String user_id,final  String deviceId) {
       // store record into database

        final ProgressDialog pDialog = new ProgressDialog(ctx);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "addReferral";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user_id);
            jsonObject.put("device_id", deviceId);
            jsonObject.put("tstamp", System.currentTimeMillis()+"");


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    boolean b = response.getBoolean("success");
                    if (b == true) {
                      //  getProd();
                        Toast.makeText(ctx, "Success", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ctx, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "add_to_referral");
    }


}