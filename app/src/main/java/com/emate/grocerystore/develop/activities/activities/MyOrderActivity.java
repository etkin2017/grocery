package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.OrdersByDateAdapter;
import com.emate.grocerystore.develop.activities.models.Orders;
import com.emate.grocerystore.develop.activities.models.PrefManager;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyOrderActivity extends AppCompatActivity {

    RecyclerView order_history;
    TextView no_result_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        init();
        getProd();
    }


    public void init() {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Orders");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        order_history = (RecyclerView) findViewById(R.id.order_history);
        no_result_found = (TextView) findViewById(R.id.no_result_found);


    }


    public List<String> getDates(List<Orders> productModelList) {
        List<String> dates = new ArrayList<>();
        for (int i = 0; i < productModelList.size(); i++) {
            if (!dates.contains(productModelList.get(i).getOrder_dt_tim())) {
                dates.add(productModelList.get(i).getOrder_dt_tim());
            }
        }
        return dates;
    }

    public void getProd() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getMyOrderList";
        JSONObject jsonObject = new JSONObject();
        PrefManager prefManager = new PrefManager(MyOrderActivity.this);

        try {
            jsonObject.put("user_id", prefManager.getID());


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<Orders> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");

                    if (j_products.length() == 0) {
                        no_result_found.setVisibility(View.VISIBLE);
                        order_history.setVisibility(View.GONE);
                    } else {
                        no_result_found.setVisibility(View.GONE);
                        order_history.setVisibility(View.VISIBLE);
                    }

                    try {

                        for (int i = 0; i < j_products.length(); i++) {
                            Orders orders = new Orders();
//order_id, user_id, order_dt_tim, total_amt, payment_mode, delivery_status, delivery_dt_tim, product_id, unit_id, prod_qty, txnid
                            String order_id = j_products.getJSONObject(i).getString("orderid");
                            String order_dt_tim = j_products.getJSONObject(i).getString("order_dt_tim");
                            String total_amt = j_products.getJSONObject(i).getString("total_amt");
                            String payment_mode = j_products.getJSONObject(i).getString("payment_mode");
                            String delivery_status = j_products.getJSONObject(i).getString("delivery_status");
                            String delivery_dt_tim = j_products.getJSONObject(i).getString("delivery_dt_tim");
                            String txnid = j_products.getJSONObject(i).getString("txnid");
                            String prod_qty = j_products.getJSONObject(i).getString("prod_qty");

                            orders.setOrder_id(order_id);
                            orders.setOrder_dt_tim(order_dt_tim);
                            orders.setDelivery_dt_tim(delivery_dt_tim);
                            orders.setDelivery_status(delivery_status);
                            orders.setPayment_mode(payment_mode);
                            orders.setProd_qty(prod_qty);
                            orders.setTxnid(txnid);
                            orders.setTotal_amt(total_amt);

                            ProductModel pm = new ProductModel();
                            String id = j_products.getJSONObject(i).getString("id");
                            String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                            String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                            String brand_id = j_products.getJSONObject(i).getString("brand_id");
                            String title = j_products.getJSONObject(i).getString("title");
                            String available = j_products.getJSONObject(i).getString("available");
                            String seller_name = j_products.getJSONObject(i).getString("seller_name");

                            String status = j_products.getJSONObject(i).getString("status");
                            String hstatus = j_products.getJSONObject(i).getString("hstatus");
                            String image1 = j_products.getJSONObject(i).getString("image1");
                            String image2 = j_products.getJSONObject(i).getString("image2");
                            String image3 = j_products.getJSONObject(i).getString("image3");
                            String image4 = j_products.getJSONObject(i).getString("image4");
                            String image5 = j_products.getJSONObject(i).getString("image5");
                            String seller = j_products.getJSONObject(i).getString("seller");
                            String shipping = j_products.getJSONObject(i).getString("shipping");
                            String cgst = j_products.getJSONObject(i).getString("cgst");
                            String sgst = j_products.getJSONObject(i).getString("sgst");
                            String seller_id = j_products.getJSONObject(i).getString("seller_id");

                            String content = j_products.getJSONObject(i).getString("content");


                            pm.setId(id);
                            pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                            pm.setSubcat_id(subcat_id);
                            pm.setDet_cat_id(det_cat_id);
                            pm.setBrand_id(brand_id);
                            pm.setTitle(title);
                            pm.setAvailable(available);
                            pm.setSeller_name(seller_name);
                            pm.setStatus(status);
                            pm.setHstatus(hstatus);
                            pm.setImage1(image1);
                            pm.setImage2(image2);
                            pm.setImage3(image3);
                            pm.setImage4(image4);
                            pm.setImage5(image5);
                            pm.setSeller(seller);
                            pm.setShipping(shipping);
                            pm.setCgst(cgst);
                            pm.setSgst(sgst);
                            pm.setSeller_id(seller_id);
                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("unit_id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);

                            pm.setUnitModel(um);
                            pm.setContent(content);
                            orders.setPm(pm);
                            productModelList.add(orders);

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    List<String> alldts = getDates(productModelList);
                    OrdersByDateAdapter adapter = new OrdersByDateAdapter(productModelList, MyOrderActivity.this, alldts);
                    order_history.setLayoutManager(new LinearLayoutManager(MyOrderActivity.this));
                    order_history.setAdapter(adapter);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(MyOrderActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
