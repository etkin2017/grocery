package com.emate.grocerystore.develop.activities.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.emate.grocerystore.develop.activities.fragments.RefinedByFragment;
import com.emate.grocerystore.develop.activities.fragments.SortByFragment;
import com.emate.grocerystore.develop.activities.models.Brand;

import java.util.List;

/**
 * Created by Ashvini on 1/19/2018.
 */

public class FilterPager extends FragmentStatePagerAdapter {

    List<Brand> brandList;

    public FilterPager(FragmentManager fm, List<Brand> brandList) {
        super(fm);
        this.brandList = brandList;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {


            case 0:

                //    getSupportFragmentManager().beginTransaction().add(R.id.container, cff, "supp1").addToBackStack("supp1").commit();
                RefinedByFragment rbf = new RefinedByFragment();
                rbf.setBrandList(brandList);
                return rbf;
            case 1:

                //   getSupportFragmentManager().beginTransaction().add(R.id.container, cff1, "supp2").addToBackStack("supp2").commit();
                return new SortByFragment();


        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return "Filter By";
            case 1:
                return "Sort By";


        }
        return null;
    }
}
 /*extends FragmentStatePagerAdapter {

    //integer to count number of tabs
    int tabCount;

    //Constructor to the class
    public FilterPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                RefinedByFragment tab1 = new RefinedByFragment();
                return tab1;
            case 1:
                SortByFragment tab2 = new SortByFragment();
                return tab2;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}*/