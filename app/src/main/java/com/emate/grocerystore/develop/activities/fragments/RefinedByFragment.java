package com.emate.grocerystore.develop.activities.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ProductBySuperCategory;
import com.emate.grocerystore.develop.activities.adapter.FilterAdapter;
import com.emate.grocerystore.develop.activities.expandableview.ExpandableRelativeLayout;
import com.emate.grocerystore.develop.activities.interfaces.FilterListener;
import com.emate.grocerystore.develop.activities.models.Brand;
import com.emate.grocerystore.develop.activities.models.FilterModel;
import com.emate.grocerystore.develop.activities.views.RecyclerItemClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RefinedByFragment extends Fragment {


    FilterListener refineBy;
    Button btn_filter;
    View view1;
    List<Brand> brandList = new ArrayList<>();
    ExpandableRelativeLayout brand_expandble_view, price_expandble_view, discount_expandble_view;
    RecyclerView brand_listView, price_listView, discount_listView;
    RelativeLayout select_brand, select_price, select_discnt;
    List<String> brand_id = new ArrayList<>();
    List<String> disnt_id = new ArrayList<>();
    List<String> price_id = new ArrayList<>();

    List<FilterModel> filterModelDiscountList1 = new ArrayList<>();
    List<String> prcList;//= Arrays.asList(getResources().getStringArray(R.array.price_array));
    // String[] arr = {"Upto 5%", "5% - 10%", "10% - 15%", "15% - 20% ", "20% - 25%", "More than 25%"};
    List<FilterModel> filterModelPriceList2 = new ArrayList<>();
    List<FilterModel> filterModelBrandList = new ArrayList<>();
    List<String> disList;//= Arrays.asList(getResources().getStringArray(R.array.discount_array));

    String prcArr = "null";
    String disArr = "null";

    String bndWhQuery = "null";
    String prcWhQuery = "null";
    String discWhQuery = "null";
    TextView brnd_cnt, prc_cnt, dis_cnt;

    public RefinedByFragment() {
        // Required empty public constructor
    }


    public void setBrandList(List<Brand> brandList1) {
        this.brandList = brandList1;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        try {
            refineBy = (FilterListener) context;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view1 = inflater.inflate(R.layout.fragment_refined_by, container, false);
        initView(view1);
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
                discWhQuery = discWhere();
                prcWhQuery = priceWhere();
                bndWhQuery = brndWhere();


                passRefinedata(bndWhQuery, prcWhQuery, discWhQuery);
                ((ProductBySuperCategory) getActivity()).onDetachedFragment();

            }
        });


        return view1;
    }

    public void brand_count() {
        try {
            if (ProductBySuperCategory.brand_id.size() > 0) {
                brnd_cnt.setText(ProductBySuperCategory.brand_id.size() + "");
                brnd_cnt.setVisibility(View.VISIBLE);
            } else {
                brnd_cnt.setVisibility(View.GONE);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void priceCount() {
        try {
            if (ProductBySuperCategory.prcList.size() > 0) {
                prc_cnt.setText(ProductBySuperCategory.prcList.size() + "");
                prc_cnt.setVisibility(View.VISIBLE);
            } else {
                prc_cnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void disCount() {

        try {
            if (ProductBySuperCategory.disList.size() > 0) {
                dis_cnt.setText(ProductBySuperCategory.disList.size() + "");
                dis_cnt.setVisibility(View.VISIBLE);
            } else {
                dis_cnt.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void initView(View vw1) {


        brand_listView = (RecyclerView) vw1.findViewById(R.id.brand_listView);
        price_listView = (RecyclerView) vw1.findViewById(R.id.price_listView);
        discount_listView = (RecyclerView) vw1.findViewById(R.id.discount_listView);
        btn_filter = (Button) vw1.findViewById(R.id.btn_filter);

        brnd_cnt = (TextView) vw1.findViewById(R.id.brnd_cnt);
        prc_cnt = (TextView) vw1.findViewById(R.id.prc_cnt);
        dis_cnt = (TextView) vw1.findViewById(R.id.dis_cnt);

        //  TextView select_brand,select_price,select_discnt;
        select_brand = (RelativeLayout) vw1.findViewById(R.id.select_brand);
        select_price = (RelativeLayout) vw1.findViewById(R.id.select_price);
        select_discnt = (RelativeLayout) vw1.findViewById(R.id.select_discnt);
        brand_expandble_view = (ExpandableRelativeLayout) vw1.findViewById(R.id.brand_expandble_view);
        price_expandble_view = (ExpandableRelativeLayout) vw1.findViewById(R.id.price_expandble_view);
        discount_expandble_view = (ExpandableRelativeLayout) vw1.findViewById(R.id.discount_expandble_view);
        prcList = Arrays.asList(getResources().getStringArray(R.array.price_array));
        disList = Arrays.asList(getResources().getStringArray(R.array.discount_array));
        brand_expandble_view.collapse();
        price_expandble_view.collapse();
        discount_expandble_view.collapse();
        brand_count();
        disCount();
        priceCount();


        for (int i = 0; i < brandList.size(); i++) {
            FilterModel fm = new FilterModel();
            fm.setId(brandList.get(i).getId());
            fm.setNm(brandList.get(i).getName());
            if (ProductBySuperCategory.brand_id.contains(brandList.get(i).getId())) {
                brand_id.add(brandList.get(i).getId());
                fm.setChecked(true);
            } else {
                fm.setChecked(false);
            }
            filterModelBrandList.add(fm);

        }

        FilterAdapter filterAdapter = new FilterAdapter(filterModelBrandList, getActivity());
        brand_listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        brand_listView.setAdapter(filterAdapter);


        for (int i = 0; i < prcList.size(); i++) {
            FilterModel fm = new FilterModel();
            fm.setId(i + "");
            fm.setNm(prcList.get(i));
            if (ProductBySuperCategory.prcList.contains(prcList.get(i))) {
                price_id.add(prcList.get(i));
                fm.setChecked(true);
            } else {
                fm.setChecked(false);
            }
            filterModelPriceList2.add(fm);

        }

        FilterAdapter filterAdapter1 = new FilterAdapter(filterModelPriceList2, getActivity());
        price_listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        price_listView.setAdapter(filterAdapter1);


        for (int i = 0; i < disList.size(); i++) {
            FilterModel fm = new FilterModel();
            fm.setId(i + "");
            fm.setNm(disList.get(i));
            if (ProductBySuperCategory.disList.contains(disList.get(i))) {
                disnt_id.add(disList.get(i));
                fm.setChecked(true);
            } else {
                fm.setChecked(false);
            }
            filterModelDiscountList1.add(fm);

        }

        FilterAdapter filterAdapter2 = new FilterAdapter(filterModelDiscountList1, getActivity());
        discount_listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        discount_listView.setAdapter(filterAdapter2);


        select_brand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //brand_expandble_view.toggle();
                openOrCollapseLayout(brand_expandble_view);
            }
        });

        select_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // price_expandble_view.toggle();
                openOrCollapseLayout(price_expandble_view);

            }
        });


        select_discnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // discount_expandble_view.toggle();

                openOrCollapseLayout(discount_expandble_view);

            }
        });
        discount_listView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        CheckBox fil_nmcb = (CheckBox) view.findViewById(R.id.fil_nmcb);
                        fil_nmcb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    disnt_id.add(filterModelDiscountList1.get(position).getNm());
                                    ProductBySuperCategory.disList.add(filterModelDiscountList1.get(position).getNm());

                                } else {
                                    if (disnt_id.contains(filterModelDiscountList1.get(position).getNm())) {
                                        disnt_id.remove(filterModelDiscountList1.get(position).getNm());
                                    }
                                    if (ProductBySuperCategory.disList.contains(filterModelDiscountList1.get(position).getNm())) {
                                        ProductBySuperCategory.disList.remove(filterModelDiscountList1.get(position).getNm());

                                    }
                                }
                                disCount();
                            }
                        });

                    }
                }));


        price_listView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        CheckBox fil_nmcb = (CheckBox) view.findViewById(R.id.fil_nmcb);
                        fil_nmcb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    price_id.add(filterModelPriceList2.get(position).getNm());
                                    ProductBySuperCategory.prcList.add(filterModelPriceList2.get(position).getNm());
                                } else {
                                    if (price_id.contains(filterModelPriceList2.get(position).getNm())) {
                                        price_id.remove(filterModelPriceList2.get(position).getNm());
                                    }
                                    if (ProductBySuperCategory.prcList.contains(filterModelPriceList2.get(position).getNm())) {
                                        ProductBySuperCategory.prcList.remove(filterModelPriceList2.get(position).getNm());

                                    }
                                }

                                priceCount();
                            }
                        });


                    }

                }));

        brand_listView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, final int position) {
                        CheckBox fil_nmcb = (CheckBox) view.findViewById(R.id.fil_nmcb);
                        fil_nmcb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    brand_id.add(filterModelBrandList.get(position).getId());
                                    ProductBySuperCategory.brand_id.add(filterModelBrandList.get(position).getId());

                                } else {
                                    if (brand_id.contains(filterModelBrandList.get(position).getId())) {
                                        brand_id.remove(filterModelBrandList.get(position).getId());
                                    }

                                    if (ProductBySuperCategory.brand_id.contains(filterModelBrandList.get(position).getId())) {
                                        ProductBySuperCategory.brand_id.remove(filterModelBrandList.get(position).getId());

                                    }
                                }

                                brand_count();
                            }
                        });


                        //    passRefinedata();

                    }
                }));


    }

    public void passRefinedata(String bndWhQuery, String prcWhQuery, String discWhQuery) {
        try {

            refineBy.onRefineByDataPass(bndWhQuery, prcWhQuery, discWhQuery);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String priceWhere() {
        //$this->db->where("$accommodation BETWEEN $minvalue AND $maxvalue");
        String prcWH = "";
        List<String> clauseList = new ArrayList<>();
        for (int i = 0; i < price_id.size(); i++) {

            if (prcList.get(0).equalsIgnoreCase(price_id.get(i))) {
                clauseList.add("(tpd.price <= 50)");

            } else if (prcList.get(1).equalsIgnoreCase(price_id.get(i))) {

                clauseList.add("(tpd.price  BETWEEN 51 AND 150)");

            } else if (prcList.get(2).equalsIgnoreCase(price_id.get(i))) {

                clauseList.add("(tpd.price  BETWEEN 151 AND 250)");

            } else if (prcList.get(3).equalsIgnoreCase(price_id.get(i))) {

                clauseList.add("(tpd.price  BETWEEN 251 AND 350)");

            } else if (prcList.get(4).equalsIgnoreCase(price_id.get(i))) {

                clauseList.add("(tpd.price  BETWEEN 351 AND 500)");

            } else if (prcList.get(5).equalsIgnoreCase(price_id.get(i))) {
                clauseList.add("(tpd.price >= 500)");
            }
        }


        try {
            for (int c = 0; c <= clauseList.size(); c++) {


                if (c == (clauseList.size() - 1)) {
                    prcWH = prcWH + clauseList.get(c);
                } else {
                    prcWH = prcWH + clauseList.get(c) + " OR ";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (prcWH.equalsIgnoreCase("")) {
            return "null";
        } else {
            return " ( " + prcWH + " ) ";
        }
    }


    public String discWhere() {
        //$this->db->where("$accommodation BETWEEN $minvalue AND $maxvalue");


        String disWH = "";
        List<String> clauseList = new ArrayList<>();
        for (int i = 0; i < disnt_id.size(); i++) {

            if (disList.get(0).equalsIgnoreCase(disnt_id.get(i))) {
                clauseList.add("(tpd.dis <= 5)");

            } else if (disList.get(1).equalsIgnoreCase(disnt_id.get(i))) {

                clauseList.add("(tpd.dis  BETWEEN 5 AND 10)");

            } else if (disList.get(2).equalsIgnoreCase(disnt_id.get(i))) {

                clauseList.add("(tpd.dis  BETWEEN 10 AND 15)");

            } else if (disList.get(3).equalsIgnoreCase(disnt_id.get(i))) {

                clauseList.add("(tpd.dis  BETWEEN 15 AND 20)");

            } else if (disList.get(4).equalsIgnoreCase(disnt_id.get(i))) {

                clauseList.add("(tpd.dis  BETWEEN 20 AND 30)");

            } else if (disList.get(5).equalsIgnoreCase(disnt_id.get(i))) {
                clauseList.add("(tpd.dis >= 30)");
            }
        }


        try {
            for (int c = 0; c <= clauseList.size(); c++) {
                if (c == (clauseList.size() - 1)) {
                    disWH = disWH + clauseList.get(c);
                } else {
                    disWH = disWH + clauseList.get(c) + " OR ";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (disWH.equalsIgnoreCase("")) {
            return "null";
        } else {
            return " ( " + disWH + " ) ";
        }
    }


    public String brndWhere() {
        //$this->db->where("$accommodation BETWEEN $minvalue AND $maxvalue");

        String brandIdWH = "";


        try {
            for (int c = 0; c < brand_id.size(); c++) {


                if (c == brand_id.size() - 1) {
                    brandIdWH = brandIdWH + " tp.brand_id = " + brand_id.get(c);
                } else {
                    brandIdWH = brandIdWH + " tp.brand_id =" + brand_id.get(c) + " OR ";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (brandIdWH.equalsIgnoreCase("")) {
            return "null";
        } else {
            return " ( " + brandIdWH + " ) ";
        }
    }


    public void openOrCollapseLayout(ExpandableRelativeLayout expandableRelativeLayout) {
        if (expandableRelativeLayout.isExpanded()) {
            expandableRelativeLayout.collapse();
        } else {
            expandableRelativeLayout.expand();
        }

    }

   /* public void showBrandList(View view) {
        brand_expandble_view.toggle(); // toggle expand and collapse
    }


    public void showPriceList(View view) {
        price_expandble_view.toggle(); // toggle expand and collapse
    }

    public void showDiscountList(View view) {
        discount_expandble_view.toggle(); // toggle expand and collapse
    }*/


}
