package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 1/19/2018.
 */

public class Brand implements Serializable {

    String id,name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
