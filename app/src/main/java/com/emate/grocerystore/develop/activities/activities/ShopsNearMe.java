package com.emate.grocerystore.develop.activities.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.MarkerModel;
import com.emate.grocerystore.develop.activities.receiver.GPSTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ShopsNearMe extends AppCompatActivity implements com.google.android.gms.maps.OnMapReadyCallback {
    private GoogleMap mMap;
    double mLatitude = 0;
    double mLongitude = 0;
    GPSTracker gps;
    LatLng mCenterLatLong;
    final List<MarkerModel> markerModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_near_me);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Shops Near To You");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            SupportMapFragment mapFragment = (SupportMapFragment) this.getSupportFragmentManager()
                    .findFragmentById(R.id.map2);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        gps = new GPSTracker(ShopsNearMe.this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

            // \n is for new line
        }
        updateMarkOnMap();

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                mCenterLatLong = cameraPosition.target;
                updateMovingMarker(mCenterLatLong);
            }
        });
        getLocation1();


        /*mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                double latitude = marker.getPosition().latitude;
                double longitude = marker.getPosition().longitude;
                String id = getMerchantId(latitude, longitude);
                Intent intent = new Intent(ShopsNearMe.this, HomeActivity.class);
                intent.putExtra(Config.FROM, id);
                startActivity(intent);
                AppController.retShowAlertDialod("lat : " + latitude + " and long : " + longitude, ShopsNearMe.this);
            }
        });*/

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                double latitude = marker.getPosition().latitude;
                double longitude = marker.getPosition().longitude;

                String id = getMerchantId(latitude, longitude);

                SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
                sh.edit().putString(Config.MERCHANT_ID, id + "")
                        .commit();
                Intent intent = new Intent(ShopsNearMe.this, HomeActivity.class);
                intent.putExtra(Config.FROM, id);
                startActivity(intent);

                //  AppController.retShowAlertDialod("lat : " + latitude + " and long : " + longitude, ShopsNearMe.this);
                return false;
            }
        });

    }


    public void getLocation1() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getShopNearByMe";
        JSONObject jsonObject = new JSONObject();
        try {

            SharedPreferences sh = getSharedPreferences("location_deatils", Context.MODE_PRIVATE);

            String lat = sh.getString("lat", "");
            String lng = sh.getString("lng", "");

            jsonObject.put("lat", lat);
            jsonObject.put("lng", lng);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        markerModelList.clear();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("markers");

                    try {

                        for (int i = 0; i < j_products.length(); i++) {

                            JSONObject json1 = j_products.getJSONObject(i);
                            MarkerModel mm = new MarkerModel();
                            mm.setId(json1.getString("id"));
                            mm.setDistance(json1.getString("distance"));
                            mm.setName(json1.getString("name"));
                            mm.setShop(json1.getString("shop"));
                            mm.setLatitude(json1.getDouble("lat"));
                            mm.setLongitude(json1.getDouble("lng"));


                            markerModelList.add(mm);

                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    try {
                        for (int i = 0; i < markerModelList.size(); i++) {
                            createMarker(markerModelList.get(i));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ShopsNearMe.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "markers");

    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }

    public void updateMarkOnMap() {

        LatLng loc1 = new LatLng(mLatitude, mLongitude);
        //   Marker m = mMap.addMarker(new MarkerOptions().position(loc1).title(note));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
        //  mLocationMarkerText.setText(note);
        //  m.showInfoWindow();

        Location mLocation = new Location("");
        mLocation.setLatitude(loc1.latitude);
        mLocation.setLongitude(loc1.longitude);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void updateMovingMarker(LatLng l1) {
        Location mLocation = new Location("");
        mLocation.setLatitude(l1.latitude);
        mLocation.setLongitude(l1.longitude);


    }


    protected Marker createMarker(MarkerModel mm) {

/*
        TextView text = new TextView(this);

        text.setGravity(View.TEXT_ALIGNMENT_CENTER);
        text.setGravity(Gravity.CENTER);
        text.setTypeface(text.getTypeface(), Typeface.BOLD);*/


        View view1 = LayoutInflater.from(ShopsNearMe.this).inflate(R.layout.marker_view, null);
        TextView nm = (TextView) view1.findViewById(R.id.nm);
        TextView distance = (TextView) view1.findViewById(R.id.distance);
        nm.setText(mm.getShop());
        try {
            distance.setText(mm.getDistance().substring(0, 4) + " km from you");
        } catch (Exception e) {
            distance.setText(mm.getDistance() + " km from you");
            e.printStackTrace();
        }
        IconGenerator generator = new IconGenerator(this);
        //  generator.setBackground(this.getResources().getDrawable(R.drawable.speechbubble));
        generator.setContentView(view1);
        Bitmap icon = generator.makeIcon();

        Marker m1 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(mm.getLatitude(), mm.getLongitude()))
                //  .anchor(0.5f, 0.5f)
                // .title(mm.getName())
                //.snippet(mm.getShop())
                // .visible(true)

                .icon(BitmapDescriptorFactory.fromBitmap(icon)));

        m1.showInfoWindow();
        return m1;


    }


    public String getMerchantId(double lat, double lng) {

        String id = "";
        for (int o = 0; o < markerModelList.size(); o++) {
            if ((markerModelList.get(o).getLatitude() == lat) && (markerModelList.get(o).getLongitude() == lng)) {

                id = markerModelList.get(o).getId();
            }
        }

        return id;
    }

}
