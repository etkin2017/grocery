package com.emate.grocerystore.develop.activities.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.FavLocation;
import com.emate.grocerystore.develop.activities.receiver.AppUtils;
import com.emate.grocerystore.develop.activities.receiver.FetchAddressIntentService;
import com.emate.grocerystore.develop.activities.receiver.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class SetMapLocationActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleMap.OnCameraChangeListener,
        LocationListener, com.google.android.gms.maps.OnMapReadyCallback {

    private GoogleMap mMap;
    double mLatitude = 0;
    double mLongitude = 0;
    ImageView our_gps_loc;
    private LatLng mCenterLatLong;
    GPSTracker gps;
    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    TextView place_txt;
    String lat = "", lng = "";
    ImageButton our_location, fav_location;
    SupportMapFragment mapFragment;
    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    protected Location mLastLocation;
    LocationRequest mLocationRequest;

    private FusedLocationProviderClient mFusedLocationClient;

    String chk = "";
    FavLocation fav = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_map_location);
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Set Your Location");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        chk = getIntent().getStringExtra(Config.FROM);
        try {
            fav = (FavLocation) getIntent().getSerializableExtra(Config.DATA);
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* try {
            startLocationUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        gps = new GPSTracker(SetMapLocationActivity.this);
        place_txt = (TextView) findViewById(R.id.place_txt);
        our_gps_loc = (ImageView) findViewById(R.id.our_gps_loc);
        our_location = (ImageButton) findViewById(R.id.our_location);
        fav_location = (ImageButton) findViewById(R.id.fav_location);
        try {
            mapFragment = (SupportMapFragment) this.getSupportFragmentManager()
                    .findFragmentById(R.id.map2);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        our_gps_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sh = getSharedPreferences("location_deatils", MODE_PRIVATE);
                sh.edit().putString("lat", mLatitude + "")
                        .putString("lng", mLongitude + "")
                        .commit();

                startActivity(new Intent(SetMapLocationActivity.this, ShopsNearMe.class));
            }
        });

        fav_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(place_txt.getText().toString())) {
                    AppController.retShowAlertDialod("Please Select Location", SetMapLocationActivity.this);
                } else {
                    DatabaseHandler databaseHandler = new DatabaseHandler(SetMapLocationActivity.this);
                    FavLocation favLocation = new FavLocation();
                    favLocation.setLoc(place_txt.getText().toString());
                    favLocation.setLat(mLatitude + "");
                    favLocation.setLng(mLongitude + "");
                    boolean c = databaseHandler.addLocation(favLocation);

                    if (c) {
                        AppController.retShowAlertDialod("Added As a favourite Location", SetMapLocationActivity.this);
                    } else {
                        AppController.retShowAlertDialod(" Location Already Exist", SetMapLocationActivity.this);

                    }

                }
            }
        });

        our_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                try {

                    gps = new GPSTracker(SetMapLocationActivity.this);
                    if (gps.canGetLocation()) {

                        if (mLatitude == 0.0 && mLongitude == 0.0) {
                            getLastLocation();
                        } else {


                            double latitude = gps.getLatitude();
                            double longitude = gps.getLongitude();

                            LatLng loc1 = new LatLng(latitude, longitude);
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                        }
                    } else {
                        getLastLocation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public String getLocationInfo() {
        JSONObject location;
        String location_string = null;
        //Http Request
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&sensor=true");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
        //Create a JSON from the String that was return.
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
            try {
                location = jsonObject.getJSONArray("results").getJSONObject(0);
                location_string = location.getString("formatted_address");
                Log.d("test", "formattted address:" + location_string);
            } catch (JSONException e1) {
                e1.printStackTrace();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return location_string;
    }


    protected void startLocationUpdates() {

        // Create the location request to start receiving updates
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();
        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        getLastLocation();
       /* getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                // do work here
                // onLocationChanged(locationResult.getLastLocation());
                Location loc1 = locationResult.getLastLocation();
                if (loc1 != null) {
                    mLatitude = loc1.getLatitude();
                    mLongitude = loc1.getLongitude();
                    updateMarkOnMap();
                    getFusedLocationProviderClient(SetMapLocationActivity.this).flushLocations();

                }

            }
        }, Looper.myLooper());*/

        mResultReceiver = new AddressResultReceiver(new Handler());
    }

    @Override
    public void onLocationChanged(Location location) {
       /* String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        // You can now create a LatLng Object for use with maps
        //  LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();

        LatLng loc1 = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
        mResultReceiver = new AddressResultReceiver(new Handler());*/

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }


    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (fav == null) {
            gps = new GPSTracker(SetMapLocationActivity.this);
            if (gps.canGetLocation()) {

                if (mLatitude == 0.0 && mLongitude == 0.0) {
                    getLastLocation();
                } else {

                    mLatitude = gps.getLatitude();
                    mLongitude = gps.getLongitude();
                }
                // \n is for new line
            } else {
                getLastLocation();
            }


        } else {
            mLatitude = Double.parseDouble(fav.getLat());
            mLongitude = Double.parseDouble(fav.getLng());
          //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude,mLongitude), 15));
        }

        updateMarkOnMap();

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                mCenterLatLong = cameraPosition.target;
                mLatitude = mCenterLatLong.latitude;
                mLongitude = mCenterLatLong.longitude;
                updateMovingMarker(mCenterLatLong);
            }
        });
    }

    public void updateMarkOnMap() {
        LatLng loc1 = new LatLng(mLatitude, mLongitude);
        //   Marker m = mMap.addMarker(new MarkerOptions().position(loc1).title(note));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
        //  mLocationMarkerText.setText(note);
        //  m.showInfoWindow();

        Location mLocation = new Location("");
        mLocation.setLatitude(loc1.latitude);
        mLocation.setLongitude(loc1.longitude);

        lat = loc1.latitude + "";
        lng = loc1.longitude + "";
        startIntentService(mLocation);
        mResultReceiver = new AddressResultReceiver(new Handler());


    }

    public void updateMovingMarker(LatLng l1) {
        Location mLocation = new Location("");
        mLocation.setLatitude(l1.latitude);
        mLocation.setLongitude(l1.longitude);

        lat = l1.latitude + "";
        lng = l1.longitude + "";

        startIntentService(mLocation);
        mResultReceiver = new AddressResultReceiver(new Handler());
    }

    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(SetMapLocationActivity.this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);

            String a = mAddressOutput.replace("\n", ",");
          /*  locname = a;
            Log.e("ADDRESS", a);
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);*/


            place_txt.setText(a);
            //  mLocationMarkerText.setText(mCityOutput + " " + mStateOutput);

            // mLocationMarkerText.setText(a);


            // displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));


            }


        }

    }


    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        try {
            mFusedLocationClient = getFusedLocationProviderClient(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();


                            mLatitude = mLastLocation.getLatitude();
                            mLongitude = mLastLocation.getLongitude();

                            LatLng loc1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                            mResultReceiver = new AddressResultReceiver(new Handler());
                        } else {
                            Log.w("LastLocation", "getLastLocation:exception", task.getException());
                            showSnackbar(R.string.no_location_detected, R.string.no_location_detected, null);
                        }
                    }
                });
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }


  /*  @SuppressWarnings("MissingPermission")
    private void getLastLocation() {

        FusedLocationProviderClient locationClient = getFusedLocationProviderClient(this);

        locationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            onLocationChanged(location);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });


     *//*   try {
            mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {

                        mLatitude = mLastLocation.getLatitude();
                        mLongitude = mLastLocation.getLongitude();

                        LatLng loc1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                        mResultReceiver = new AddressResultReceiver(new Handler());
                    } else {
                     //   Log.w("LastLocation", "getLastLocation:exception", location.get());
                        showSnackbar(R.string.no_location_detected, R.string.no_location_detected, null);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            showSnackbar(R.string.exceptiom, R.string.exceptiom, null);

        }*//*




                *//*.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();


                            mLatitude = mLastLocation.getLatitude();
                            mLongitude = mLastLocation.getLongitude();

                            LatLng loc1 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                            mResultReceiver = new AddressResultReceiver(new Handler());
                        } else {
                            Log.w("LastLocation", "getLastLocation:exception", task.getException());
                            showSnackbar(R.string.no_location_detected, R.string.no_location_detected, null);
                        }
                    }
                });*//*
    }

    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }*/

  /*  public JSONObject getLocationInfo() {
        //Http Request
        HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=true");
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }
        //Create a JSON from the String that was return.
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(stringBuilder.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }*/


}
