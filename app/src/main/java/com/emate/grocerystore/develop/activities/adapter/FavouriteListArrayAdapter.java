package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.FavLocation;

import java.util.List;

public class FavouriteListArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<FavLocation> items ;//
    private final int mResource;

    public FavouriteListArrayAdapter(@NonNull Context context, @LayoutRes int resource,
                                     @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txt_nm = (TextView) view.findViewById(R.id.txt_nm);
        TextView txt_id = (TextView) view.findViewById(R.id.txt_id);


        FavLocation favLocation = items.get(position);

        txt_nm.setText(favLocation.getLoc());
        txt_id.setText(favLocation.getLoc_id()+"");

        return view;
    }
}