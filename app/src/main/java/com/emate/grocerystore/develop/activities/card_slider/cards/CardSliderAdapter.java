package com.emate.grocerystore.develop.activities.card_slider.cards;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.emate.grocerystore.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Ashvini on 12/7/2017.
 */

public class CardSliderAdapter extends RecyclerView.Adapter<CardSliderAdapter.CardSliderHolder> {

    String[] con;
    Context ctx;
    View.OnClickListener listener;
    View view;

    public CardSliderAdapter(String[] con, Context ctx, View.OnClickListener listener) {
        this.con = con;
        this.ctx = ctx;
        this.listener = listener;
    }


    @Override
    public CardSliderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.layout_slider_card, parent,false);

        if (listener != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }

        return new CardSliderHolder(view);
    }

    @Override
    public void onBindViewHolder(CardSliderHolder holder, int position) {
        Picasso.with(ctx).load(Uri.parse(con[position])).placeholder(R.drawable.grocery_icon).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return con.length;
    }

    public class CardSliderHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public CardSliderHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
