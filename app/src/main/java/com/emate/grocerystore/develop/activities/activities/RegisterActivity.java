package com.emate.grocerystore.develop.activities.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;

import org.json.JSONArray;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    EditText uemail, uname, umobi, uship_address, upsd;
    Button btn_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(uname.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Name", RegisterActivity.this);
                } else if (TextUtils.isEmpty(uemail.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Email Id", RegisterActivity.this);
                } else if (TextUtils.isEmpty(umobi.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Mobile Number", RegisterActivity.this);
                } else if (umobi.getText().toString().length() < 10) {
                    AppController.retShowAlertDialod("Please Enter Valid Mobile Number", RegisterActivity.this);
                } else if (TextUtils.isEmpty(uship_address.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Address", RegisterActivity.this);
                } else if (TextUtils.isEmpty(upsd.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Password", RegisterActivity.this);
                } else {
                    registerUser();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void init() {

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Sign Up");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

        uemail = (EditText) findViewById(R.id.uemail);
        uname = (EditText) findViewById(R.id.uname);
        umobi = (EditText) findViewById(R.id.umobi);
        uship_address = (EditText) findViewById(R.id.uship_address);
        upsd = (EditText) findViewById(R.id.upsd);
        btn_signup = (Button) findViewById(R.id.btn_signup);

    }

    public void registerUser() {

        String unique_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);


        final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "register";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            //user_name,email,mb_no,shipping_add,total_order,user_status
            jsonObject.put("user_name", uname.getText().toString());
            jsonObject.put("email", uemail.getText().toString());
            jsonObject.put("mb_no", umobi.getText().toString());
            jsonObject.put("shipping_add", uship_address.getText().toString());
            jsonObject.put("total_order", "0");
            jsonObject.put("password", upsd.getText().toString());

            jsonObject.put("unique_id", unique_id);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = response.getJSONArray("success");

                    boolean b = jsonArray.getJSONObject(0).getBoolean("success");
                    if (b == true) {

                        Dialog dialog = new Dialog(RegisterActivity.this);
                        dialog.setContentView(R.layout.verify_otp);
                        dialog.setCancelable(false);
                        final EditText edt_otp = (EditText) dialog.findViewById(R.id.edt_otp);
                        Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);


                        btn_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (TextUtils.isEmpty(edt_otp.getText().toString())) {

                                } else {

                                }

                            }
                        });

                        dialog.show();

                       /* finish();
                        String intent_data = getIntent().getStringExtra("groceryData");
                        String amount = getIntent().getStringExtra("amount");
                        if (intent_data != null) {
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            intent.putExtra("groceryData", intent_data);
                            intent.putExtra("amount", amount);
                            startActivity(intent);
                        } else {
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                        }*/
                    } else {
                        AppController.retShowAlertDialod("Failed To Register , User Already Registered With Email-Id Or Mobile Number", RegisterActivity.this);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(RegisterActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "register_");

    }


}
