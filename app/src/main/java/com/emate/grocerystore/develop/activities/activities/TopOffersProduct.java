package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.ProductAdapter;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TopOffersProduct extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {

    ProductModel pm;
    TextView prod_title, prod_desc, prod_price, or_price;
    Button add_prod;

    SliderLayout prod_slider;
    PagerIndicator custom_indicator;
    LinearLayout related_product_container;
    RecyclerView related_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_offers_product);


        init();


        try {
            getProd(getIntent().getStringExtra("subcat_id"), "getProd");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void init() {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
          /*  getSupportActionBar().setTitle(pm.getTitle());*/

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

        prod_desc = (TextView) findViewById(R.id.prod_desc);
        prod_price = (TextView) findViewById(R.id.prod_price);
        prod_title = (TextView) findViewById(R.id.prod_title);
        or_price = (TextView) findViewById(R.id.or_price);
        add_prod = (Button) findViewById(R.id.add_prod);

        prod_slider = (SliderLayout) findViewById(R.id.prod_slider);
        custom_indicator = (PagerIndicator) findViewById(R.id.custom_indicator);
        related_product_container = (LinearLayout) findViewById(R.id.related_product_container);
        related_product = (RecyclerView) findViewById(R.id.related_product);
    }

    public void initData() {

        or_price.setText(getResources().getString(R.string.indian_rupee) + pm.getUnitModel().getMrp());
        Config.strikeThroughText(or_price);
       // float disPrice = Config.getPrice(Float.parseFloat(pm.getP()), Float.parseFloat(pm.getPrice()));
        prod_price.setText(getResources().getString(R.string.indian_rupee) +pm.getUnitModel().getPrice() + "");

        prod_title.setText(pm.getTitle());

        try {
            prod_desc.setText(Html.fromHtml(pm.getContent()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void initSlider() {
        HashMap<String, String> url_maps = new HashMap<String, String>();
        if (!pm.getImage1().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle(), pm.getImage1());

        }
        if (!pm.getImage2().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + " ", pm.getImage2());

        }
        if (!pm.getImage3().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "  ", pm.getImage3());

        }
        if (!pm.getImage4().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "   ", pm.getImage4());

        }
        if (!pm.getImage5().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "     ", pm.getImage5());

        }


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            prod_slider.addSlider(textSliderView);
        }
        prod_slider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        prod_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        prod_slider.setCustomAnimation(new DescriptionAnimation());
        prod_slider.setDuration(4000);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getProd(String id1, String url1) {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + url1;// "getProductBySuperCategory"; //getProd
        SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
        String merchId = sh.getString(Config.MERCHANT_ID, "");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id1);
            jsonObject.put("first", 0);
            jsonObject.put("last", 10);
            jsonObject.put("brnd", "null");
            jsonObject.put("price", "null");
            jsonObject.put("discount", "null");
            jsonObject.put("sortBy", "null");
            jsonObject.put("merchant_id", merchId);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<ProductModel> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");
                    if (j_products.length() == 0) {
                        related_product_container.setVisibility(View.GONE);

                    } else {
                        related_product_container.setVisibility(View.VISIBLE);

                        try {

                            for (int i = 0; i < j_products.length(); i++) {

                                ProductModel pm = new ProductModel();
                                String id = j_products.getJSONObject(i).getString("id");
                                String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                                String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                                String brand_id = j_products.getJSONObject(i).getString("brand_id");
                                String title = j_products.getJSONObject(i).getString("title");
                                String available = j_products.getJSONObject(i).getString("available");
                                String seller_name = j_products.getJSONObject(i).getString("seller_name");

                                String status = j_products.getJSONObject(i).getString("status");
                                String hstatus = j_products.getJSONObject(i).getString("hstatus");
                                String image1 = j_products.getJSONObject(i).getString("image1");
                                String image2 = j_products.getJSONObject(i).getString("image2");
                                String image3 = j_products.getJSONObject(i).getString("image3");
                                String image4 = j_products.getJSONObject(i).getString("image4");
                                String image5 = j_products.getJSONObject(i).getString("image5");
                                String seller = j_products.getJSONObject(i).getString("seller");
                                String shipping = j_products.getJSONObject(i).getString("shipping");
                                String cgst = j_products.getJSONObject(i).getString("cgst");
                                String sgst = j_products.getJSONObject(i).getString("sgst");
                                String seller_id = j_products.getJSONObject(i).getString("seller_id");



                                pm.setId(id);
                                pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                                pm.setSubcat_id(subcat_id);
                                pm.setDet_cat_id(det_cat_id);
                                pm.setBrand_id(brand_id);
                                pm.setTitle(title);
                                pm.setAvailable(available);
                                pm.setSeller_name(seller_name);
                                pm.setStatus(status);
                                pm.setHstatus(hstatus);
                                pm.setImage1(image1);
                                pm.setImage2(image2);
                                pm.setImage3(image3);
                                pm.setImage4(image4);
                                pm.setImage5(image5);
                                pm.setSeller(seller);
                                pm.setShipping(shipping);
                                pm.setCgst(cgst);
                                pm.setSgst(sgst);
                                pm.setSeller_id(seller_id);
                                UnitModel um = new UnitModel();
                                String unit_id = j_products.getJSONObject(i).getString("unit_id");
                                String quantity = j_products.getJSONObject(i).getString("quantity");
                                String dprice = j_products.getJSONObject(i).getString("price");
                                String mrp = j_products.getJSONObject(i).getString("mrp");
                                String unit = j_products.getJSONObject(i).getString("unit");
                                String discount = j_products.getJSONObject(i).getString("dis");

                                um.setId(unit_id);
                                um.setQuantity(quantity);
                                um.setDis(discount);
                                um.setMrp(mrp);
                                um.setPrice(dprice);
                                um.setUnit(unit);

                                pm.setUnitModel(um);


                                productModelList.add(pm);

                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        pm = productModelList.get(0);
                        getSupportActionBar().setTitle(pm.getTitle());
                        initSlider();
                        initData();
                        ProductAdapter adapter = new ProductAdapter(productModelList, TopOffersProduct.this);
                        related_product.setLayoutManager(new GridLayoutManager(TopOffersProduct.this, 2));
                        related_product.setAdapter(adapter);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(TopOffersProduct.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");

    }
}
