package com.emate.grocerystore.develop.activities.interfaces;

/**
 * Created by Ashvini on 1/19/2018.
 */

public interface FilterListener {


   /* public interface RefineBy {
        public void onRefineByDataPass(String brandid, String price, String discount);

    }

    public interface SortBy {
        public void onSortByDataPass(String sortby);
    }
*/

    public void onRefineByDataPass(String brandid, String price, String discount);
    public void onSortByDataPass(String sortby);

   // public void onFilterData(String brandid, String price, String discount, String srtby);



}
