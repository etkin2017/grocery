package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ProductByBrandActivity;
import com.emate.grocerystore.develop.activities.models.Brand;
import com.emate.grocerystore.develop.activities.views.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashvini on 1/19/2018.
 */

public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.BrandHolder> {

    List<Brand> brandList1;
    Context ctx;


    public BrandListAdapter(List<Brand> catList, Context ctx) {
        this.brandList1 = catList;
        this.ctx = ctx;

    }

    @Override
    public BrandHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.brand_row, parent, false);
        BrandHolder holder = new BrandHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(BrandHolder holder, final int position) {

        holder.brand_name.setText(brandList1.get(position).getName());
        try {
            //holder.brand_icon.setText(AppController.getInitalsStr(brandList1.get(position).getName()));

            Picasso.with(ctx).load(retBrandIcon(position)).into(holder.brand_icon);
            holder.brand_icon.setScaleType(CircularImageView.ScaleType.FIT_XY);

            // holder.brand_icon.setBackgroundColor(AppController.getRandomColorCode());

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        holder.cat_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ctx, ProductByBrandActivity.class);
                intent.putExtra("brand_id", brandList1.get(position).getId());
                intent.putExtra("brand_nm", brandList1.get(position).getName());
                ctx.startActivity(intent);
                /*ArrayList<SubBrand> subBrandArrayList = new ArrayList<SubBrand>();
                for (int i = 0; i < subCatList.size(); i++) {
                    if (subCatList.get(i).getId().equals(brandList1.get(position).getId())) {
                        subBrandArrayList.add(subCatList.get(i));
                    }

                }


                Intent int1 = new Intent(ctx, ProductBySuperBrand.class);
                int1.putExtra("sub_cat", subBrandArrayList);
                int1.putExtra("cat_id", brandList1.get(position).getId());
                int1.putExtra("cat_nm", brandList1.get(position).getBrand_type());
                int1.putExtra("sub_Brand", "0");
                ctx.startActivity(int1);*/
            }
        });


    }

    public int retBrandIcon(int p) {
        int a = R.drawable.backet;
        switch (p) {
            case 0:
                a = R.drawable.parle;
                break;
            case 1:
                a = R.drawable.brit_logo;
                break;

            case 2:
                a = R.drawable.nestle;
                break;

            case 3:
                a = R.drawable.png;
                break;
            case 4:
                a = R.drawable.utc;
                break;

            case 5:
                a = R.drawable.hul;
                break;

            case 6:
                a = R.drawable.emami;
                break;
            case 7:
                a = R.drawable.shree_mangal;
                break;

            default:
                break;


        }
        return a;

    }


    @Override
    public int getItemCount() {
        return brandList1.size();
    }

    public class BrandHolder extends RecyclerView.ViewHolder {


        TextView brand_name;
        CircularImageView brand_icon;
        LinearLayout cat_container;

        public BrandHolder(View itemView) {
            super(itemView);
            brand_icon = (CircularImageView) itemView.findViewById(R.id.brand_icon);
            brand_name = (TextView) itemView.findViewById(R.id.brand_name);
            cat_container = (LinearLayout) itemView.findViewById(R.id.cat_container);

        }
    }
}
