package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.CustomPriceArrayAdapter;
import com.emate.grocerystore.develop.activities.adapter.ProductAdapter;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.Cart;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.UnitModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProdDetails extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {

    ProductModel pm;
    TextView prod_title, prod_desc, prod_price, or_price, dis_per;
    TextView add_prod, minus_prod, plus_prod;
    Spinner price_spinner;

    SliderLayout prod_slider;
    PagerIndicator custom_indicator;
    LinearLayout related_product_container;
    RecyclerView related_product;
    String prod_id = "";
    List<UnitModel> unitModelList = new ArrayList<>();
    DatabaseHandler databaseHandler;
    Cart c1 = null;
    String merchId="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prod_details);
        SharedPreferences sh = getSharedPreferences(Config.MERCHANT_ID, MODE_PRIVATE);
         merchId = sh.getString(Config.MERCHANT_ID, "");
        try {
            pm = (ProductModel) getIntent().getSerializableExtra("product_details");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        init();
        initSlider();
        initData(pm.getUnitModel());

        try {
            prod_id = pm.getId();
            getPriceList();
            // getProd(pm.getSubcat_id(), "getRelatedProd");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void init() {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(pm.getTitle());

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        prod_desc = (TextView) findViewById(R.id.prod_desc);
        prod_price = (TextView) findViewById(R.id.prod_price);
        prod_title = (TextView) findViewById(R.id.prod_title);
        or_price = (TextView) findViewById(R.id.or_price);
        add_prod = (TextView) findViewById(R.id.add_prod);
        minus_prod = (TextView) findViewById(R.id.minus_prod);
        plus_prod = (TextView) findViewById(R.id.plus_prod);
        price_spinner = (Spinner) findViewById(R.id.price_spinner);

        prod_slider = (SliderLayout) findViewById(R.id.prod_slider);
        custom_indicator = (PagerIndicator) findViewById(R.id.custom_indicator);
        related_product_container = (LinearLayout) findViewById(R.id.related_product_container);
        related_product = (RecyclerView) findViewById(R.id.related_product);
        dis_per = (TextView) findViewById(R.id.dis_per);

        try {
            databaseHandler = new DatabaseHandler(ProdDetails.this);
            changeCartData(Integer.parseInt(pm.getUnitModel().getId()));
        } catch (Exception e) {
            e.printStackTrace();
        }


        minus_prod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (c1.getQty() == 1) {
                    databaseHandler.deleteCart(c1);
                    changeCartData(c1.getUnit_id());
                } else {
                    updateQuantity(false);
                }

            }
        });


        plus_prod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateQuantity(true);
            }
        });


        price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //    String item = ((TextView)view.findViewById(R.id.offer_type_txt)).getText().toString();
                UnitModel um = unitModelList.get(i);
                initData(um);
                changeCartData(Integer.parseInt(um.getId()));
                if (c1 != null) {
                    try {
                        Cart cc = new Cart();
                        cc.setCartid(c1.getCartid());
                        cc.setQty(c1.getQty());
                        cc.setUnit_id(Integer.parseInt(um.getId()));
                        cc.setProduct_id(pm.getId());
                        cc.setOndate(c1.getOndate());
                        databaseHandler.updateCart(cc);
                        changeCartData(Integer.parseInt(um.getId()));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        add_prod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (c1 != null) {
                    add_prod.setEnabled(false);
                } else {
                    add_prod.setEnabled(true);
                    Cart cart = new Cart();
                    cart.setProduct_id(pm.getId() + "");
                    cart.setUnit_id(Integer.parseInt(unitModelList.get(price_spinner.getSelectedItemPosition()).getId()));
                    cart.setOndate(System.currentTimeMillis() + "");
                    cart.setQty(1);
                    databaseHandler.addCart(cart);
                    changeCartData(Integer.parseInt(unitModelList.get(price_spinner.getSelectedItemPosition()).getId()));
                }
            }
        });
    }

    public void updateQuantity(boolean a) {
        Cart cc = new Cart();
        cc.setCartid(c1.getCartid());
        if (a) {
            cc.setQty((c1.getQty() + 1));
        } else {
            cc.setQty((c1.getQty() - 1));

        }
        cc.setUnit_id(c1.getUnit_id());
        cc.setProduct_id(pm.getId());
        cc.setOndate(c1.getOndate());
        databaseHandler.updateCart(cc);
        changeCartData(c1.getUnit_id());
    }

    public void changeCartData(int unit_id) {
        try {
            c1 = databaseHandler.getCart(Integer.parseInt(pm.getId()),unit_id);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (c1 != null) {
            int qty_value = c1.getQty();
            add_prod.setText(qty_value + "");

            minus_prod.setVisibility(View.VISIBLE);
            plus_prod.setVisibility(View.VISIBLE);

        } else {
            add_prod.setText("Add");
            minus_prod.setVisibility(View.GONE);
            plus_prod.setVisibility(View.GONE);
        }
    }

    public void getPriceList() {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "getProdPriceUnits";// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("pro_id", pm.getId());

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<String> priceList = new ArrayList<>();


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");


                    try {

                        for (int i = 0; i < j_products.length(); i++) {

                            UnitModel um = new UnitModel();
                            String unit_id = j_products.getJSONObject(i).getString("id");
                            String quantity = j_products.getJSONObject(i).getString("quantity");
                            String dprice = j_products.getJSONObject(i).getString("price");
                            String mrp = j_products.getJSONObject(i).getString("mrp");
                            String unit = j_products.getJSONObject(i).getString("unit");
                            String discount = j_products.getJSONObject(i).getString("dis");

                            um.setId(unit_id);
                            um.setQuantity(quantity);
                            um.setDis(discount);
                            um.setMrp(mrp);
                            um.setPrice(dprice);
                            um.setUnit(unit);
                            unitModelList.add(um);

                            priceList.add(quantity + " " + unit + "  " + dprice);


                        }


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    CustomPriceArrayAdapter adapter = new CustomPriceArrayAdapter(ProdDetails.this,
                            R.layout.price_spin_row, unitModelList);
                    price_spinner.setAdapter(adapter);
                    int pos = getData(unitModelList);
                    price_spinner.setSelection(pos);

                    //c1.getUnit_id();

                    getProd(pm.getSubcat_id(), "getRelatedProd");


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ProdDetails.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");


    }

    public int getData(List<UnitModel> unitModelList) {
        int pos = 0;
        try {
            for (int i = 0; i < unitModelList.size(); ) {
                if (unitModelList.get(i).getId().equalsIgnoreCase(c1.getUnit_id() + "")) {
                    pos = i;
                    i = unitModelList.size();
                } else {
                    i++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pos;
    }


    public void initData(UnitModel um) {

        or_price.setText(getResources().getString(R.string.indian_rupee) + um.getMrp());
        Config.strikeThroughText(or_price);
        // float disPrice = Config.getPrice(Float.parseFloat(pm.getUnitModel().getDprice()), Float.parseFloat(pm.getPrice()));
        prod_price.setText(getResources().getString(R.string.indian_rupee) + um.getPrice() + "");
        prod_title.setText(pm.getTitle());

        try {
            prod_desc.setText(Html.fromHtml(pm.getContent()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            dis_per.setText(um.getDis() + "%");
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }

    public void initSlider() {
        HashMap<String, String> url_maps = new HashMap<String, String>();
        if (!pm.getImage1().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle(), pm.getImage1());

        }
        if (!pm.getImage2().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + " ", pm.getImage2());

        }
        if (!pm.getImage3().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "  ", pm.getImage3());

        }
        if (!pm.getImage4().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "   ", pm.getImage4());

        }
        if (!pm.getImage5().equalsIgnoreCase("0")) {
            url_maps.put(pm.getTitle() + "     ", pm.getImage5());

        }


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            prod_slider.addSlider(textSliderView);
        }
        prod_slider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        prod_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        prod_slider.setCustomAnimation(new DescriptionAnimation());
        prod_slider.setDuration(4000);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getProd(String id1, String url1) {


        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + url1;// "getProductBySuperCategory"; //getProd
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id1);
            jsonObject.put("first", 0);
            jsonObject.put("last", 10);
            jsonObject.put("prod_id", prod_id);
            jsonObject.put("merchant_id", merchId);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        final List<ProductModel> productModelList = new ArrayList<>();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray j_products = response.getJSONArray("success");
                    if (j_products.length() == 0) {
                        related_product_container.setVisibility(View.GONE);

                    } else {
                        related_product_container.setVisibility(View.VISIBLE);

                        try {

                            for (int i = 0; i < j_products.length(); i++) {

                                ProductModel pm = new ProductModel();
                                String id = j_products.getJSONObject(i).getString("id");
                                String subcat_id = j_products.getJSONObject(i).getString("subcat_id");
                                String det_cat_id = j_products.getJSONObject(i).getString("det_cat_id");
                                String brand_id = j_products.getJSONObject(i).getString("brand_id");
                                String title = j_products.getJSONObject(i).getString("title");
                                String available = j_products.getJSONObject(i).getString("available");
                                String seller_name = j_products.getJSONObject(i).getString("seller_name");

                                String status = j_products.getJSONObject(i).getString("status");
                                String hstatus = j_products.getJSONObject(i).getString("hstatus");
                                String image1 = j_products.getJSONObject(i).getString("image1");
                                String image2 = j_products.getJSONObject(i).getString("image2");
                                String image3 = j_products.getJSONObject(i).getString("image3");
                                String image4 = j_products.getJSONObject(i).getString("image4");
                                String image5 = j_products.getJSONObject(i).getString("image5");
                                String seller = j_products.getJSONObject(i).getString("seller");
                                String shipping = j_products.getJSONObject(i).getString("shipping");
                                String cgst = j_products.getJSONObject(i).getString("cgst");
                                String sgst = j_products.getJSONObject(i).getString("sgst");
                                String seller_id = j_products.getJSONObject(i).getString("seller_id");

                                String content = j_products.getJSONObject(i).getString("content");


                                pm.setId(id);
                                pm.setCat_id(j_products.getJSONObject(i).getString("cat_id"));

                                pm.setSubcat_id(subcat_id);
                                pm.setDet_cat_id(det_cat_id);
                                pm.setBrand_id(brand_id);
                                pm.setTitle(title);
                                pm.setAvailable(available);
                                pm.setSeller_name(seller_name);
                                pm.setStatus(status);
                                pm.setHstatus(hstatus);
                                pm.setImage1(image1);
                                pm.setImage2(image2);
                                pm.setImage3(image3);
                                pm.setImage4(image4);
                                pm.setImage5(image5);
                                pm.setSeller(seller);
                                pm.setShipping(shipping);
                                pm.setCgst(cgst);
                                pm.setSgst(sgst);
                                pm.setSeller_id(seller_id);
                                UnitModel um = new UnitModel();
                                String unit_id = j_products.getJSONObject(i).getString("unit_id");
                                String quantity = j_products.getJSONObject(i).getString("quantity");
                                String dprice = j_products.getJSONObject(i).getString("price");
                                String mrp = j_products.getJSONObject(i).getString("mrp");
                                String unit = j_products.getJSONObject(i).getString("unit");
                                String discount = j_products.getJSONObject(i).getString("dis");

                                um.setId(unit_id);
                                um.setQuantity(quantity);
                                um.setDis(discount);
                                um.setMrp(mrp);
                                um.setPrice(dprice);
                                um.setUnit(unit);

                                pm.setUnitModel(um);
                                pm.setContent(content);
                                productModelList.add(pm);

                            }


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        ProductAdapter adapter = new ProductAdapter(productModelList, ProdDetails.this);
                        related_product.setLayoutManager(new GridLayoutManager(ProdDetails.this, 2));
                        related_product.setAdapter(adapter);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(ProdDetails.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().addToRequestQueue(req, "get_product");

    }


}
