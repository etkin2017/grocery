package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.FilterModel;

import java.util.List;

/**
 * Created by Ashvini on 1/19/2018.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.FilterHolder> {

    Context ctx;
    List<FilterModel> nms;

    public FilterAdapter(List<FilterModel> nms, Context ctx) {

        this.nms = nms;
        this.ctx = ctx;
    }

    @Override
    public FilterHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.filter_brand_row, parent, false);
        FilterHolder holder = new FilterHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(FilterHolder holder, int position) {


        String nm = nms.get(position).getNm().trim();
        holder.fil_nm.setText(String.valueOf(nm));
        holder.fil_nmcb.setChecked(nms.get(position).isChecked());
        //  holder.fil_nm.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

    }

    @Override
    public int getItemCount() {
        return nms.size();
    }

    public class FilterHolder extends RecyclerView.ViewHolder {

        TextView fil_nm;
        CheckBox fil_nmcb;

        public FilterHolder(View itemView) {
            super(itemView);
            fil_nmcb = (CheckBox) itemView.findViewById(R.id.fil_nmcb);
            fil_nm = (TextView) itemView.findViewById(R.id.fil_nm);
        }
    }
}
