package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ProdDetails;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashvini on 12/18/2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {
    List<ProductModel> productModelArrayList;
    Context ctx;

    public  ProductAdapter(List<ProductModel> catList, Context ctx) {
        this.productModelArrayList = catList;
        this.ctx = ctx;
    }


    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(ctx).inflate(R.layout.product_row, parent, false);
        ProductHolder holder = new ProductHolder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {
        holder.ttl.setText(productModelArrayList.get(position).getTitle());
        Picasso.with(ctx).load(Uri.parse(productModelArrayList.get(position).getImage1())).placeholder(R.drawable.grocery_icon).into(holder.img);
        holder.price.setText(ctx.getResources().getString(R.string.indian_rupee) + productModelArrayList.get(position).getUnitModel().getMrp());
        Config.strikeThroughText(holder.price);
       // float disPrice = Config.getPrice(Float.parseFloat(productModelArrayList.get(position).getDprice()), Float.parseFloat(productModelArrayList.get(position).getPrice()));
        holder.dis_price.setText(ctx.getResources().getString(R.string.indian_rupee) + productModelArrayList.get(position).getUnitModel().getPrice() + "");
        holder.dis_per.setText(productModelArrayList.get(position).getUnitModel().getDis() + "%");

        holder.product_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent product_Details = new Intent(ctx, ProdDetails.class);
                product_Details.putExtra("product_details", productModelArrayList.get(position));
                ctx.startActivity(product_Details);
            }
        });

    }

    @Override
    public int getItemCount() {
        return productModelArrayList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        TextView ttl, dis_price, price, dis_per;
        ImageView img;
        LinearLayout product_container;


        public ProductHolder(View itemView) {
            super(itemView);
            product_container=(LinearLayout) itemView.findViewById(R.id.product_container);
            ttl = (TextView) itemView.findViewById(R.id.ttl);
            dis_price = (TextView) itemView.findViewById(R.id.dis_price);
            price = (TextView) itemView.findViewById(R.id.price);
            dis_per = (TextView) itemView.findViewById(R.id.dis_per);
            img = (ImageView) itemView.findViewById(R.id.img);

        }
    }
}

