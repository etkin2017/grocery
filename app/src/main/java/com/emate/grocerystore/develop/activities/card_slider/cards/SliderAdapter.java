package com.emate.grocerystore.develop.activities.card_slider.cards;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.emate.grocerystore.R;
import com.squareup.picasso.Picasso;


public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderAdpHolder> {
    String[] cont;
    View.OnClickListener listener;
    Context ctx;
    View view;

    public SliderAdapter(String[] cont, View.OnClickListener listener, Context ctx) {
        try {
            this.cont = cont;
            this.listener = listener;
            this.ctx = ctx;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public SliderAdpHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(ctx).inflate(R.layout.layout_slider_card, parent, false);
        SliderAdpHolder holder = new SliderAdpHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(SliderAdpHolder holder, int position) {
        Picasso.with(ctx).load(Uri.parse(cont[position])).placeholder(R.drawable.grocery_icon).into(holder.img);

        if (listener != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return cont.length;
    }

    public class SliderAdpHolder extends RecyclerView.ViewHolder {
        ImageView img;

        public SliderAdpHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}




/*extends RecyclerView.Adapter<SliderAdapter.CardHolder> {


    private final String[] content;
    private final View.OnClickListener listener;
    Context context;

    public SliderAdapter(String[] content, View.OnClickListener listener, Context context) {
        this.content = content;

        this.listener = listener;
        this.context = context;
    }

    @Override
    public CardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View view =null;
        try {
            view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.layout_slider_card, parent, false);

            if (listener != null) {
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onClick(view);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // return new SliderCard(view);
        CardHolder holder = new CardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CardHolder holder, int position) {
        //  holder.setContent(content[position % content.length]);
        Picasso.with(context).load(content[position]).placeholder(R.drawable.grocery_icon).into(holder.image);

    }


    @Override
    public int getItemCount() {
        return content.length;
    }


    public class CardHolder extends RecyclerView.ViewHolder {
        ImageView image;

        public CardHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

}*/
