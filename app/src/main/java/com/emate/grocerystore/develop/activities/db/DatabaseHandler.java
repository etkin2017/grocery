package com.emate.grocerystore.develop.activities.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.emate.grocerystore.develop.activities.models.Cart;
import com.emate.grocerystore.develop.activities.models.FavLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashvini on 1/25/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "cartManager";

    // CART table name
    private static final String TABLE_CART = "cart";
    private static final String TABLE_FAVOURITE_LOCATION = "favorite_location";

    // CART Table Columns names
    //cartid,user_id,product_id,ondate,chng_addrs,deliver_to,delive_mob_no,prod_qty
    private static final String KEY_cartid = "cartid";
    private static final String KEY_product_id = "product_id";
    private static final String KEY_ondate = "ondate";
    private static final String KEY_prod_qty = "qty";
    private static final String KEY_prod_unit = "unit";

    private static final String KEY_loc_id = "locid";
    private static final String KEY_loc = "loc";
    private static final String KEY_lat = "lat";
    private static final String KEY_long = "lng";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CART_TABLE = "CREATE TABLE " + TABLE_CART + "("
                + KEY_cartid + " INTEGER PRIMARY KEY," + KEY_product_id + " INTEGER ,"
                + KEY_ondate + " TEXT ," + KEY_prod_qty + " INTEGER ," + KEY_prod_unit + " INTEGER " + " )";
        db.execSQL(CREATE_CART_TABLE);


        String CREATE_FAVOURITE_LOCATION = "CREATE TABLE " + TABLE_FAVOURITE_LOCATION + "("
                + KEY_loc_id + " INTEGER PRIMARY KEY,"
                + KEY_loc + " TEXT ," + KEY_lat + " TEXT ," + KEY_long + " TEXT " + " )";
        db.execSQL(CREATE_FAVOURITE_LOCATION);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FAVOURITE_LOCATION);

        // Create tables again
        onCreate(db);
    }

    public void deleteCart() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_CART);
    }

    // Adding new contact
    public void addCart(Cart cart) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_product_id, cart.getProduct_id()); // Cart Product Id
        values.put(KEY_ondate, cart.getOndate()); // Cart Date
        values.put(KEY_prod_qty, cart.getQty()); //  Quantity
        values.put(KEY_prod_unit, cart.getUnit_id()); // Product Unit
        // Inserting Row
        db.insert(TABLE_CART, null, values);
        db.close(); // Closing database connection

    }

    public boolean addLocation(FavLocation favLoc) {
        boolean chkInsert = false;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_loc, favLoc.getLoc()); // Cart Product Id
        values.put(KEY_lat, favLoc.getLat()); // Cart Date
        values.put(KEY_long, favLoc.getLng()); //  Quantity


        if (!getLocExist(favLoc.getLoc())) {
            // Inserting Row
            long rowInserted = db.insert(TABLE_FAVOURITE_LOCATION, null, values);
            db.close(); // Closing database connection

            if (rowInserted != -1)
                chkInsert = true;

        } else {
            chkInsert = false;
        }
        return chkInsert;

    }

    // Getting single CART
    public Cart getCart(int id, int unit_id) {
        try {
            SQLiteDatabase db = this.getReadableDatabase();

       /* Cursor cursor = db.query(TABLE_CART, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);*/

            Cursor cursor = db.rawQuery("select * from " + TABLE_CART + " where " + KEY_product_id + "=" + id + " AND " + KEY_prod_unit + " = " + unit_id, null);

            if (cursor != null)
                cursor.moveToFirst();

            Cart cart = new Cart();//
            //Integer.parseInt(cursor.getString(0)),
            //cursor.getString(1), cursor.getString(2)
            // return CART

            cart.setCartid(String.valueOf(cursor.getInt(0)));
            cart.setProduct_id(String.valueOf(cursor.getString(1)));
            cart.setOndate(cursor.getString(2));
            cart.setQty(cursor.getInt(3));
            cart.setUnit_id(cursor.getInt(4));
            return cart;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    // Getting All CART
    public List<Cart> getAllCarts() {

        List<Cart> cartList = new ArrayList<Cart>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CART;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Cart cart = new Cart();
                cart.setCartid(String.valueOf(cursor.getInt(0)));
                cart.setProduct_id(String.valueOf(cursor.getString(1)));
                cart.setOndate(cursor.getString(2));
                cart.setQty(cursor.getInt(3));
                cart.setUnit_id(cursor.getInt(4));
                // Adding contact to list
                cartList.add(cart);
            } while (cursor.moveToNext());
        }

        // return CART list
        return cartList;

    }

    public boolean getLocExist(String loc1) {

        String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITE_LOCATION + " WHERE " + KEY_loc + " = '" + loc1 + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.getCount() > 0) {
            return true;
        } else {
            return false;
        }


    }


    public List<FavLocation> getAllLocation() {

        List<FavLocation> favLocationList = new ArrayList<FavLocation>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_FAVOURITE_LOCATION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                FavLocation favLocation = new FavLocation();
                favLocation.setLoc_id((cursor.getInt(0)));
                favLocation.setLoc(cursor.getString(1));
                favLocation.setLat(String.valueOf(cursor.getString(2)));
                favLocation.setLng(cursor.getString(3));

                favLocationList.add(favLocation);
            } while (cursor.moveToNext());
        }

        // return CART list
        return favLocationList;

    }


    // Getting CART Count
    public int getCartCount() {
        int cnt = 0;
        try {
            String countQuery = "SELECT  * FROM " + TABLE_CART;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            cnt = cursor.getCount();
            cursor.close();

            // return count
            return cnt;
        } catch (Exception e) {
            e.printStackTrace();
            return cnt;
        }
    }

    // Updating single contact
    public int updateCart(Cart cart) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_product_id, cart.getProduct_id()); // Cart Product Id
        values.put(KEY_ondate, cart.getOndate()); // Cart Date
        values.put(KEY_prod_qty, cart.getQty()); //  Quantity
        values.put(KEY_prod_unit, cart.getUnit_id()); // Product Unit

        // updating row
        return db.update(TABLE_CART, values, KEY_cartid + " = ? AND " + KEY_prod_unit + " = ?",
                new String[]{String.valueOf(cart.getCartid()), String.valueOf(cart.getUnit_id())});
    }

    // Deleting single contact
    public void deleteCart(Cart cart) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CART, KEY_cartid + " = ?",
                new String[]{String.valueOf(cart.getCartid())});
        db.close();
    }

    public String prodIdList() {
        String prodIdsList = "";
        List<Cart> cartList = getAllCarts();
        for (int i = 0; i < cartList.size(); i++) {
            if (i == (cartList.size() - 1)) {
                prodIdsList = prodIdsList + cartList.get(i).getProduct_id();

            } else {
                prodIdsList = prodIdsList + cartList.get(i).getProduct_id() + ",";
            }
        }
        return prodIdsList;
    }

    public String unitIdsArray() {
        String unitIdsList = "";
        List<Cart> cartList = getAllCarts();
        for (int i = 0; i < cartList.size(); i++) {
            if (i == (cartList.size() - 1)) {
                unitIdsList = unitIdsList + cartList.get(i).getUnit_id();

            } else {
                unitIdsList = unitIdsList + cartList.get(i).getUnit_id() + ",";
            }
        }
        return unitIdsList;
    }

    public String qtyIdList() {
        String qtyList = "";
        List<Cart> cartList = getAllCarts();
        for (int i = 0; i < cartList.size(); i++) {
            if (i == (cartList.size() - 1)) {
                qtyList = qtyList + cartList.get(i).getQty();

            } else {
                qtyList = qtyList + cartList.get(i).getQty() + " , ";
            }
        }
        return qtyList;
    }


    public String unitIdList() {
        List<Cart> cartList = getAllCarts();
        //List<String> cartUnitId = new ArrayList<>();
        String cartUnitId = "";
        for (int i = 0; i < cartList.size(); i++) {
            if (i == (cartList.size() - 1)) {
                cartUnitId = cartUnitId + "tpd.id = " + cartList.get(i).getUnit_id();

            } else {
                cartUnitId = cartUnitId + "tpd.id = " + cartList.get(i).getUnit_id() + " OR ";
            }
        }
        return cartUnitId;
    }
}
