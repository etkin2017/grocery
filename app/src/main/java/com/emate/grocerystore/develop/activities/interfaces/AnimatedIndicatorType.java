package com.emate.grocerystore.develop.activities.interfaces;

/**
 * Created by Ashvini on 4/17/2017.
 */

public enum AnimatedIndicatorType {
    DACHSHUND,
    POINT_MOVE,
    LINE_MOVE,
    POINT_FADE,
}
