package com.emate.grocerystore.develop.activities.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.FilterPager;
import com.emate.grocerystore.develop.activities.indicators.TabLayoutIndicator;
import com.emate.grocerystore.develop.activities.interfaces.FilterListener;
import com.emate.grocerystore.develop.activities.models.Brand;
import com.emate.grocerystore.develop.activities.views.CustomTabLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FIlterDialogFragment extends Fragment implements FilterListener {

    View rootView;
    CustomTabLayout tabLayout;

    //This is our viewPager
    ViewPager viewPager;
    ImageView back_btn;


    // FilterListener filterListener;
    String brndIds, prc, dicnt, sortID;

    List<Brand> brandList = new ArrayList<>();

    public List<Brand> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<Brand> brandList) {
        this.brandList = brandList;
    }

    public FIlterDialogFragment() {
        // Required empty public constructor
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_filter_dialog, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        tabLayout = (CustomTabLayout) rootView.findViewById(R.id.tabs);

        back_btn = (ImageView) rootView.findViewById(R.id.back_btn);
        back_btn.setColorFilter(getResources().getColor(R.color.appColor));
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
            }
        });

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        FilterPager adapter = new FilterPager(getActivity().getSupportFragmentManager(), brandList);
        viewPager.setAdapter(adapter);


        try {
            tabLayout.setupWithViewPager(viewPager);
            TabLayoutIndicator indicator = new TabLayoutIndicator(tabLayout);
            tabLayout.setAnimatedIndicator(indicator);


        } catch (Exception e) {
            e.printStackTrace();
        }


        return rootView;

    }


    @Override
    public void onRefineByDataPass(String brandid, String price, String discount) {
        this.brndIds = brandid;
        this.prc = price;
        this.dicnt = discount;

    }

    @Override
    public void onSortByDataPass(String sortby) {
        this.sortID = sortby;
    }
}
