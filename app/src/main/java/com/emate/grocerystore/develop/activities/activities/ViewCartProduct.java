package com.emate.grocerystore.develop.activities.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.Cart;

import java.util.HashMap;

public class ViewCartProduct extends AppCompatActivity implements BaseSliderView.OnSliderClickListener {
    int minteger = 1;
    long price = 0;

    Cart cMdoel;
    TextView priceTxt, quantity, shipping_fee, total, deliver_to;
    Button change_address;
    SliderLayout prod_slider;
    PagerIndicator custom_indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cart_product);
        cMdoel = (Cart) getIntent().getSerializableExtra(AppController.FROM);
        price = Long.parseLong(cMdoel.getPmData().getUnitModel().getPrice());
        init();
        initSlider();
    }

    public void initSlider() {
        HashMap<String, String> url_maps = new HashMap<String, String>();
        if (!cMdoel.getPmData().getImage1().equalsIgnoreCase("0")) {
            url_maps.put(cMdoel.getPmData().getTitle(), cMdoel.getPmData().getImage1());

        }
        if (!cMdoel.getPmData().getImage2().equalsIgnoreCase("0")) {
            url_maps.put(cMdoel.getPmData().getTitle() + " ", cMdoel.getPmData().getImage2());

        }
        if (!cMdoel.getPmData().getImage3().equalsIgnoreCase("0")) {
            url_maps.put(cMdoel.getPmData().getTitle() + "  ", cMdoel.getPmData().getImage3());

        }
        if (!cMdoel.getPmData().getImage4().equalsIgnoreCase("0")) {
            url_maps.put(cMdoel.getPmData().getTitle() + "   ", cMdoel.getPmData().getImage4());

        }
        if (!cMdoel.getPmData().getImage5().equalsIgnoreCase("0")) {
            url_maps.put(cMdoel.getPmData().getTitle() + "     ", cMdoel.getPmData().getImage5());

        }


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView.description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            prod_slider.addSlider(textSliderView);
        }
        prod_slider.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        prod_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        prod_slider.setCustomAnimation(new DescriptionAnimation());
        prod_slider.setDuration(4000);

    }

    public void init() {

        priceTxt = (TextView) findViewById(R.id.price);
        quantity = (TextView) findViewById(R.id.quantity);
        shipping_fee = (TextView) findViewById(R.id.shipping_fee);
        total = (TextView) findViewById(R.id.total);
        deliver_to = (TextView) findViewById(R.id.deliver_to);
        change_address = (Button) findViewById(R.id.change_address);

        priceTxt.setText("Rs. : " + cMdoel.getPmData().getUnitModel().getPrice());
        quantity.setText("1");
        minteger = 1;
        shipping_fee.setText("Rs. : " + cMdoel.getPmData().getShipping());


        prod_slider = (SliderLayout) findViewById(R.id.prod_slider);
        custom_indicator = (PagerIndicator) findViewById(R.id.custom_indicator);

        try {
            long tPrice = Long.parseLong(cMdoel.getPmData().getUnitModel().getPrice()) + Long.parseLong(cMdoel.getPmData().getShipping());
            total.setText("Rs. : " + tPrice);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }


    public void increaseInteger(View view) {
        minteger = minteger + 1;
        display(minteger);

    }

    public void decreaseInteger(View view) {
        if (minteger > 0) {
            minteger = minteger - 1;
            display(minteger);
        }
    }

    private void display(int number) {

        quantity.setText("" + number);
        long price1 = price * number;
        priceTxt.setText("Rs. : " + price1);

        long tP = Long.parseLong(cMdoel.getPmData().getShipping()) + price1;
        total.setText("Rs. : " + tP);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
