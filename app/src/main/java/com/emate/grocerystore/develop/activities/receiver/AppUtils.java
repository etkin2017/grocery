package com.emate.grocerystore.develop.activities.receiver;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;


public class AppUtils {


    public class LocationConstants {
        public static final int SUCCESS_RESULT = 0;

        public static final int FAILURE_RESULT = 1;

        public static final String PACKAGE_NAME = "com.acekabs.passenger";

        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

        public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

        public static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
        public static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
        public static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";

        public static final String LATITUDE = PACKAGE_NAME + ".Latitude";
        public static final String LONGITUDE = PACKAGE_NAME + ".Longitude";

        public String developers_api = "AIzaSyBwYgQ76uJOpk-WHe4gKwP1iOfJV_MZFso";
        public String owner_api = "AIzaSyBd8C9KQLHnvqRbRD0PZijOLqjPtIEv0pA";

    }

    public final class Constants {
        public static final int SUCCESS_RESULT = 0;
        public static final int FAILURE_RESULT = 1;

        public static final int USE_ADDRESS_NAME = 1;
        public static final int USE_ADDRESS_LOCATION = 2;

        public static final String PACKAGE_NAME =
                "com.acekabs.passenger";
        public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
        public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
        public static final String RESULT_ADDRESS = PACKAGE_NAME + ".RESULT_ADDRESS";
        public static final String LOCATION_LATITUDE_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_LATITUDE_DATA_EXTRA";
        public static final String LOCATION_LONGITUDE_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_LONGITUDE_DATA_EXTRA";
        public static final String LOCATION_NAME_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_NAME_DATA_EXTRA";
        public static final String FETCH_TYPE_EXTRA = PACKAGE_NAME + ".FETCH_TYPE_EXTRA";
        public static final String PREFERENCES_FILE = "my_app_settings";

        public static final int CONN_TIME_OUT = 30000;
        public static final String VERIFTYURL = "http://riders.acekabs.com:8080/acekabs/generate/email/sms/";
        public static final String KEY_EMAIL = "email";
    }


    public static boolean hasLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

}
