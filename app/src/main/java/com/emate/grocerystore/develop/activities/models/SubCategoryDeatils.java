package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 11/7/2017.
 */

public class SubCategoryDeatils implements Serializable {

    String det_cat_id,det_cat_name,sub_cat_id,id;


    public String getDet_cat_id() {
        return det_cat_id;
    }

    public void setDet_cat_id(String det_cat_id) {
        this.det_cat_id = det_cat_id;
    }

    public String getDet_cat_name() {
        return det_cat_name;
    }

    public void setDet_cat_name(String det_cat_name) {
        this.det_cat_name = det_cat_name;
    }

    public String getSub_cat_id() {
        return sub_cat_id;
    }

    public void setSub_cat_id(String sub_cat_id) {
        this.sub_cat_id = sub_cat_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
