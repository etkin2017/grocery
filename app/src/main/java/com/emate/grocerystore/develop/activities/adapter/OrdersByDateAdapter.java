package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.OrderDetails;
import com.emate.grocerystore.develop.activities.models.Orders;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Etkin King on 2/10/2018.
 */

public class OrdersByDateAdapter extends RecyclerView.Adapter<OrdersByDateAdapter.OrderHolders> {
    List<Orders> ordersList;
    Context ctx;
    List<String> allDts;

    public OrdersByDateAdapter(List<Orders> ordersList, Context ctx, List<String> allDts) {
        this.ordersList = ordersList;
        this.ctx = ctx;
        this.allDts = allDts;
    }

    @Override
    public OrderHolders onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.order_rows, parent, false);
        return new OrderHolders(view);
    }

    @Override
    public void onBindViewHolder(OrderHolders orderHolders, final int i) {
        try {
            orderHolders.order_date.setText(AppController.getDay(Long.parseLong(allDts.get(i))));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        orderHolders.prod_status.setText("Status : " + getStatusByOrder(allDts.get(i)));
        try {
            orderHolders.price.setText(getProductAmt(allDts.get(i)) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            orderHolders.purchased_prod.setText("Total Product: " + getProductCnt(allDts.get(i)) + "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderHolders.container_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<Orders> ordersList1 = new ArrayList<>();
                for (int a = 0; a < ordersList.size(); a++) {
                    if (ordersList.get(a).getOrder_dt_tim().equalsIgnoreCase(allDts.get(i))) {
                        ordersList1.add(ordersList.get(a));
                    }
                }
                Intent ite = new Intent(ctx, OrderDetails.class);
                ite.putExtra("orderList", (ArrayList) ordersList1);
                ctx.startActivity(ite);
            }
        });

    }

    public String getStatusByOrder(String dt) {
        String orderStatus = " ";
        for (int i = 0; i < ordersList.size(); i++) {
            if (ordersList.get(i).getOrder_dt_tim().equalsIgnoreCase(dt)) {
                orderStatus = ordersList.get(i).getDelivery_status();
                i = ordersList.size();
            }
        }

        return orderStatus;
    }

    public int getProductCnt(String dt) {
        int cnt = 0;
        for (int i = 0; i < ordersList.size(); i++) {
            if (ordersList.get(i).getOrder_dt_tim().equalsIgnoreCase(dt)) {
                cnt = cnt + 1;
            }
        }

        return cnt;
    }

    public int getProductAmt(String dt) {
        int amt = 0;
        for (int i = 0; i < ordersList.size(); i++) {
            if (ordersList.get(i).getOrder_dt_tim().equalsIgnoreCase(dt)) {
                amt = amt + Integer.parseInt(ordersList.get(i).getTotal_amt());
            }
        }

        return amt;
    }

    @Override
    public int getItemCount() {
        return allDts.size();
    }

    public class OrderHolders extends RecyclerView.ViewHolder {
        TextView purchased_prod, prod_status, price, order_date;
        LinearLayout container_id;


        public OrderHolders(View itemView) {
            super(itemView);
            container_id = (LinearLayout) itemView.findViewById(R.id.container_id);
            purchased_prod = (TextView) itemView.findViewById(R.id.purchased_prod);
            prod_status = (TextView) itemView.findViewById(R.id.prod_status);
            price = (TextView) itemView.findViewById(R.id.price);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
        }
    }
}
