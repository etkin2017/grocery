package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.emate.grocerystore.R;

import com.emate.grocerystore.develop.activities.models.FavLocation;

import java.util.List;

/**
 * Created by Ashvini on 3/3/2018.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationHolder> {
    Context ctx;
    List<FavLocation> favLocationList;

    public LocationAdapter(Context ctx, List<FavLocation> favLocationList) {
        this.ctx = ctx;
        this.favLocationList = favLocationList;
    }

    @Override
    public LocationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.location_row, parent, false);
        return new LocationHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationHolder holder, int position) {
        holder.loc.setText(favLocationList.get(position).getLoc());


    }

    @Override
    public int getItemCount() {
        return favLocationList.size();
    }

    public class LocationHolder extends RecyclerView.ViewHolder {
        ImageButton fav_btn;
        TextView loc;

        public LocationHolder(View itemView) {
            super(itemView);
            loc = (TextView) itemView.findViewById(R.id.loc);
            fav_btn = (ImageButton) itemView.findViewById(R.id.fav_btn);

        }
    }

}
