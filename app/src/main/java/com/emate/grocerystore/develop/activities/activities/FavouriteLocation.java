package com.emate.grocerystore.develop.activities.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.LocationAdapter;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.FavLocation;

import java.util.List;

public class FavouriteLocation extends AppCompatActivity {


    RecyclerView loc_list;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite_location);
        loc_list = (RecyclerView) findViewById(R.id.loc_list);
        databaseHandler = new DatabaseHandler(FavouriteLocation.this);

        List<FavLocation> favLocationList = databaseHandler.getAllLocation();

        LocationAdapter adapter = new LocationAdapter(FavouriteLocation.this, favLocationList);
        loc_list.setLayoutManager(new LinearLayoutManager(this));
        loc_list.setAdapter(adapter);


    }
}
