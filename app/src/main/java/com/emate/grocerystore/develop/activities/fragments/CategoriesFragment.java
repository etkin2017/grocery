package com.emate.grocerystore.develop.activities.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.ContactUsActivity;
import com.emate.grocerystore.develop.activities.activities.EarnedPoints;
import com.emate.grocerystore.develop.activities.activities.FavouriteLocation;
import com.emate.grocerystore.develop.activities.activities.HomeActivity;
import com.emate.grocerystore.develop.activities.activities.LoginActivity;
import com.emate.grocerystore.develop.activities.activities.MyOrderActivity;
import com.emate.grocerystore.develop.activities.activities.RewardPoints;
import com.emate.grocerystore.develop.activities.activities.SetMapLocationActivity;
import com.emate.grocerystore.develop.activities.models.Category;
import com.emate.grocerystore.develop.activities.models.PrefManager;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {


    public CategoriesFragment() {
        // Required empty public constructor
    }


    FragmentManager manager;
    FragmentTransaction transaction;
    RecyclerView lvCities;
    View rootview;
    LinearLayout set_location, contact_us_layout, login_container;
    TextView nm, eml, access_login;
    ImageButton logout_btn;
    PrefManager prefManager;// = new PrefManager(getActivity());
    LinearLayout order_container;
    TextView rewards_tabs, earned_rewards_tabs, fav_btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_categories, container, false);
        lvCities = (RecyclerView) rootview.findViewById(R.id.categories_list);
        set_location = (LinearLayout) rootview.findViewById(R.id.set_location);
        contact_us_layout = (LinearLayout) rootview.findViewById(R.id.contact_us_layout);
        login_container = (LinearLayout) rootview.findViewById(R.id.login_container);
        order_container = (LinearLayout) rootview.findViewById(R.id.order_container);
        prefManager = new PrefManager(getActivity());
        nm = (TextView) rootview.findViewById(R.id.nm);
        eml = (TextView) rootview.findViewById(R.id.eml);
        access_login = (TextView) rootview.findViewById(R.id.access_login);
        logout_btn = (ImageButton) rootview.findViewById(R.id.logout_btn);

        rewards_tabs = (TextView) rootview.findViewById(R.id.rewards_tabs);
        earned_rewards_tabs = (TextView) rootview.findViewById(R.id.earned_rewards_tabs);
        fav_btn = (TextView) rootview.findViewById(R.id.fav_btn);

        rewards_tabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), RewardPoints.class));
            }
        });
        earned_rewards_tabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), EarnedPoints.class));
            }
        });

        fav_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FavouriteLocation.class));

            }
        });


        access_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prefManager.setIsUserLoggedIn(false);
                setLoginData();
            }
        });

        order_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), MyOrderActivity.class));
            }
        });
        set_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent i1 = new Intent(getActivity(), SetLocaton.class);
                startActivity(i1);*/

                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    Intent i1 = new Intent(getActivity(), SetMapLocationActivity.class);
                    startActivity(i1);
                }
            }
        });

        contact_us_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ContactUsActivity.class));
            }
        });


        try {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        lvCities.setLayoutAnimation(animation);
        CategoriesAdapter adapter = new CategoriesAdapter(HomeActivity.catList);
        lvCities.setHasFixedSize(true);
        lvCities.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvCities.setAdapter(adapter);
        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        setLoginData();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, To proceed further please enable gps")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void setLoginData() {
        try {

            boolean chkLogin = prefManager.isUserLoggedIn();
            if (chkLogin) {
                nm.setText(prefManager.getUsername());
                eml.setText(prefManager.getUserEmail());
                login_container.setVisibility(View.VISIBLE);
                access_login.setVisibility(View.GONE);
                order_container.setVisibility(View.VISIBLE);
                rewards_tabs.setVisibility(View.VISIBLE);
                earned_rewards_tabs.setVisibility(View.VISIBLE);
                fav_btn.setVisibility(View.VISIBLE);

            } else {
                login_container.setVisibility(View.GONE);
                access_login.setVisibility(View.VISIBLE);
                order_container.setVisibility(View.GONE);
                rewards_tabs.setVisibility(View.GONE);
                earned_rewards_tabs.setVisibility(View.GONE);
                fav_btn.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CatHolder> {

        List<Category> categoriesList;

        public CategoriesAdapter(List<Category> categoriesList) {
            this.categoriesList = categoriesList;
        }


        @Override
        public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(getActivity()).inflate(R.layout.caterories_row, parent, false);
            CatHolder catHolder = new CatHolder(v);
            return catHolder;
        }

        @Override
        public void onBindViewHolder(final CatHolder holder, final int position) {

            holder.cat_nm.setText(categoriesList.get(position).getCategory_type());
            if (Config.cotainCat(HomeActivity.subCatList, categoriesList.get(position).getId())) {
                holder.cat_arrow.setVisibility(View.VISIBLE);
            } else {
                holder.cat_arrow.setVisibility(View.GONE);

            }

            holder.category_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (holder.cat_arrow.getVisibility() != View.GONE) {
                        SubCategoriesFragment states = new SubCategoriesFragment();
                        //  city.setData(position,getActivity());
                        manager = getActivity().getSupportFragmentManager();
                        transaction = manager.beginTransaction();
                        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);

                        Bundle bundle = new Bundle();
                        bundle.putString("cat_id", categoriesList.get(position).getId());
                        states.setArguments(bundle);


                        transaction.replace(R.id.contain, states, "states");
                        transaction.addToBackStack("states");
                        transaction.commit();
                    }
                }
            });

        }

        @Override
        public int getItemCount() {
            return categoriesList.size();
        }

        public class CatHolder extends RecyclerView.ViewHolder {

            TextView cat_nm;
            ImageView cat_arrow;
            LinearLayout category_container;

            public CatHolder(View itemView) {
                super(itemView);
                cat_arrow = (ImageView) itemView.findViewById(R.id.cat_arrow);
                cat_nm = (TextView) itemView.findViewById(R.id.cat_nm);
                category_container = (LinearLayout) itemView.findViewById(R.id.category_container);


            }
        }

    }


}
