package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.Orders;
import com.emate.grocerystore.develop.activities.views.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashvini on 1/27/2018.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderHolder> {

    List<Orders> ordersList;
    Context ctx;


    public OrderAdapter(Context ctx, List<Orders> ordersList) {
        this.ctx = ctx;
        this.ordersList = ordersList;

    }

    @Override
    public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.order_row, parent, false);
        return new OrderHolder(view);

    }

    @Override
    public void onBindViewHolder(OrderHolder holder, int position) {
        AppController.setData(holder.product_nm, ordersList.get(position).getPm().getTitle());
        AppController.setData(holder.quantity, "Qty. : " + ordersList.get(position).getProd_qty());
        AppController.setData(holder.price, ordersList.get(position).getTotal_amt());
        AppController.setData(holder.product_desc, ordersList.get(position).getPm().getContent());
        AppController.setData(holder.prod_status, ordersList.get(position).getDelivery_status());
        AppController.setData(holder.order_date, "Date : " + AppController.retDate(ordersList.get(position).getOrder_dt_tim()));
        AppController.setData(holder.p_quantity, ordersList.get(position).getPm().getUnitModel().getPrice() + "/" + ordersList.get(position).getPm().getUnitModel().getQuantity() + ordersList.get(position).getPm().getUnitModel().getUnit());
        Picasso.with(ctx).load(Uri.parse(ordersList.get(position).getPm().getImage1())).into(holder.product_img);

    }

    @Override
    public int getItemCount() {
        return ordersList.size();
    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        TextView product_nm, product_desc, prod_status, quantity, price, order_date, p_quantity;
        CircularImageView product_img;
        LinearLayout container_id;

        public OrderHolder(View itemView) {
            super(itemView);

            product_nm = (TextView) itemView.findViewById(R.id.product_nm);
            product_desc = (TextView) itemView.findViewById(R.id.product_desc);
            prod_status = (TextView) itemView.findViewById(R.id.prod_status);
            product_img = (CircularImageView) itemView.findViewById(R.id.product_img);
            container_id = (LinearLayout) itemView.findViewById(R.id.container_id);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            order_date = (TextView) itemView.findViewById(R.id.order_date);
            p_quantity = (TextView) itemView.findViewById(R.id.p_quantity);

            price = (TextView) itemView.findViewById(R.id.price);

        }
    }
}
