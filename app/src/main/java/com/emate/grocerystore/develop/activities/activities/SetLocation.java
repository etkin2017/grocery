package com.emate.grocerystore.develop.activities.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.FavouriteListArrayAdapter;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.FavLocation;

import java.util.ArrayList;
import java.util.List;

public class SetLocation extends AppCompatActivity {

    LinearLayout spinner_container;
    CardView use_my_location;
    DatabaseHandler databaseHandler;
    List<String> locList = new ArrayList<>();
    List<FavLocation> favLocationList = new ArrayList<>();
    Spinner favloc_spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_location);
        spinner_container = (LinearLayout) findViewById(R.id.spinner_container);
        use_my_location = (CardView) findViewById(R.id.use_my_location);
        favloc_spinner = (Spinner) findViewById(R.id.favloc_spinner);


        favloc_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    FavLocation fav = favLocationList.get(i);
                    Intent i1 = new Intent(SetLocation.this, SetMapLocationActivity.class);
                    i1.putExtra(Config.FROM, "1");
                    i1.putExtra(Config.DATA, fav);
                    startActivity(i1);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        use_my_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final LocationManager manager = (LocationManager) SetLocation.this.getSystemService(Context.LOCATION_SERVICE);

                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    Intent i1 = new Intent(SetLocation.this, SetMapLocationActivity.class);
                    i1.putExtra(Config.FROM, "0");

                    startActivity(i1);
                }


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        favLocationList.clear();

        FavLocation favLocation = new FavLocation();
        favLocation.setLoc("Select");
        favLocationList.add(favLocation);
        getLoc();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SetLocation.this);
        builder.setMessage("Your GPS seems to be disabled, To proceed further please enable gps")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void getLoc() {
        try {
            databaseHandler = new DatabaseHandler(SetLocation.this);

            List<FavLocation> favLocationList1 = databaseHandler.getAllLocation();
            favLocationList.addAll(favLocationList1);

        } catch (Exception e) {
            e.printStackTrace();
        }


        FavouriteListArrayAdapter adapter = new FavouriteListArrayAdapter(SetLocation.this,
                R.layout.spinner_layout, favLocationList);
        favloc_spinner.setAdapter(adapter);
    }
}
