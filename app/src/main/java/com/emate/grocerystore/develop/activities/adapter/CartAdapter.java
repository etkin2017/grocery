package com.emate.grocerystore.develop.activities.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.Cart;
import com.emate.grocerystore.develop.activities.views.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ashvini on 1/16/2018.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {

    Context ctx;
    List<Cart> cartList;
    TextView totaltxt;
    TextView shippingTxttxt;
    TextView deliver_txt;
    DatabaseHandler databaseHandler;

    public CartAdapter(Context ctx, List<Cart> cartList, TextView totaltxt, TextView shippingTxttxt, TextView deliver_txt) {
        this.ctx = ctx;
        this.cartList = cartList;
        this.totaltxt = totaltxt;
        this.shippingTxttxt = shippingTxttxt;
        this.deliver_txt = deliver_txt;
        if (cartList.size() != 0) {
            setTotaltxtData(cartList);
        }

        databaseHandler = new DatabaseHandler(ctx);


    }

    public void setTotaltxtData(List<Cart> cartList1) {
        long calPrice = 0;
        for (Cart c : cartList1) {
            if (c.getPmData().getUnitModel().getPrice() != null) {

                long singleProdPrc = Long.parseLong(c.getPmData().getUnitModel().getPrice()) * c.getQty();

                calPrice = calPrice + singleProdPrc;
            }
        }

        totaltxt.setText(String.valueOf(calPrice) + "");
        if (calPrice > 500) {
            AppController.setData(shippingTxttxt, String.valueOf(0));
        } else {
            AppController.setData(shippingTxttxt, String.valueOf(50));
        }
        //AppController.setData(deliver_txt, cartList.get(0).getChng_addrs());
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.cart_row, parent, false);
        return new CartHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartHolder holder, final int position) {
        try {
            final Cart c = cartList.get(position);
            AppController.setData(holder.product_nm, c.getPmData().getTitle());
            AppController.setData(holder.deliver_to, "-");
            AppController.setData(holder.prod_status, c.getPmData().getAvailable());
            AppController.setData(holder.product_desc, c.getPmData().getContent());
            AppController.setData(holder.shipping_fee, c.getPmData().getShipping());
            long tP = Long.parseLong(c.getPmData().getUnitModel().getPrice()) * c.getQty();
            AppController.setData(holder.price, (tP) + "");
            AppController.setData(holder.quantity, c.getQty() + "");
            AppController.setData(holder.p_quantity, c.getPmData().getUnitModel().getPrice() + "/" + c.getPmData().getUnitModel().getQuantity() + c.getPmData().getUnitModel().getUnit());
            //AppController.setData(holder.quantity, "Qty. : " + c.getQty());


            //holder.product_img

           /* holder.container_id.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent cartIntent = new Intent(ctx, ViewCartProduct.class);
                    cartIntent.putExtra(AppController.FROM, c);
                    ctx.startActivity(cartIntent);
                }
            });
*/
            holder.increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Cart c1 = databaseHandler.getCart(Integer.parseInt(c.getPmData().getId()), Integer.parseInt(c.getPmData().getUnitModel().getId()));
                    cartList.get(position).setQty(cartList.get(position).getQty() + 1);
                    updateQuantity(true, c1);
                    holder.quantity.setText(cartList.get(position).getQty() + "");
                    long totP = cartList.get(position).getQty() * Long.parseLong(cartList.get(position).getPmData().getUnitModel().getPrice());
                    holder.price.setText(String.valueOf(totP));
                    // changeQuantity(cartList.get(position).getmInteger(), Long.parseLong(cartList.get(position).getPmData().getPrice()), holder.price);
                    setTotaltxtData(cartList);


                }
            });

            holder.decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (cartList.get(position).getQty() > 1) {
                        Cart c1 = databaseHandler.getCart(Integer.parseInt(c.getPmData().getId()), Integer.parseInt(c.getPmData().getUnitModel().getId()));
                        cartList.get(position).setQty(cartList.get(position).getQty() - 1);
                        holder.quantity.setText(c.getQty() + "");
                        updateQuantity(false, c1);

                        long totP = cartList.get(position).getQty() * Long.parseLong(cartList.get(position).getPmData().getUnitModel().getPrice());
                        holder.price.setText(String.valueOf(totP));
                        // changeQuantity(cartList.get(position).getmInteger(), Long.parseLong(cartList.get(position).getPmData().getPrice()), holder.price);

                        setTotaltxtData(cartList);

                    }
                }
            });

         /*   holder.remove_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeCartItem(c.getCartid());
                }
            });*/

            Picasso.with(ctx).load(Uri.parse(c.getPmData().getImage1())).into(holder.product_img);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void updateQuantity(boolean a, Cart c1) {
        DatabaseHandler databaseHandler = new DatabaseHandler(ctx);
        Cart cc = new Cart();
        cc.setCartid(c1.getCartid());
        if (a) {
            cc.setQty((c1.getQty() + 1));
        } else {
            cc.setQty((c1.getQty() - 1));

        }
        cc.setUnit_id(c1.getUnit_id());
        cc.setProduct_id(c1.getProduct_id());
        cc.setOndate(c1.getOndate());
        databaseHandler.updateCart(cc);
        //changeCartData();
    }

   /* public void changeCartData(CartHolder holder) {
        try {
            c2 = databaseHandler.getCart(Integer.parseInt(pm.getId()));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (c2 != null) {
            int qty_value = c2.getQty();
            holder.decrease.setVisibility(View.VISIBLE);
            holder.increase.setVisibility(View.VISIBLE);

        } else {

            holder.decrease.setVisibility(View.GONE);
            holder.increase.setVisibility(View.GONE);
        }
    }*/


    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {

        TextView product_nm, product_desc, deliver_to, shipping_fee, prod_status, quantity, price, remove_cart, txt_price, txt_quat, p_quantity;
        ImageView decrease, increase;
        CircularImageView product_img;
        LinearLayout container_id;

        public CartHolder(View itemView) {
            super(itemView);

            product_nm = (TextView) itemView.findViewById(R.id.product_nm);
            product_desc = (TextView) itemView.findViewById(R.id.product_desc);
            deliver_to = (TextView) itemView.findViewById(R.id.deliver_to);
            shipping_fee = (TextView) itemView.findViewById(R.id.shipping_fee);
            prod_status = (TextView) itemView.findViewById(R.id.prod_status);
            product_img = (CircularImageView) itemView.findViewById(R.id.product_img);
            container_id = (LinearLayout) itemView.findViewById(R.id.container_id);
            quantity = (TextView) itemView.findViewById(R.id.quantity);
            price = (TextView) itemView.findViewById(R.id.price);

            decrease = (ImageView) itemView.findViewById(R.id.decrease);
            increase = (ImageView) itemView.findViewById(R.id.increase);
            remove_cart = (TextView) itemView.findViewById(R.id.remove_cart);

            txt_price = (TextView) itemView.findViewById(R.id.txt_price);
            txt_quat = (TextView) itemView.findViewById(R.id.txt_quat);
            p_quantity = (TextView) itemView.findViewById(R.id.p_quantity);


        }
    }
}
