package com.emate.grocerystore.develop.activities.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.activities.HomeActivity;
import com.emate.grocerystore.develop.activities.activities.ProductByDetailCategoryActivity;
import com.emate.grocerystore.develop.activities.models.SubCategoryDeatils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategoriesDetailsFragment extends Fragment {


    String sub_cat_id="", cat_id="";
    ImageView back_btn;
    public SubCategoriesDetailsFragment() {
        // Required empty public constructor
    }


    RecyclerView lvCities;
    View rootview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_sub_categories_details, container, false);
        lvCities = (RecyclerView) rootview.findViewById(R.id.sub_category_deatils_list);
        back_btn =(ImageView) rootview.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                }
            }
        });
        cat_id =getArguments().getString("cat_id");
        sub_cat_id =getArguments().getString("sub_cat_id");
        try{
            ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getActivity(), resId);
        lvCities.setLayoutAnimation(animation);
        SubCategoryDeatilsAdapter adapter = new SubCategoryDeatilsAdapter(sortByCatId());
        lvCities.setHasFixedSize(true);
        lvCities.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvCities.setAdapter(adapter);
        return rootview;
    }


    public List<SubCategoryDeatils> sortByCatId(){
        List<SubCategoryDeatils> sortedList =new ArrayList<>();
        for (int i=0; i<HomeActivity.subCategoryDeatilsList.size();i++) {
            if (HomeActivity.subCategoryDeatilsList.get(i).getId().equals(cat_id) && HomeActivity.subCategoryDeatilsList.get(i).getSub_cat_id().equals(sub_cat_id)){
                sortedList.add(HomeActivity.subCategoryDeatilsList.get(i));
            }
        }

        return sortedList;
    }

    public class SubCategoryDeatilsAdapter extends RecyclerView.Adapter<SubCategoryDeatilsAdapter.CatHolder> {

        List<SubCategoryDeatils> categoriesList;

        public SubCategoryDeatilsAdapter(List<SubCategoryDeatils> categoriesList) {
            this.categoriesList = categoriesList;
        }


        @Override
        public CatHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(getActivity()).inflate(R.layout.caterories_row, parent, false);
            CatHolder catHolder = new CatHolder(v);
            return catHolder;
        }

        @Override
        public void onBindViewHolder(CatHolder holder, final int position) {

            holder.cat_nm.setText(categoriesList.get(position).getDet_cat_name());
            holder.cat_arrow.setVisibility(View.INVISIBLE);

            holder.cat_nm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent i = new Intent(getActivity(), ProductByDetailCategoryActivity.class);
                    i.putExtra("prod_cat",categoriesList.get(position));
                    startActivity(i);
                   // Toast.makeText(getActivity(),categoriesList.get(position).getDet_cat_name(),Toast.LENGTH_LONG).show();
                }
            });

        }

        @Override
        public int getItemCount() {
            return categoriesList.size();
        }

        public class CatHolder extends RecyclerView.ViewHolder {

            TextView cat_nm;
            ImageView cat_arrow;

            public CatHolder(View itemView) {
                super(itemView);
                cat_arrow = (ImageView) itemView.findViewById(R.id.cat_arrow);
                cat_nm = (TextView) itemView.findViewById(R.id.cat_nm);

            }
        }

    }


}
