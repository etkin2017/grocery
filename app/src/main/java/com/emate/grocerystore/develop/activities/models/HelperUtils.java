package com.emate.grocerystore.develop.activities.models;

/**
 * Created by Ashvini on 4/17/2017.
 */


import android.content.res.Resources;
import android.util.TypedValue;

public class HelperUtils {

    public static int pxToDp(float px) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px, Resources.getSystem().getDisplayMetrics());
    }

    public static int dpToPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
    }

}
