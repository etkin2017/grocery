package com.emate.grocerystore.develop.activities.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.ProductAutoCompleteAdapter;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.views.DelayAutoCompleteTextView;

public class SearchProduct extends AppCompatActivity {

    DelayAutoCompleteTextView search_product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Search Products");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }

        search_product = (DelayAutoCompleteTextView) findViewById(R.id.search_product);
        search_product.setThreshold(2);
        search_product.setAdapter(new ProductAutoCompleteAdapter(this, (android.widget.ProgressBar) findViewById(R.id.pb_loading_indicator))); // 'this' is Activity instance
        search_product.setLoadingIndicator(
                (android.widget.ProgressBar) findViewById(R.id.pb_loading_indicator));
        search_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ProductModel products = (ProductModel) adapterView.getItemAtPosition(position);
                search_product.setText(products.getTitle());
                Intent product_Details = new Intent(SearchProduct.this, ProdDetails.class);
                product_Details.putExtra("product_details", products);
                startActivity(product_Details);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
