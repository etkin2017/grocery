package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 1/16/2018.
 */

public class Cart implements Serializable {
    String cartid, user_id, product_id, ondate, chng_addrs, deliver_to, delive_mob_no;
    int unit_id;
    int qty ;

   /* int mInteger;

    public int getmInteger() {
        return mInteger;
    }

    public void setmInteger(int mInteger) {
        this.mInteger = mInteger;
    }
*/
    Users usersData;
    ProductModel pmData;

    public int getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(int unit_id) {
        this.unit_id = unit_id;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Users getUsersData() {
        return usersData;
    }

    public void setUsersData(Users usersData) {
        this.usersData = usersData;
    }

    public ProductModel getPmData() {
        return pmData;
    }

    public void setPmData(ProductModel pmData) {
        this.pmData = pmData;
    }

    public String getCartid() {
        return cartid;
    }

    public void setCartid(String cartid) {
        this.cartid = cartid;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOndate() {
        return ondate;
    }

    public void setOndate(String ondate) {
        this.ondate = ondate;
    }

    public String getChng_addrs() {
        return chng_addrs;
    }

    public void setChng_addrs(String chng_addrs) {
        this.chng_addrs = chng_addrs;
    }

    public String getDeliver_to() {
        return deliver_to;
    }

    public void setDeliver_to(String deliver_to) {
        this.deliver_to = deliver_to;
    }

    public String getDelive_mob_no() {
        return delive_mob_no;
    }

    public void setDelive_mob_no(String delive_mob_no) {
        this.delive_mob_no = delive_mob_no;
    }
}
