package com.emate.grocerystore.develop.activities.models;

import java.io.Serializable;

/**
 * Created by Ashvini on 1/16/2018.
 */

public class Users implements Serializable {

    String user_name, email, mb_no, shipping_add, total_order, otp, unique_id;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMb_no() {
        return mb_no;
    }

    public void setMb_no(String mb_no) {
        this.mb_no = mb_no;
    }

    public String getShipping_add() {
        return shipping_add;
    }

    public void setShipping_add(String shipping_add) {
        this.shipping_add = shipping_add;
    }

    public String getTotal_order() {
        return total_order;
    }

    public void setTotal_order(String total_order) {
        this.total_order = total_order;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    /* String uid,username,password,name,address;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }*/
}
