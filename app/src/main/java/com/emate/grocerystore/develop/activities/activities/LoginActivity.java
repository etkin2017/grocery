package com.emate.grocerystore.develop.activities.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.emate.grocerystore.AppController;
import com.emate.grocerystore.Config;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.PrefManager;

import org.json.JSONArray;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    EditText u_pswd, email_id;
    Button btn_login;
    TextView forget_password, txt_sign_up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(email_id.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Email Id", LoginActivity.this);
                } else if (TextUtils.isEmpty(u_pswd.getText().toString())) {
                    AppController.retShowAlertDialod("Please Enter Password", LoginActivity.this);
                } else {
                    loginUser();
                }
            }
        });
    }


    public void init() {

        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Login");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        u_pswd = (EditText) findViewById(R.id.u_pswd);
        email_id = (EditText) findViewById(R.id.email_id);
        btn_login = (Button) findViewById(R.id.btn_login);
        txt_sign_up = (TextView) findViewById(R.id.txt_sign_up);
        forget_password = (TextView) findViewById(R.id.forget_password);

        txt_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  startActivity(new Intent(LoginActivity.this, RegisterActivity.class));

                String intent_data = getIntent().getStringExtra("groceryData");
                String amount = getIntent().getStringExtra("amount");
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                intent.putExtra("groceryData", intent_data);
                intent.putExtra("amount", amount);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loginUser() {


        final ProgressDialog pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        String url = Config.Api_Url + "login";// "getProductBySuperCategory"; //getProd
        final JSONObject jsonObject = new JSONObject();
        try {
            //user_name,email,mb_no,shipping_add,total_order,user_status

            jsonObject.put("email", email_id.getText().toString());
            jsonObject.put("mb_no", u_pswd.getText().toString());


        } catch (Exception ex) {
            ex.printStackTrace();
        }


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pDialog.dismiss();
                try {
                    JSONArray jsonArray = response.getJSONArray("success");
                    if (jsonArray.length() > 0) {
                        JSONObject jobject = jsonArray.getJSONObject(0);
                        // AppController.retShowAlertDialod("Login Done", LoginActivity.this);
                        PrefManager prefManager = new PrefManager(LoginActivity.this);
                        prefManager.setWhoLogin(Config.USER);
                        prefManager.setIsUserLoggedIn(true);
                        prefManager.setMobileNumber(jobject.getString("mb_no"));
                        prefManager.setUserEmail(jobject.getString("email"));
                        prefManager.setUserName(jobject.getString("user_name"));
                        prefManager.setUserID(jobject.getString("id"));


                        try {
                            String intent_data = getIntent().getStringExtra("groceryData");
                            String amount = getIntent().getStringExtra("amount");
                            if (intent_data != null) {
                                Intent intent = new Intent(LoginActivity.this, BuyProduct.class);
                                intent.putExtra("groceryData", intent_data);
                                intent.putExtra("amount", amount);
                                startActivity(intent);
                                finish();
                            } else {
                                finish();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        AppController.retShowAlertDialod("Login Failed", LoginActivity.this);

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                error.printStackTrace();
                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_LONG).show();

            }
        });
        AppController.getInstance().

                addToRequestQueue(req, "remove_cartItem");

    }
}
