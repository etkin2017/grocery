package com.emate.grocerystore.develop.activities.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.emate.grocerystore.AppController;
import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.adapter.OrderAdapter;
import com.emate.grocerystore.develop.activities.models.Orders;

import java.util.ArrayList;
import java.util.List;

public class OrderDetails extends AppCompatActivity {

    TextView txt_dt, total_item;
    RecyclerView order_history;
    TextView no_result_found;
    List<Orders> productModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        init();
    }

    public void init() {
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Order Details");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }


        order_history = (RecyclerView) findViewById(R.id.order_history);
        no_result_found = (TextView) findViewById(R.id.no_result_found);
        total_item = (TextView) findViewById(R.id.total_item);
        txt_dt = (TextView) findViewById(R.id.txt_dt);

        productModelList = (ArrayList<Orders>) getIntent().getSerializableExtra("orderList");
        total_item.setText("ITEMS : " + productModelList.size());
        txt_dt.setText("DATE : " + AppController.getDay(Long.parseLong(productModelList.get(0).getOrder_dt_tim())));


        if (productModelList.size() > 0) {
            no_result_found.setVisibility(View.GONE);
            order_history.setVisibility(View.VISIBLE);
        } else {
            no_result_found.setVisibility(View.VISIBLE);
            order_history.setVisibility(View.GONE);
        }
        OrderAdapter adapter = new OrderAdapter(OrderDetails.this, productModelList);
        order_history.setLayoutManager(new LinearLayoutManager(OrderDetails.this));
        order_history.setAdapter(adapter);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
