package com.emate.grocerystore.develop.activities.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.emate.grocerystore.R;
import com.emate.grocerystore.develop.activities.models.PlaceArrayAdapter;
import com.emate.grocerystore.develop.activities.receiver.AppUtils;
import com.emate.grocerystore.develop.activities.receiver.FetchAddressIntentService;
import com.emate.grocerystore.develop.activities.receiver.GPSTracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class SetLocaton extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleMap.OnCameraChangeListener,
        LocationListener, com.google.android.gms.maps.OnMapReadyCallback,
        com.google.android.gms.location.LocationListener,
        ResultCallback<LocationSettingsResult> {

    String LOG_TAG = "Location_New";
    AutoCompleteTextView autoCompleteTextView_from_loc;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private GoogleApiClient mGoogleApiClient;
    private GoogleApiClient mGoogleApiClient1;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));


    //for map

    private GoogleMap mMap;
    double mLatitude = 0;
    double mLongitude = 0;
    ImageView our_gps_loc;
    private LatLng mCenterLatLong;
    GPSTracker gps;
    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    TextView place_txt;

    String lat = "", lng = "";


    // for gps
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private final long INTERVAL = 1000 * 10;//10000
    private final long FASTEST_INTERVAL = 1000 * 5;
    String TAG = "map_data";
    SupportMapFragment mapFragment;

    @Override
    protected void onResume() {
        super.onResume();

    }

    ImageButton our_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_locaton);
        try {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Set Your Location");

            getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.trasparent_toolbar_bg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        gps = new GPSTracker(SetLocaton.this);

        try {
            mGoogleApiClient1 = new GoogleApiClient.Builder(SetLocaton.this).enableAutoManage(SetLocaton.this,
                    GOOGLE_API_CLIENT_ID, this)
                    .addApi(Places.GEO_DATA_API).addConnectionCallbacks(this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        autoCompleteTextView_from_loc = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_from_loc);
        place_txt = (TextView) findViewById(R.id.place_txt);
        autoCompleteTextView_from_loc.setThreshold(3);
        autoCompleteTextView_from_loc.setOnItemClickListener(mAutocompleteClickListenerSource);
        mPlaceArrayAdapter = new PlaceArrayAdapter(SetLocaton.this, android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        autoCompleteTextView_from_loc.setAdapter(mPlaceArrayAdapter);


        try {
            mapFragment = (SupportMapFragment) this.getSupportFragmentManager()
                    .findFragmentById(R.id.map2);
            mapFragment.getMapAsync(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        our_gps_loc = (ImageView) findViewById(R.id.our_gps_loc);
        our_location = (ImageButton) findViewById(R.id.our_location);
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 11, this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        buildLocationSettingsRequest();
        checkLocationSettings();


        our_gps_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SharedPreferences sh = getSharedPreferences("location_deatils", MODE_PRIVATE);
                sh.edit().putString("lat", mLatitude + "")
                        .putString("lng", mLongitude + "")
                        .commit();

                startActivity(new Intent(SetLocaton.this, ShopsNearMe.class));
            }
        });


        our_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    gps = new GPSTracker(SetLocaton.this);
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                        LatLng loc1 = new LatLng(latitude, longitude);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mResultReceiver = new AddressResultReceiver(new Handler());

    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerSource = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            String curlocation = String.valueOf(item.description);
            Log.i("Desc", "Selected: " + item.description);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient1, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackSource);
            Log.i("Place Id", "Fetching details for ID: " + item.placeId);
            String src1 = curlocation;





           /* Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("SRC",src);*/

        }

    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackSource = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e("Palce_Query", "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

            String locnm = Html.fromHtml(place.getAddress() + "") + "";
            Log.e("locnm", locnm);
            String mlatlng = Html.fromHtml(place.getLatLng() + "") + "";//mLatLngTextView.getText().toString();

            String[] parts = mlatlng.split(":");
            String part1 = parts[0];
            String part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            String[] s3 = s1.split("\\(");
            String srclat = s3[1];

            String[] s4 = s2.split("\\)");
            String srclng = s4[0];

            //  Toast.makeText(getActivity(), srclat + "," + srclng, Toast.LENGTH_LONG).show();


        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here
                onBackPressed();
                finish();

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient1);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(this,
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)

            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(this);
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
      /*  try {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }catch (Exception ex1){
            ex1.printStackTrace();
        }*/

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        // mMap.setMyLocationEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        gps = new GPSTracker(SetLocaton.this);

        if (gps.canGetLocation()) {

            mLatitude = gps.getLatitude();
            mLongitude = gps.getLongitude();

            // \n is for new line
        }

        updateMarkOnMap();

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                mCenterLatLong = cameraPosition.target;
                updateMovingMarker(mCenterLatLong);
            }
        });
    }

    public void updateMarkOnMap() {
        LatLng loc1 = new LatLng(mLatitude, mLongitude);
        //   Marker m = mMap.addMarker(new MarkerOptions().position(loc1).title(note));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(loc1));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
        //  mLocationMarkerText.setText(note);
        //  m.showInfoWindow();

        Location mLocation = new Location("");
        mLocation.setLatitude(loc1.latitude);
        mLocation.setLongitude(loc1.longitude);

        lat = loc1.latitude + "";
        lng = loc1.longitude + "";
        startIntentService(mLocation);
        mResultReceiver = new AddressResultReceiver(new Handler());


    }

    public void updateMovingMarker(LatLng l1) {
        Location mLocation = new Location("");
        mLocation.setLatitude(l1.latitude);
        mLocation.setLongitude(l1.longitude);

        lat = l1.latitude + "";
        lng = l1.longitude + "";
        startIntentService(mLocation);
        mResultReceiver = new AddressResultReceiver(new Handler());
    }

    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(SetLocaton.this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    try {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            this.recreate();
                        } else {
                            final Intent intent = getIntent();
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        }

                        mapFragment.getMapAsync(this);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    checkLocationSettings();
                    break;
            }
        }
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        //                final LocationSettingsStates state = result.getStatus().ge
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can initialize location
                startLocationUpdates();
                // Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();

                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                //Toast.makeText(getApplicationContext(), "RESOLUTION_REQUIRED", Toast.LENGTH_SHORT).show();

                try {
                    status.startResolutionForResult(SetLocaton.this, 101);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                //   Toast.makeText(getApplicationContext(), "SETTINGS_CHANGE_UNAVAILABLE", Toast.LENGTH_SHORT).show();

                break;
        }
    }


    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected void startLocationUpdates() {
       /* if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/

        if (ActivityCompat.checkSelfPermission((Activity) SetLocaton.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) SetLocaton.this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            // pendingResult.

            Log.d(TAG, "Location update started ..............: ");
        }


    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);

            String a = mAddressOutput.replace("\n", ",");
          /*  locname = a;
            Log.e("ADDRESS", a);
            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);*/


            place_txt.setText(a);
            //  mLocationMarkerText.setText(mCityOutput + " " + mStateOutput);

            // mLocationMarkerText.setText(a);


            // displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));


            }


        }

    }

}
