package com.emate.grocerystore;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class product extends AppCompatActivity  {
String pro_det_id;
    ProgressDialog pDialog;
    String pro_id;
    String position;
    GridView gridView;
    ArrayList<String> productList = new ArrayList<>();
    ArrayList<String> fdId = new ArrayList<>();
    ArrayList<String> food_image = new ArrayList<>();
    ArrayList<String> price = new ArrayList<>();
    ArrayList<String> d_price = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Bundle   psn = getIntent().getExtras();
        pro_det_id = getIntent().getStringExtra("position");

        gridView= (GridView) findViewById(R.id.productList);

        String tag_json_arry = "json_array_req";

        String url = Config.api_bsase_url+"sub_category.php";

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();


        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("hii", response.toString());

                pDialog.dismiss();

                try {

                    JSONObject jsonObject=new JSONObject(response);

                    JSONObject obj = new JSONObject(jsonObject.toString());

                    JSONArray js=obj.getJSONArray("product_result");

                    //   JSONArray arr = obj.getJSONArray("value");

                    //  JSONArray js = new JSONArray(response);// new JSONArray(response.getJSONObject("result"));//response.getJSONObject("result");
                    for (int i = 0; i < js.length(); i++) {
                        String id = js.getJSONObject(i).getString("id");
                        fdId.add(id);
                        String item = js.getJSONObject(i).getString("name");
                        productList.add(item);
                        String pp = js.getJSONObject(i).getString("price");
                        price.add(pp);
                        String dpp = js.getJSONObject(i).getString("dprice");
                        d_price.add(dpp);
                        String image = js.getJSONObject(i).getString("image");
                        food_image.add(image);
                    }
                  //  gridView.setAdapter(new product.myProduct(product.this, productList));
                    gridView.setAdapter(new myProduct(product.this,productList,food_image,price,d_price));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        })
        {

            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                //      indexValue = psn.getString("position");

                params.put("pro_det_id", pro_det_id);
                return params;
            }


        };
        AppController.getInstance().addToRequestQueue(sr);


    }

//    @Override
//    public void onClick(View view) {
//        Intent intent = new Intent(product.this, product_details.class);
//
//        if (view.getId()==R.id.product_img){
//            pro_id=fdId.get(0).toString();
//            intent.putExtra("position",pro_id );
//        }
//        startActivity(intent);
//    }

    public class myProduct extends BaseAdapter {
        Context context;

        ArrayList<String> list;
        ArrayList<String> price_list;
        ArrayList<String> disprice_list;
        ArrayList<String> img_list;

      public   myProduct(Context context, ArrayList<String> list, ArrayList<String> img_list, ArrayList<String> price_list, ArrayList<String> disprice_list ) {
            this.context = context;
            this.list = list;// new ArrayList<Item>();
            this.img_list=img_list;
            this.price_list =price_list ;// new ArrayList<Item>();
            this.disprice_list=disprice_list;
        }




        @Override
        public int getCount() {
            return list.size();
          //  return price_list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        class ViewHolder {

            TextView offer_hint,disPrice,price;
            ImageView product_img;

            ViewHolder(View v) {
                offer_hint = (TextView) v.findViewById(R.id.offer_hint);
                disPrice = (TextView) v.findViewById(R.id.disPrice);
                price = (TextView) v.findViewById(R.id.price);
                product_img= (ImageView) v.findViewById(R.id.product_img);
            }
        }


        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View row = view;

            product.myProduct.ViewHolder holder = null;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.product_singlerow, viewGroup, false);
                holder = new product.myProduct.ViewHolder(row);
                holder.offer_hint = (TextView) row.findViewById(R.id.offer_hint);
                holder.price= (TextView) row.findViewById(R.id.price);
                holder.disPrice= (TextView) row.findViewById(R.id.disPrice);
                holder.product_img= (ImageView) row.findViewById(R.id.product_img);

                String i1 = list.get(i);
                String p = price_list.get(i);
                String dp = disprice_list.get(i);
                String img = img_list.get(i);

                try {
                    holder.offer_hint.setText(i1);
                    holder.disPrice.setText(dp);
                    holder.price.setText(p);
                  //  holder.product_img.setImageResource(img);
                    Picasso.with(context).load(Config.img_base_url+img).placeholder(R.drawable.store).into(holder.product_img);

                    //  holder.imageView1.setImageResource(i1.imageId);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                final product.myProduct.ViewHolder finalHolder = holder;
                holder.product_img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                     //   String title = finalHolder.product_img.getText().toString();
                       /* if (i == 0) {*/
                            position = fdId.get(i).toString();
                            Intent intent = new Intent(context, product_details.class);
                            intent.putExtra("position", position);
                            startActivity(intent);

                        //}//

                    }
                });
            }
                return row;
        }
    }

}
