package com.emate.grocerystore;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.emate.grocerystore.develop.activities.db.DatabaseHandler;
import com.emate.grocerystore.develop.activities.models.Cart;
import com.emate.grocerystore.develop.activities.models.ProductModel;
import com.emate.grocerystore.develop.activities.models.TypefaceUtil;
import com.emate.grocerystore.develop.activities.models.UnitModel;
import com.emate.grocerystore.develop.activities.models.Users;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public static String FROM = "FROM";
    //public static SweetAlertDialog sweetAlertDialog;

    private static AppController mInstance;
    private static final RetryPolicy VOLLEY_REQUEST_RETRY_POLICY = new DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 10,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
    public static SweetAlertDialog sweetAlertDialog;

    public static String Api_Url = "http://192.168.0.109/grossary/mobile_app/webservice/";//http://grocery.etkininfotech.com/grocery/mobile_app/webservice/";
    public static String api_bsase_url = "http://192.168.0.109/groceryShop_webServices/";
    public static String img_base_url = "http://192.168.0.109/";

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mInstance = this;
        //   sweetAlertDialog = new SweetAlertDialog(this);

        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Rupee_Foradian.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf

    }


    public static double roundStr(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    public static void successDialog(String alert_text, Context ctx) {
        sweetAlertDialog = new SweetAlertDialog((Activity) ctx);
        sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText("\n" + alert_text)
                .setContentText("\n")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
        sweetAlertDialog.show();


    }

    public static float retFlot(String str) {
        float v = 0;

        try {
            if (str.equalsIgnoreCase("null") || str.equalsIgnoreCase("")) {

            } else {
                v = Float.parseFloat(str);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return v;
    }

    public static String retStrr(String b) {

        String a = "-";
        if (b.equalsIgnoreCase("null") || b.equalsIgnoreCase("")) {
            a = "-";
        } else {
            a = b;
        }
        return a;

    }


    public static String getInitalsStr(String str) {
        String[] parts = str.split(" ");
        String initialsStr = "";
        for (int i = 0; i < parts.length; i++) {
            initialsStr = initialsStr + parts[i].charAt(0);
        }

        return initialsStr;
    }


    public static String getDay(long timstamp) {

        String dt = "";
        try {

            Date date = new Date(timstamp);
            String d1 = new SimpleDateFormat("MMM-dd/yyyy hh:mm").format(date);

            dt = d1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        // return dt;

        String day = (new SimpleDateFormat("EEEE")).format(new Timestamp(timstamp).getTime()).toString();
        return day + ", " + dt;

    }

    public static int getRandomColorCode() {
        Random rand = new Random();
        int r = rand.nextInt(255);
        int g = rand.nextInt(255);
        int b = rand.nextInt(255);
        int randomColor = Color.rgb(r, g, b);

        return randomColor;
    }

    public static void retFromDiscount(Context ctx, String item) {
     /*   List<String> disList = Arrays.asList(ctx.getResources().getStringArray(R.array.discount_array));
          <item>Upto 5%</item>
        <item>5% - 10%   </item>
        <item>10% - 15%   </item>
        <item>15% - 20%   </item>
        <item>20% - 25%    </item>
        <item>More than 25%</item>

                for(int i)
        if(disList.contains(item)){

        }

    }

    public int getPos(List<String> disList , String item){*/
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public static void setData(TextView tv, String str) {
        try {
            if (str == null || str.equalsIgnoreCase("null")) {
                tv.setText("-");
            } else {
                tv.setText(" " + str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void retShowAlertDialod(String alert_text, Context ctx) {
        sweetAlertDialog = new SweetAlertDialog((Activity) ctx);
        sweetAlertDialog.changeAlertType(SweetAlertDialog.WARNING_TYPE);
        sweetAlertDialog.setTitleText("\n" + alert_text)
                .setContentText("\n")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                });
        sweetAlertDialog.show();


    }

  /*  public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }



    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
*/

    /**
     * @return The Volley Request queue, the queue will be created if it is null
     */
    public RequestQueue getRequestQueue() {
        // lazy initialize the request queue, the queue instance will be
        // created when it is accessed for the first time
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new OkHttpStack());

        }

        return mRequestQueue;
    }

    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);

    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);
        req.setRetryPolicy(VOLLEY_REQUEST_RETRY_POLICY);
        getRequestQueue().add(req);
    }


    public static List<Cart> cartListfromJson(JSONArray jsonArray) throws JSONException {
        List<Cart> cartList = new ArrayList<>();
        DatabaseHandler databaseHandler = new DatabaseHandler(AppController.getInstance());
        List<Cart> carts = databaseHandler.getAllCarts();
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                //  Cart cart = new Cart();
                Users users = new Users();
                ProductModel pm = new ProductModel();


                JSONObject jsonObject = jsonArray.getJSONObject(i);
               /* cart.setOndate(jsonObject.getString("ondate"));
                cart.setCartid(jsonObject.getString("cartid"));
                cart.setChng_addrs(jsonObject.getString("chng_addrs"));
                cart.setDelive_mob_no(jsonObject.getString("delive_mob_no"));
                cart.setProduct_id(jsonObject.getString("product_id"));
                cart.setUser_id(jsonObject.getString("user_id"));
                cart.setDeliver_to(jsonObject.getString("deliver_to"));*/
                // cart.setmInteger(jsonObject.getInt("prod_qty"));
             /*   users.setAddress(jsonObject.getString("address"));
                users.setName(jsonObject.getString("Name"));
                users.setUid(jsonObject.getString("uid"));
                users.setUsername(jsonObject.getString("username"));*/


                Cart cart = getCart(carts, jsonObject.getInt("unit_id"));

                //   cart.setmInteger(1);

                pm.setContent(jsonObject.getString("content"));
                pm.setId(jsonObject.getString("id"));
                pm.setStatus(jsonObject.getString("status"));
                pm.setSubcat_id(jsonObject.getString("subcat_id"));

                pm.setAvailable(jsonObject.getString("available"));
                pm.setDet_cat_id(jsonObject.getString("det_cat_id"));
                pm.setBrand_id(jsonObject.getString("brand_id"));
                pm.setTitle(jsonObject.getString("title"));
                pm.setSeller_name(jsonObject.getString("seller_name"));
                pm.setImage1(jsonObject.getString("image1"));
                pm.setImage2(jsonObject.getString("image2"));
                pm.setImage3(jsonObject.getString("image3"));
                pm.setImage4(jsonObject.getString("image4"));
                pm.setImage5(jsonObject.getString("image5"));
                pm.setSeller(jsonObject.getString("seller"));
                pm.setShipping(jsonObject.getString("shipping"));
                pm.setCgst(jsonObject.getString("cgst"));
                pm.setSgst(jsonObject.getString("sgst"));
                pm.setSeller_id(jsonObject.getString("seller_id"));
                UnitModel um = new UnitModel();
                String unit_id = jsonObject.getString("unit_id");
                String quantity = jsonObject.getString("quantity");
                String dprice = jsonObject.getString("price");
                String mrp = jsonObject.getString("mrp");
                String unit = jsonObject.getString("unit");
                String discount = jsonObject.getString("dis");

                um.setId(unit_id);
                um.setQuantity(quantity);
                um.setDis(discount);
                um.setMrp(mrp);
                um.setPrice(dprice);
                um.setUnit(unit);

                pm.setUnitModel(um);


                cart.setPmData(pm);
                cart.setUsersData(users);

                cartList.add(cart);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cartList;
    }

    public static String retDate(String tstamp) {
        String dt = "";
        try {
            long time_st = Long.parseLong(tstamp);
            Date date = new Date(time_st);
            String d1 = new SimpleDateFormat("dd-MM-yyyy").format(date);

            dt = d1;

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return dt;
    }


    public static Cart getCart(List<Cart> cartList, int unitId) {
        Cart c = new Cart();
        for (int i = 0; i < cartList.size(); ) {
            if (cartList.get(i).getUnit_id() == unitId) {
                c.setQty(cartList.get(i).getQty());
                c.setOndate(cartList.get(i).getOndate());
                c.setProduct_id(cartList.get(i).getProduct_id());
                c.setCartid(cartList.get(i).getCartid());
                c.setUnit_id(cartList.get(i).getUnit_id());
                i = cartList.size();

            } else {
                i++;
            }
        }
        return c;
    }

}

