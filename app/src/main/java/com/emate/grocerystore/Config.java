package com.emate.grocerystore;


import android.graphics.Paint;
import android.widget.TextView;

import com.emate.grocerystore.develop.activities.models.SubCategory;
import com.emate.grocerystore.develop.activities.models.SubCategoryDeatils;

import java.util.List;

/**
 * Created by Ashvini on 11/7/2017.
 */

public class Config {
    //http://jbims-prayaag.com/grocery
    public static String Api_Url = "http://192.168.0.110/grossary/mobile_app/webservice/";//http://grocery.etkininfotech.com/grocery/mobile_app/webservice/";
    public static String api_bsase_url = "http://192.168.0.110/groceryShop_webServices/";
    public static String img_base_url = "http://192.168.0.110/";
    public static String REFRESHTOKEN = "REFRESHTOKEN";
    public static String TOKEN = "TOKEN";

    public static String MERCHANT = "MERCHANT";
    public static String USER = "USER";


    public static String EMail_Url = "http://192.168.0.110/grossary/";
 /*   public static String Api_Url = "http://jbims-prayaag.com/grocery/mobile_app/webservice/";//http://grocery.etkininfotech.com/grocery/mobile_app/webservice/";
    public static String api_bsase_url = "http://jbims-prayaag.com/grocery/groceryShop_webServices/";
    public static String img_base_url = "http://jbims-prayaag.com/grocery";*/


    /*public static String Api_Url = "http://192.168.43.233:8099/grossary/mobile_app/webservice/";//http://grocery.etkininfotech.com/grocery/mobile_app/webservice/";
    public static String api_bsase_url = "http://192.168.43.233:8099/groceryShop_webServices/";
    public static String img_base_url = "http://192.168.43.233:8099/";*/
    public static String img_path = "http://grocery.etkininfotech.com/webpanel/images/products/";

    public static String FROM = "From";
    public static String DATA = "DATA";
    public static String MERCHANT_ID = "merchant_id";

    public static boolean cotainCat(List<SubCategory> categoryList, String catId) {
        boolean a = false;
        for (int n = 0; n < categoryList.size(); n++) {

            if (categoryList.get(n).getId().equals(catId)) {
                a = true;
                n = categoryList.size();

            }
        }


        return a;
    }

    public static void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static float getPrice(float discnt_per, float main_p) {

        float disPrice = main_p * (discnt_per / 100);

        disPrice = main_p - disPrice;
        return disPrice;
    }


    public static boolean cotainSubCat(List<SubCategoryDeatils> categoryList, String sub_cat_id) {
        boolean a = false;
        for (int n = 0; n < categoryList.size(); n++) {

            if (categoryList.get(n).getSub_cat_id().equals(sub_cat_id)) {
                a = true;
                n = categoryList.size();

            }
        }


        return a;
    }
}
