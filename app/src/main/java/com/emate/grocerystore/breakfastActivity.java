package com.emate.grocerystore;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class breakfastActivity extends AppCompatActivity {

    ArrayList<String> breakfast_list=new ArrayList<>();
    ListView listView;
    String indexValue;
    String foodCategory;
    Bundle psn;
    ProgressDialog pDialog;
    ArrayList<String> sub_details=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breakfast);

        listView= (ListView) findViewById(R.id.breakfast);
      //  breakfast_list=getIntent().getStringArrayListExtra("breakfast");
        listView.setAdapter(new breakfastActivity.breakfastCategory(breakfastActivity.this, sub_details));



        Bundle   psn = getIntent().getExtras();



        // Intent  intent=getIntent();
        indexValue = getIntent().getStringExtra("position");
        foodCategory = getIntent().getStringExtra("foodCategory");

        String tag_json_arry = "json_array_req";

        String url = Config.api_bsase_url+"sub_category.php";

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();




        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("hii", response.toString());

                pDialog.dismiss();

                try {

                    JSONObject jsonObject=new JSONObject(response);

                    JSONObject obj = new JSONObject(jsonObject.toString());


                    JSONArray js=obj.getJSONArray("details_cat_result");

                    //   JSONArray arr = obj.getJSONArray("value");

                    //  JSONArray js = new JSONArray(response);// new JSONArray(response.getJSONObject("result"));//response.getJSONObject("result");
                    for (int i = 0; i < js.length(); i++) {

                        String food = js.getJSONObject(i).getString("details_cat");
                        sub_details.add(food);
                    }
                    listView.setAdapter(new breakfastActivity.breakfastCategory(breakfastActivity.this, sub_details));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        })
        {

            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

                //      indexValue = psn.getString("position");

                params.put("indexValue", indexValue);
                params.put("foodCategory", foodCategory);
                return params;
            }



        };
        AppController.getInstance().addToRequestQueue(sr);

    }

    public class breakfastCategory extends BaseAdapter{
        Context context;
        ArrayList<String> list;
        breakfastCategory(Context context, ArrayList<String> list) {
            this.context = context;
            this.list = list;// new ArrayList<Item>();
        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        class ViewHolder {

            TextView textView;
            ImageView img;

            ViewHolder(View v) {
                textView = (TextView) v.findViewById(R.id.food_row);
                img = (ImageView) v.findViewById(R.id.imageFood);

            }
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View row = view;

            breakfastActivity.breakfastCategory.ViewHolder holder = null;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.foodcategory_singlerow, viewGroup, false);
                holder = new breakfastActivity.breakfastCategory.ViewHolder(row);
                holder.textView = (TextView) row.findViewById(R.id.food_row);
                holder.img = (ImageView) row.findViewById(R.id.imageFood);


                String i1 = list.get(i);

                try {
                    holder.textView.setText(i1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                final breakfastActivity.breakfastCategory.ViewHolder finalHolder = holder;
                holder.textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String title = finalHolder.textView.getText().toString();
                        if (i==0) {
//                            position="1";
//                            Intent intent = new Intent(context, breakfastActivity.class);
//                            intent.putExtra("position", position);
//                            startActivity(intent);


                        }
                    }
                });


            }
            return row;
        }
    }
}
