package com.emate.grocerystore;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    ProgressDialog pDialog;
            String position;
            String food_position;
            String personal_position;
            String home_position;
            String health_position;

    ListView listView;
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> idList = new ArrayList<>();
    ArrayList<String> offer_id = new ArrayList<>();
    ArrayList<Integer> food_id = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();
    ArrayList<String> bestOffer = new ArrayList<>();
    ArrayList<String> best_ofr_img = new ArrayList<>();
    ArrayList<String> personal_id = new ArrayList<>();
    ArrayList<String> perCare_img = new ArrayList<>();
    ArrayList<String> home_id = new ArrayList<>();
    ArrayList<String> home_img = new ArrayList<>();
    ArrayList<String> health_id = new ArrayList<>();
    ArrayList<String> health_img = new ArrayList<>();

    ImageView foodProduct1,foodProduct2,foodProduct3,foodProduct4,foodProduct5, personalCare1,personalCare2,personalCare3,personalCare4,
            personalCare5,homeCare1,homeCare2,homeCare3,homeCare4,homeCare5,healthCare1,healthCare2,healthCare3,healthCare4,healthCare5;
    private static ViewPager mPager;
    private static int currentPage = 0;
 //  private static final Integer[] XMEN = new Integer[0];
  // private ArrayList<Integer> XMENArray = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        foodProduct1= (ImageView) findViewById(R.id.foodProduct1);
        foodProduct2= (ImageView) findViewById(R.id.foodProduct2);
        foodProduct3= (ImageView) findViewById(R.id.foodProduct3);
        foodProduct4= (ImageView) findViewById(R.id.foodProduct4);
        foodProduct5= (ImageView) findViewById(R.id.foodProduct5);
        foodProduct1.setOnClickListener(this);
        foodProduct2.setOnClickListener(this);
        foodProduct3.setOnClickListener(this);
        foodProduct4.setOnClickListener(this);
        foodProduct5.setOnClickListener(this);

        personalCare1= (ImageView) findViewById(R.id.personalCare1);
        personalCare2= (ImageView) findViewById(R.id.personalCare2);
        personalCare3= (ImageView) findViewById(R.id.personalCare3);
        personalCare4= (ImageView) findViewById(R.id.personalCare4);
        personalCare5= (ImageView) findViewById(R.id.personalCare5);
        personalCare1.setOnClickListener(this);
        personalCare2.setOnClickListener(this);
        personalCare3.setOnClickListener(this);
        personalCare4.setOnClickListener(this);
        personalCare5.setOnClickListener(this);


        homeCare1= (ImageView) findViewById(R.id.homeCare1);
        homeCare2= (ImageView) findViewById(R.id.homeCare2);
        homeCare3= (ImageView) findViewById(R.id.homeCare3);
        homeCare4= (ImageView) findViewById(R.id.homeCare4);
        homeCare5= (ImageView) findViewById(R.id.homeCare5);
        homeCare1.setOnClickListener(this);
        homeCare2.setOnClickListener(this);
        homeCare3.setOnClickListener(this);
        homeCare4.setOnClickListener(this);
        homeCare5.setOnClickListener(this);



        healthCare1= (ImageView) findViewById(R.id.healthCare1);
        healthCare2= (ImageView) findViewById(R.id.healthCare2);
        healthCare3= (ImageView) findViewById(R.id.healthCare3);
        healthCare4= (ImageView) findViewById(R.id.healthCare4);
        healthCare5= (ImageView) findViewById(R.id.healthCare5);
        healthCare1.setOnClickListener(this);
        healthCare2.setOnClickListener(this);
        healthCare3.setOnClickListener(this);
        healthCare4.setOnClickListener(this);
        healthCare5.setOnClickListener(this);


        listView = (ListView) findViewById(R.id.category);

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//             String  item= list.tit;
//
//
//            }
//        });

        String tag_json_arry = "json_array_req";

        String url = Config.api_bsase_url+"category_list.php";

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();


        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("hii", response.toString());

                        pDialog.dismiss();

                        try {

                            JSONArray js = response.getJSONArray("category_result");
                            JSONArray ja = response.getJSONArray("offer_result");
                            JSONArray jm = response.getJSONArray("bestoffer_result");
                            JSONArray jp = response.getJSONArray("personalcare_result");
                            JSONArray jhm = response.getJSONArray("home_result");
                            JSONArray jhel = response.getJSONArray("health_result");

                            for (int i = 0; i < js.length(); i++) {
                                String id = js.getJSONObject(i).getString("id");
                                idList.add(id);
                                String cat_type = js.getJSONObject(i).getString("category_type");
                                list.add(cat_type);

                            }
                            listView.setAdapter(new ShopMenu(MainActivity.this, list));

                            for (int i = 0; i < ja.length(); i++) {
                                String id = ja.getJSONObject(i).getString("id");
                                offer_id.add(id);
                                String image = ja.getJSONObject(i).getString("image");
                                images.add(image);

                            }
                            init();

                            for (int i = 0; i < jm.length(); i++) {
                                String det_cat_id = jm.getJSONObject(i).getString("det_cat_id");
                                bestOffer.add(det_cat_id);
                                String image = jm.getJSONObject(i).getString("image");
                                best_ofr_img.add(image);

                            }
                            foodProduct();

                            for (int i = 0; i < jp.length(); i++) {
                                String id = jp.getJSONObject(i).getString("id");
                                personal_id.add(id);
                                String image = jp.getJSONObject(i).getString("image");
                                perCare_img.add(image);

                            }
                            personalCare();

                            for (int i = 0; i < jhm.length(); i++) {
                                String id = jhm.getJSONObject(i).getString("id");
                                home_id.add(id);
                                String image = jhm.getJSONObject(i).getString("image");
                                home_img.add(image);

                            }
                            homeCare();

                            for (int i = 0; i < jhel.length(); i++) {
                                String id = jhel.getJSONObject(i).getString("id");
                                health_id.add(id);
                                String image = jhel.getJSONObject(i).getString("image");
                                health_img.add(image);

                            }
                            healthCare();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                VolleyLog.d("byee", "Error: " + error.getMessage());
                pDialog.dismiss();
            }
        });

        AppController.getInstance().addToRequestQueue(req, tag_json_arry);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // navigationView.setNavigationItemSelectedListener(this);
    }

    private void healthCare() {

        int img=health_img.size();
        switch (img){
            case 1:
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(0)).placeholder(R.drawable.store).into(healthCare1);

                //  Picasso.with(MainActivity.this).load(Uri.parse(best_ofr_img.get(0).toString())).into(foodProduct1);
                healthCare1.setVisibility(View.VISIBLE);
                healthCare2.setVisibility(View.GONE);
                healthCare3.setVisibility(View.GONE);
                healthCare4.setVisibility(View.GONE);
                healthCare5.setVisibility(View.GONE);
                break;
            case 2:
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(0)).placeholder(R.drawable.store).into(healthCare1);
                healthCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(1)).placeholder(R.drawable.store).into(healthCare2);
                healthCare2.setVisibility(View.VISIBLE);
                healthCare3.setVisibility(View.GONE);
                healthCare4.setVisibility(View.GONE);
                healthCare5.setVisibility(View.GONE);
                break;
            case 3:
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(0)).placeholder(R.drawable.store).into(healthCare1);
                healthCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(1)).placeholder(R.drawable.store).into(healthCare2);
                healthCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(2)).placeholder(R.drawable.store).into(healthCare3);
                healthCare3.setVisibility(View.VISIBLE);
                healthCare4.setVisibility(View.GONE);
                healthCare5.setVisibility(View.GONE);
                break;
            case 4:
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(0)).placeholder(R.drawable.store).into(healthCare1);
                healthCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(1)).placeholder(R.drawable.store).into(healthCare2);
                healthCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(2)).placeholder(R.drawable.store).into(healthCare3);
                healthCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(3)).placeholder(R.drawable.store).into(healthCare4);
                healthCare4.setVisibility(View.VISIBLE);
                healthCare5.setVisibility(View.GONE);
                break;
            case 5:
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(0)).placeholder(R.drawable.store).into(healthCare1);
                healthCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(1)).placeholder(R.drawable.store).into(healthCare2);
                healthCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(2)).placeholder(R.drawable.store).into(healthCare3);
                healthCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(3)).placeholder(R.drawable.store).into(healthCare4);
                healthCare4.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+health_img.get(4)).placeholder(R.drawable.store).into(healthCare5);
                healthCare5.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void homeCare() {
        int img=home_img.size();
        switch (img){
            case 1:
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(0)).placeholder(R.drawable.store).into(homeCare1);

                //  Picasso.with(MainActivity.this).load(Uri.parse(best_ofr_img.get(0).toString())).into(foodProduct1);
                homeCare1.setVisibility(View.VISIBLE);
                homeCare2.setVisibility(View.GONE);
                homeCare3.setVisibility(View.GONE);
                homeCare4.setVisibility(View.GONE);
                homeCare5.setVisibility(View.GONE);
                break;
            case 2:
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(0)).placeholder(R.drawable.store).into(homeCare1);
                homeCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(1)).placeholder(R.drawable.store).into(homeCare2);
                homeCare2.setVisibility(View.VISIBLE);
                homeCare3.setVisibility(View.GONE);
                homeCare4.setVisibility(View.GONE);
                homeCare5.setVisibility(View.GONE);
                break;
            case 3:
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(0)).placeholder(R.drawable.store).into(homeCare1);
                homeCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(1)).placeholder(R.drawable.store).into(homeCare2);
                homeCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(2)).placeholder(R.drawable.store).into(homeCare3);
                homeCare3.setVisibility(View.VISIBLE);
                homeCare4.setVisibility(View.GONE);
                homeCare5.setVisibility(View.GONE);
                break;
            case 4:
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(0)).placeholder(R.drawable.store).into(homeCare1);
                homeCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(1)).placeholder(R.drawable.store).into(homeCare2);
                homeCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(2)).placeholder(R.drawable.store).into(homeCare3);
                homeCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(3)).placeholder(R.drawable.store).into(homeCare4);
                homeCare4.setVisibility(View.VISIBLE);
                homeCare5.setVisibility(View.GONE);
                break;
            case 5:
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(0)).placeholder(R.drawable.store).into(homeCare1);
                homeCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(1)).placeholder(R.drawable.store).into(homeCare2);
                homeCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(2)).placeholder(R.drawable.store).into(homeCare3);
                homeCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(3)).placeholder(R.drawable.store).into(homeCare4);
                homeCare4.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+home_img.get(4)).placeholder(R.drawable.store).into(homeCare5);
                homeCare5.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void personalCare() {

        int img=perCare_img.size();
        switch (img){
            case 1:
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(0)).placeholder(R.drawable.store).into(personalCare1);

                //  Picasso.with(MainActivity.this).load(Uri.parse(best_ofr_img.get(0).toString())).into(foodProduct1);
                personalCare1.setVisibility(View.VISIBLE);
                personalCare2.setVisibility(View.GONE);
                personalCare3.setVisibility(View.GONE);
                personalCare4.setVisibility(View.GONE);
                personalCare5.setVisibility(View.GONE);
                break;
            case 2:
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(0)).placeholder(R.drawable.store).into(personalCare1);
                personalCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(1)).placeholder(R.drawable.store).into(personalCare2);
                personalCare2.setVisibility(View.VISIBLE);
                personalCare3.setVisibility(View.GONE);
                personalCare4.setVisibility(View.GONE);
                personalCare5.setVisibility(View.GONE);
                break;
            case 3:
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(0)).placeholder(R.drawable.store).into(personalCare1);
                personalCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(1)).placeholder(R.drawable.store).into(personalCare2);
                personalCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(2)).placeholder(R.drawable.store).into(personalCare3);
                personalCare3.setVisibility(View.VISIBLE);
                personalCare4.setVisibility(View.GONE);
                personalCare5.setVisibility(View.GONE);
                break;
            case 4:
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(0)).placeholder(R.drawable.store).into(personalCare1);
                personalCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(1)).placeholder(R.drawable.store).into(personalCare2);
                personalCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(2)).placeholder(R.drawable.store).into(personalCare3);
                personalCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(3)).placeholder(R.drawable.store).into(personalCare4);
                personalCare4.setVisibility(View.VISIBLE);
                personalCare5.setVisibility(View.GONE);
                break;
            case 5:
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(0)).placeholder(R.drawable.store).into(personalCare1);
                personalCare1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(1)).placeholder(R.drawable.store).into(personalCare2);
                personalCare2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(2)).placeholder(R.drawable.store).into(personalCare3);
                personalCare3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(3)).placeholder(R.drawable.store).into(personalCare4);
                personalCare4.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+perCare_img.get(4)).placeholder(R.drawable.store).into(personalCare5);
                personalCare5.setVisibility(View.VISIBLE);
                break;
        }


    }

    private void foodProduct() {
      int img=best_ofr_img.size();
        switch (img){
            case 1:
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(0)).placeholder(R.drawable.store).into(foodProduct1);

              //  Picasso.with(MainActivity.this).load(Uri.parse(best_ofr_img.get(0).toString())).into(foodProduct1);
                foodProduct1.setVisibility(View.VISIBLE);

                foodProduct2.setVisibility(View.GONE);
                foodProduct3.setVisibility(View.GONE);
                foodProduct4.setVisibility(View.GONE);
                foodProduct5.setVisibility(View.GONE);
                break;
            case 2:
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(0)).placeholder(R.drawable.store).into(foodProduct1);
                foodProduct1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(1)).placeholder(R.drawable.store).into(foodProduct2);
                foodProduct2.setVisibility(View.VISIBLE);
                foodProduct5.setVisibility(View.GONE);
                foodProduct3.setVisibility(View.GONE);
                foodProduct4.setVisibility(View.GONE);
                break;
            case 3:
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(0)).placeholder(R.drawable.store).into(foodProduct1);
                foodProduct1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(1)).placeholder(R.drawable.store).into(foodProduct2);
                foodProduct2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(2)).placeholder(R.drawable.store).into(foodProduct3);
                foodProduct3.setVisibility(View.VISIBLE);
                foodProduct5.setVisibility(View.GONE);
                foodProduct4.setVisibility(View.GONE);
                break;
            case 4:
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(0)).placeholder(R.drawable.store).into(foodProduct1);
                foodProduct1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(1)).placeholder(R.drawable.store).into(foodProduct2);
                foodProduct2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(2)).placeholder(R.drawable.store).into(foodProduct3);
                foodProduct3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(3)).placeholder(R.drawable.store).into(foodProduct4);
                foodProduct4.setVisibility(View.VISIBLE);
                foodProduct5.setVisibility(View.GONE);
                break;
            case 5:
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(0)).placeholder(R.drawable.store).into(foodProduct1);
                foodProduct1.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(1)).placeholder(R.drawable.store).into(foodProduct2);
                foodProduct2.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(2)).placeholder(R.drawable.store).into(foodProduct3);
                foodProduct3.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(3)).placeholder(R.drawable.store).into(foodProduct4);
                foodProduct4.setVisibility(View.VISIBLE);
                Picasso.with(MainActivity.this).load(Config.img_base_url+best_ofr_img.get(4)).placeholder(R.drawable.store).into(foodProduct5);
                foodProduct5.setVisibility(View.VISIBLE);
                break;
        }

    }

    private void init() {
//        for(int i=0;i<XMEN.length;i++)
//            XMENArray.add(XMEN[i]);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new slideAdapter(MainActivity.this,images));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == images.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {

        Intent intent = new Intent(MainActivity.this, product.class);

        if (view.getId()==R.id.foodProduct1){
            food_position=bestOffer.get(0).toString();
            intent.putExtra("position",food_position );
        }
        else  if (view.getId()==R.id.foodProduct2){
            food_position=bestOffer.get(1).toString();
            intent.putExtra("position",food_position );
        }
        else  if (view.getId()==R.id.foodProduct3){
            food_position=bestOffer.get(2).toString();
            intent.putExtra("position",food_position );
        }
        else  if (view.getId()==R.id.foodProduct4){
            food_position=bestOffer.get(3).toString();
            intent.putExtra("position",food_position );
        }
        else  if (view.getId()==R.id.foodProduct5){
            food_position=bestOffer.get(4).toString();
            intent.putExtra("position",food_position );
        }
        else  if (view.getId()==R.id.personalCare1){
            personal_position=personal_id.get(0).toString();
            intent.putExtra("position",personal_position );
        }
        else  if (view.getId()==R.id.personalCare2){
            personal_position=personal_id.get(1).toString();
            intent.putExtra("position",personal_position );
        }
        else  if (view.getId()==R.id.personalCare3){
            personal_position=personal_id.get(2).toString();
            intent.putExtra("position",personal_position );
        }
        else  if (view.getId()==R.id.personalCare4){
            personal_position=personal_id.get(3).toString();
            intent.putExtra("position",personal_position );
        }
        else  if (view.getId()==R.id.personalCare5){
            personal_position=personal_id.get(4).toString();
            intent.putExtra("position",personal_position );
        }
        else  if (view.getId()==R.id.homeCare1){
            home_position=home_id.get(0).toString();
            intent.putExtra("position",home_position );
        }
        else  if (view.getId()==R.id.homeCare2){
            home_position=home_id.get(1).toString();
            intent.putExtra("position",home_position );
        }
        else  if (view.getId()==R.id.homeCare3){
            home_position=home_id.get(2).toString();
            intent.putExtra("position",home_position );
        }
        else  if (view.getId()==R.id.homeCare4){
            home_position=home_id.get(3).toString();
            intent.putExtra("position",home_position );
        }
        else  if (view.getId()==R.id.homeCare5){
            home_position=home_id.get(4).toString();
            intent.putExtra("position",home_position );
        }
        else  if (view.getId()==R.id.healthCare1){
            health_position=home_id.get(0).toString();
            intent.putExtra("position",health_position );
        }
        else  if (view.getId()==R.id.healthCare2){
            health_position=home_id.get(1).toString();
            intent.putExtra("position",health_position );
        }
        else  if (view.getId()==R.id.healthCare3){
            health_position=home_id.get(2).toString();
            intent.putExtra("position",health_position );
        }
        else  if (view.getId()==R.id.healthCare4){
            health_position=home_id.get(3).toString();
            intent.putExtra("position",health_position );
        }
        else  if (view.getId()==R.id.healthCare5){
            health_position=home_id.get(4).toString();
            intent.putExtra("position",health_position );
        }

        startActivity(intent);

    }


    public class ShopMenu extends BaseAdapter {


        Context context;


        ArrayList<String> list;

        ShopMenu(Context context, ArrayList<String> list) {
            this.context = context;
            this.list = list;// new ArrayList<Item>();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        class ViewHolder {

            TextView textView;

            ViewHolder(View v) {
                textView = (TextView) v.findViewById(R.id.single_row);
            }
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View row = view;

            ShopMenu.ViewHolder holder = null;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.activity_single_row, viewGroup, false);
                holder = new ShopMenu.ViewHolder(row);
                holder.textView = (TextView) row.findViewById(R.id.single_row);

                String i1 = list.get(i);

                try {
                    holder.textView.setText(i1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final ViewHolder finalHolder = holder;
                holder.textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        String title = finalHolder.textView.getText().toString();
                        if ( i==0) {
                            position=idList.get(0).toString();
                            Intent intent = new Intent(context, foodCategory.class);
                            intent.putExtra("position",position );
                             startActivity(intent);

                        }
                        else if(i==1)
                        {
                             position=idList.get(1).toString();
                            Intent intent = new Intent(context, foodCategory.class);
                            intent.putExtra("position",position );
                            startActivity(intent);

                        }
                        else if(i==2)
                        {
                            position=idList.get(2).toString();
                            Intent intent = new Intent(context, foodCategory.class);
                            intent.putExtra("position",position );
                            startActivity(intent);

                        }
                        else if(i==3)
                        {
                            position=idList.get(3).toString();
                            Intent intent = new Intent(context, foodCategory.class);
                            intent.putExtra("position",position );
                            startActivity(intent);

                        }

//                        if ( listView.getItemAtPosition(2)==2) {
//
//                            position="2";
//                            Intent intent = new Intent(context, foodCategory.class);
//                            intent.putStringArrayListExtra("food_list",sub_category_food );
//                            intent.putIntegerArrayListExtra("food_id", food_id);
//                            intent.putStringArrayListExtra("breakfast", breakfast);
//                            startActivity(intent);
//
//                        }
//
//                        if ( listView.getItemAtPosition(3)==3) {
//
//                            position="3";
//                            Intent intent = new Intent(context, foodCategory.class);
//                            intent.putStringArrayListExtra("food_list",sub_category_food );
//                            intent.putIntegerArrayListExtra("food_id", food_id);
//                            intent.putStringArrayListExtra("breakfast", breakfast);
//                            startActivity(intent);
//
//                        }
//
//                        if ( listView.getItemAtPosition(4)==4) {
//
//                            position="4";
//                            Intent intent = new Intent(context, foodCategory.class);
//                            intent.putStringArrayListExtra("food_list",sub_category_food );
//                            intent.putIntegerArrayListExtra("food_id", food_id);
//                            intent.putStringArrayListExtra("breakfast", breakfast);
//                            startActivity(intent);
//
//                        }
//---------------------------------------------------------------
//                        String tag_json_arry = "json_array_req";
//
//                        String url = "http://192.168.0.102:8090/groceryShop_webServices/category_list.php";
//
//
//                        JSONObject object= new JSONObject();
//                        try {
//                            object.put("position", position);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        pDialog = new ProgressDialog(context);
//                        pDialog.setMessage("Loading...");
//                        pDialog.show();
//
//                        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
//                                url,new Response.Listener<String>() {
//
//                            @Override
//                            public void onResponse(String response) {
//                                Log.d("hii", response.toString());
//                                pDialog.hide();
//
//                                 JSONArray  sub_category_result = response.getJSONArray("sub_category_result");
//                                JSONArray  det_category_result = response.getJSONArray("details_cat_result");
//
//                                for (int i = 0; i < sub_category_result.length(); i++) {
//
//                                    try {
//                                        String food = sub_category_result.getJSONObject(i).getString("category_type");
//                                        int food_img_id = sub_category_result.getJSONObject(i).getInt("id");
//                                        sub_category_food.add(food);
//                                        food_id.add(food_img_id);
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                }
//
//
//                                for (int i = 0; i < det_category_result.length(); i++) {
//
//                                    try {
//                                        String brkFst = det_category_result.getJSONObject(i).getString("details_cat");
//                                        breakfast.add(brkFst);
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                }
//                            }
//                        }, new Response.ErrorListener() {
//
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                error.printStackTrace();
//                                VolleyLog.d("byee", "Error: " + error.getMessage());
//                                // hide the progress dialog
//                                pDialog.dismiss();
//                            }
//                        }) {
//                            @Override
//                            protected Map<String, String> getParams() throws AuthFailureError {
//                                HashMap<String , String> object= new HashMap<>();
//                                object.put("position", position);
//
//
//                                return object;
//                            }
//
//
//
//
//                        };
//                        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);
//


//
//
                    }
              });


            }
            return row;

        }


    }

}