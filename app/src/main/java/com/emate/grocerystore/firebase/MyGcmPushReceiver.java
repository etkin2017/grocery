/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emate.grocerystore.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;


import com.emate.grocerystore.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MyGcmPushReceiver extends FirebaseMessagingService {

    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();


    private static String today;
    boolean log;
    public static final String name = "Notification";
    public static final String name2 = "Notification2";
    public static final String name3 = "Notification3";
    public static final String name5 = "Notification5";
    public static long downloadedsize;


    int idd;
    String flag = "1";
    boolean isBackground = false;
    SharedPreferences shhh;
    String filepath, fname, imgnm, videonm;
    File file;
    String name1, name12;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        //Log.e(TAG, "From: " + remoteMessage.getFrom());
        String from = remoteMessage.getFrom();
        String title = remoteMessage.getData().get("title");//("title");
        String message = remoteMessage.getData().get("message");
        String mmmmm = remoteMessage.getData().get("message");
        String sid = remoteMessage.getData().get("sid");
        //  String name1 = remoteMessage.getData().get("name");
        String img = remoteMessage.getData().get("image");

        String unknown = remoteMessage.getData().get("unknown");


        sendNotification(message, from);

    }


    /**
     * Showing notification with text and image
     */


    private void sendNotification(String messageBody, String message) {


        int notify_no = 1;

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notify_no /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.grocery_icon)
                .setContentTitle(messageBody)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notify_no + 2 /* ID of notification */, notificationBuilder.build());
    }


    public void cntByID(String chat_rm_id, int cnt) {
        HashMap<String, String> m1 = new HashMap<String, String>();
        m1.put(chat_rm_id, cnt + "");
    }


}

