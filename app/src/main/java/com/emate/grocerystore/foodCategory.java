package com.emate.grocerystore;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class foodCategory extends AppCompatActivity {
    ArrayList<String> food_list = new ArrayList<>();
    ArrayList<String> breakfast_list = new ArrayList<>();
    ArrayList<String> sub_category_food = new ArrayList<>();
    ArrayList<String> breakfast = new ArrayList<>();
    ArrayList<String> idList = new ArrayList<>();
    ListView listView;
    ImageView img;
    ProgressDialog pDialog;
//   Bundle psn;
  String indexValue;
    String position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_category);
        listView = (ListView) findViewById(R.id.food_category);

        //  food_list=getIntent().getStringArrayListExtra("food_list");
        Bundle   psn = getIntent().getExtras();



       // Intent  intent=getIntent();
        indexValue = getIntent().getStringExtra("position");

        String tag_json_arry = "json_array_req";

        String url = Config.api_bsase_url+"sub_category.php";

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();




        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("hii", response.toString());

                pDialog.dismiss();

                try {

                    JSONObject jsonObject=new JSONObject(response);

                    JSONObject obj = new JSONObject(jsonObject.toString());


                    JSONArray js=obj.getJSONArray("sub_category_result");

                 //   JSONArray arr = obj.getJSONArray("value");

                  //  JSONArray js = new JSONArray(response);// new JSONArray(response.getJSONObject("result"));//response.getJSONObject("result");
                    for (int i = 0; i < js.length(); i++) {
                        String id = js.getJSONObject(i).getString("id");
                        idList.add(id);
                        String food = js.getJSONObject(i).getString("category_type");
                        sub_category_food.add(food);
                    }
                    listView.setAdapter(new foodCategory.Food(foodCategory.this, sub_category_food));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

            }
        })
        {

            @Override
            protected Map<String,String> getParams(){
                Map<String, String> params = new HashMap<String, String>();

              //      indexValue = psn.getString("position");

                params.put("position", indexValue);
                return params;
            }




        };
        AppController.getInstance().addToRequestQueue(sr);
//        JSONObject object = new JSONObject();
//        try {
//            indexValue = psn.getString("position");
//            object.put("position", indexValue);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Loading...");
//        pDialog.show();
//
//
//        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
//                url, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                Log.d("hii", response.toString());
//              //  pDialog.hide();
//
//
//                try {
//                    //JSONArray js= new JSONArray(response.getJSONObject("result"));
//                    JSONArray sub_category_result =new JSONArray(response.getJSONObject("sub_category_result"));// response.getJSONObject("sub_category_result");
//                  // JSONArray det_category_result = new JSONArray("details_cat_result");
//
//                    for (int i = 0; i < sub_category_result.length(); i++) {
//
//
//                        String food = sub_category_result.getJSONObject(i).getString("category_type");
//                        // int food_img_id = sub_category_result.getJSONObject(i).getInt("id");
//                        sub_category_food.add(food);
//                        // food_id.add(food_img_id);
//
//
//                    }
//                    listView.setAdapter(new foodCategory.Food(foodCategory.this, sub_category_food));
//
////
////                    for (int i = 0; i < det_category_result.length(); i++) {
////
////                        try {
////                            String brkFst = det_category_result.getJSONObject(i).getString("details_cat");
////                            breakfast.add(brkFst);
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
////
////                    }
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//            }
//        },  new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//                VolleyLog.d("byee", "Error: " + error.getMessage());
//                pDialog.hide();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                HashMap<String , String> object= new HashMap<>();
//                object.put("position", indexValue );
//
//
//                return object;
//            }
//
//
//
//
//        };
//
//
////            @Override
////            public Map<String, String> getHeaders() throws AuthFailureError {
////                HashMap<String, Integer> headers = new HashMap<String, String>();
////                //headers.put("Content-Type", "application/json");
////                headers.put("position", indexValue);
////                return headers;
////            }
//
//
//
//        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_arry);
//        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url, null,
//                new Response.Listener<JSONObject>() {
//                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.e("hii", response.toString());
//
//                        pDialog.hide();
//
//
//                        try {
//
//                            JSONArray sub_category_result = response.getJSONArray("sub_category_result");
//                            JSONArray  det_category_result = response.getJSONArray("details_cat_result");
//
//                            for (int i = 0; i < sub_category_result.length(); i++) {
//
//
//                                String food = sub_category_result.getJSONObject(i).getString("category_type");
//                                // int food_img_id = sub_category_result.getJSONObject(i).getInt("id");
//                                sub_category_food.add(food);
//                                // food_id.add(food_img_id);
//
//
//                            }
//                            listView.setAdapter(new foodCategory.Food(foodCategory.this, sub_category_food));
//
//
//                            for (int i = 0; i < det_category_result.length(); i++) {
//
//                                try {
//                                    String brkFst = det_category_result.getJSONObject(i).getString("details_cat");
//                                    breakfast.add(brkFst);
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//
//                            }
//
//
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//                VolleyLog.d("byee", "Error: " + error.getMessage());
//                pDialog.hide();
//            }
//
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("position", indexValue);
//
//                return params;
//            }
//        });
//        AppController.getInstance().addToRequestQueue(req, tag_json_arry);
//


    }

    public class Food extends BaseAdapter {

        Context context;
        ArrayList<String> list;

        Food(Context context, ArrayList<String> list) {
            this.context = context;
            this.list = list;// new ArrayList<Item>();
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        class ViewHolder {

            TextView textView;
            ImageView img;

            ViewHolder(View v) {
                textView = (TextView) v.findViewById(R.id.food_row);
                img = (ImageView) v.findViewById(R.id.imageFood);

            }
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            View row = view;

            foodCategory.Food.ViewHolder holder = null;
            if (row == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.foodcategory_singlerow, viewGroup, false);
                holder = new foodCategory.Food.ViewHolder(row);
                holder.textView = (TextView) row.findViewById(R.id.food_row);
                holder.img = (ImageView) row.findViewById(R.id.imageFood);

                String i1 = list.get(i);

                try {
                    holder.textView.setText(i1);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                final foodCategory.Food.ViewHolder finalHolder = holder;
                holder.textView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String title = finalHolder.textView.getText().toString();
                        if (i==0) {
                            position=idList.get(0).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);
                            startActivity(intent);


                        }

                        else if (i==1) {
                            position=idList.get(1).toString();
                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        } else if (i==2) {
                            position=idList.get(2).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        } else if (i==3) {
                            position=idList.get(3).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        } else if (i==4) {
                            position=idList.get(4).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        } else if (i==6) {
                            position=idList.get(5).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        } else if (i==7) {
                            position=idList.get(6).toString();

                            Intent intent = new Intent(context, breakfastActivity.class);
                            intent.putExtra("position", position);
                            intent.putExtra("foodCategory", indexValue);

                            startActivity(intent);


                        }

                    }
                });

//
            }
            return row;

        }
    }
}
